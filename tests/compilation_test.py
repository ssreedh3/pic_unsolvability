from picunsolv.constants import *
from picunsolv.picce_generator import  PicCeGenerator
from picunsolv.best_supporter_graph import BestSupporterGraph

from picunsolv.rpg import RPG

import unittest


class PicCeTestCase(unittest.TestCase):
    def test_basic_compilation(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1']),
                                 DELS: set()},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set(['p1'])},
                          'a3': {POS_PREC: set(['p2', 'p1']),
                                 ADDS: set(['p3']),
                                 DELS: set()
                                 }
                          }
                 }

        clauses = [{'p1', 'p2'}]
        expected_compilation =  {INSTANCE: {INIT: set(), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1']),
                                 COND_ADDS: set([(frozenset(['p2']),frozenset(),
                                                  frozenset([CONJ_PREFIX+CONJ_SEPARATOR.join(sorted(['p1','p2']))
                                                             ])
                                                  )]),
                                 DELS: set()
                                 },
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set()
                                 },
                          'a3': {POS_PREC: set(['p2', 'p1', CONJ_PREFIX + CONJ_SEPARATOR.join(sorted(['p1', 'p2']))]),
                                 ADDS: set(['p3']),DELS: set()
                                 }
                          }
                 }
        pic_gen = PicCeGenerator()
        compiled_model = pic_gen.get_semi_relaxed_model(model, clauses)
        assert compiled_model == expected_compilation


    def test_compilation_leads_to_unsolvable(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set(['p1'])},
                          'a2': {POS_PREC: set(['p2', 'p1']),
                                 ADDS: set(['p3']),
                                 DELS: set()
                                 }
                          }
                 }

        clauses = [{'p1', 'p2'}]
        pic_gen = PicCeGenerator()
        compiled_model = pic_gen.get_semi_relaxed_model(model, clauses)
        rpg = RPG()
        assert not rpg.test_reachability(compiled_model)

    def test_compilation_leads_to_rp_elimination(self):
        model = {INSTANCE: {INIT: set(['p1']), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set(['p1'])},
                          'a2': {POS_PREC: set(['p2', 'p1']),
                                 ADDS: set(['p3']),
                                 DELS: set()
                                 },
                          'a3': {POS_PREC: set(),
                                 ADDS: set(['p4']),
                                 DELS: set()
                                 },
                          'a4': {POS_PREC: set(['p4']),
                                 ADDS: set(['p5']),
                                 DELS: set()
                                 },
                          'a5': {POS_PREC: set(['p5']),
                                 ADDS: set(['p3']),
                                 DELS: set()
                                 }
                          }
                 }
        rpg = RPG()
        bsg_gen = BestSupporterGraph()
        assert rpg.test_reachability(model)
        relaxed_plan_1, predc_map = bsg_gen.get_relaxed_plan(model)
        assert relaxed_plan_1 == ['a1','a2']
        clauses = [{'p1', 'p2'}]
        pic_gen = PicCeGenerator()
        compiled_model = pic_gen.get_semi_relaxed_model(model, clauses)
        assert rpg.test_reachability(compiled_model)
        relaxed_plan_2, predc_map = bsg_gen.get_relaxed_plan(compiled_model)
        assert relaxed_plan_2 == ['a3', 'a4', 'a5']
        assert rpg.test_reachability(compiled_model)



if __name__ == '__main__':
    unittest.main()
