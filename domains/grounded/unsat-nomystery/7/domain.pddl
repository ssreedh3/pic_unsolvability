(define
	(domain grounded-TRANSPORT-STRIPS)
	(:requirements :strips :action-costs)
	(:predicates
		( IN_P0_T0 )
		( IN_P8_T0 )
		( AT_T0_L7 )
		( FUEL_T0_LEVEL91 )
		( AT_T0_L10 )
		( FUEL_T0_LEVEL87 )
		( AT_T0_L3 )
		( FUEL_T0_LEVEL85 )
		( AT_T0_L9 )
		( AT_T0_L11 )
		( FUEL_T0_LEVEL84 )
		( FUEL_T0_LEVEL83 )
		( AT_T0_L5 )
		( AT_T0_L4 )
		( AT_T0_L6 )
		( AT_T0_L2 )
		( FUEL_T0_LEVEL82 )
		( AT_T0_L1 )
		( FUEL_T0_LEVEL81 )
		( AT_T0_L0 )
		( FUEL_T0_LEVEL80 )
		( FUEL_T0_LEVEL79 )
		( FUEL_T0_LEVEL78 )
		( FUEL_T0_LEVEL77 )
		( FUEL_T0_LEVEL76 )
		( FUEL_T0_LEVEL75 )
		( FUEL_T0_LEVEL74 )
		( FUEL_T0_LEVEL73 )
		( FUEL_T0_LEVEL72 )
		( FUEL_T0_LEVEL71 )
		( FUEL_T0_LEVEL70 )
		( FUEL_T0_LEVEL69 )
		( FUEL_T0_LEVEL68 )
		( FUEL_T0_LEVEL67 )
		( FUEL_T0_LEVEL66 )
		( FUEL_T0_LEVEL65 )
		( FUEL_T0_LEVEL64 )
		( FUEL_T0_LEVEL63 )
		( FUEL_T0_LEVEL62 )
		( FUEL_T0_LEVEL61 )
		( FUEL_T0_LEVEL60 )
		( FUEL_T0_LEVEL59 )
		( FUEL_T0_LEVEL58 )
		( FUEL_T0_LEVEL57 )
		( FUEL_T0_LEVEL56 )
		( FUEL_T0_LEVEL55 )
		( FUEL_T0_LEVEL54 )
		( FUEL_T0_LEVEL53 )
		( FUEL_T0_LEVEL52 )
		( FUEL_T0_LEVEL51 )
		( FUEL_T0_LEVEL50 )
		( FUEL_T0_LEVEL49 )
		( FUEL_T0_LEVEL48 )
		( FUEL_T0_LEVEL47 )
		( FUEL_T0_LEVEL46 )
		( FUEL_T0_LEVEL45 )
		( FUEL_T0_LEVEL44 )
		( FUEL_T0_LEVEL43 )
		( FUEL_T0_LEVEL42 )
		( FUEL_T0_LEVEL41 )
		( FUEL_T0_LEVEL40 )
		( FUEL_T0_LEVEL39 )
		( FUEL_T0_LEVEL38 )
		( FUEL_T0_LEVEL37 )
		( FUEL_T0_LEVEL36 )
		( FUEL_T0_LEVEL35 )
		( FUEL_T0_LEVEL34 )
		( FUEL_T0_LEVEL33 )
		( FUEL_T0_LEVEL32 )
		( FUEL_T0_LEVEL31 )
		( FUEL_T0_LEVEL30 )
		( FUEL_T0_LEVEL29 )
		( FUEL_T0_LEVEL28 )
		( FUEL_T0_LEVEL27 )
		( FUEL_T0_LEVEL26 )
		( FUEL_T0_LEVEL25 )
		( FUEL_T0_LEVEL24 )
		( FUEL_T0_LEVEL23 )
		( FUEL_T0_LEVEL22 )
		( FUEL_T0_LEVEL21 )
		( FUEL_T0_LEVEL20 )
		( FUEL_T0_LEVEL19 )
		( FUEL_T0_LEVEL18 )
		( FUEL_T0_LEVEL17 )
		( FUEL_T0_LEVEL16 )
		( FUEL_T0_LEVEL15 )
		( FUEL_T0_LEVEL14 )
		( FUEL_T0_LEVEL13 )
		( FUEL_T0_LEVEL12 )
		( FUEL_T0_LEVEL11 )
		( FUEL_T0_LEVEL10 )
		( FUEL_T0_LEVEL9 )
		( FUEL_T0_LEVEL8 )
		( FUEL_T0_LEVEL7 )
		( FUEL_T0_LEVEL6 )
		( FUEL_T0_LEVEL5 )
		( FUEL_T0_LEVEL4 )
		( FUEL_T0_LEVEL3 )
		( FUEL_T0_LEVEL2 )
		( FUEL_T0_LEVEL1 )
		( FUEL_T0_LEVEL0 )
		( IN_P1_T0 )
		( IN_P2_T0 )
		( IN_P3_T0 )
		( IN_P4_T0 )
		( IN_P5_T0 )
		( IN_P6_T0 )
		( IN_P7_T0 )
		( IN_P9_T0 )
		( IN_P10_T0 )
		( IN_P11_T0 )
		( IN_P12_T0 )
		( IN_P13_T0 )
		( IN_P14_T0 )
		( AT_P0_L0 )
		( AT_P0_L1 )
		( AT_P0_L2 )
		( AT_P0_L3 )
		( AT_P0_L4 )
		( AT_P0_L5 )
		( AT_P0_L6 )
		( AT_P0_L7 )
		( AT_P0_L9 )
		( AT_P0_L10 )
		( AT_P0_L11 )
		( AT_P1_L0 )
		( AT_P1_L1 )
		( AT_P1_L2 )
		( AT_P1_L3 )
		( AT_P1_L4 )
		( AT_P1_L5 )
		( AT_P1_L6 )
		( AT_P1_L8 )
		( AT_P1_L9 )
		( AT_P1_L10 )
		( AT_P1_L11 )
		( AT_P2_L0 )
		( AT_P2_L2 )
		( AT_P2_L3 )
		( AT_P2_L4 )
		( AT_P2_L5 )
		( AT_P2_L6 )
		( AT_P2_L7 )
		( AT_P2_L8 )
		( AT_P2_L9 )
		( AT_P2_L10 )
		( AT_P2_L11 )
		( AT_P3_L0 )
		( AT_P3_L1 )
		( AT_P3_L3 )
		( AT_P3_L4 )
		( AT_P3_L5 )
		( AT_P3_L6 )
		( AT_P3_L7 )
		( AT_P3_L8 )
		( AT_P3_L9 )
		( AT_P3_L10 )
		( AT_P3_L11 )
		( AT_P4_L0 )
		( AT_P4_L1 )
		( AT_P4_L2 )
		( AT_P4_L3 )
		( AT_P4_L4 )
		( AT_P4_L5 )
		( AT_P4_L7 )
		( AT_P4_L8 )
		( AT_P4_L9 )
		( AT_P4_L10 )
		( AT_P4_L11 )
		( AT_P5_L0 )
		( AT_P5_L1 )
		( AT_P5_L2 )
		( AT_P5_L3 )
		( AT_P5_L4 )
		( AT_P5_L5 )
		( AT_P5_L6 )
		( AT_P5_L8 )
		( AT_P5_L9 )
		( AT_P5_L10 )
		( AT_P5_L11 )
		( AT_P6_L0 )
		( AT_P6_L2 )
		( AT_P6_L3 )
		( AT_P6_L4 )
		( AT_P6_L5 )
		( AT_P6_L6 )
		( AT_P6_L7 )
		( AT_P6_L8 )
		( AT_P6_L9 )
		( AT_P6_L10 )
		( AT_P6_L11 )
		( AT_P7_L0 )
		( AT_P7_L1 )
		( AT_P7_L2 )
		( AT_P7_L3 )
		( AT_P7_L5 )
		( AT_P7_L6 )
		( AT_P7_L7 )
		( AT_P7_L8 )
		( AT_P7_L9 )
		( AT_P7_L10 )
		( AT_P7_L11 )
		( AT_P8_L0 )
		( AT_P8_L1 )
		( AT_P8_L2 )
		( AT_P8_L3 )
		( AT_P8_L4 )
		( AT_P8_L5 )
		( AT_P8_L6 )
		( AT_P8_L7 )
		( AT_P8_L9 )
		( AT_P8_L10 )
		( AT_P8_L11 )
		( AT_P9_L0 )
		( AT_P9_L1 )
		( AT_P9_L3 )
		( AT_P9_L4 )
		( AT_P9_L5 )
		( AT_P9_L6 )
		( AT_P9_L7 )
		( AT_P9_L8 )
		( AT_P9_L9 )
		( AT_P9_L10 )
		( AT_P9_L11 )
		( AT_P10_L0 )
		( AT_P10_L1 )
		( AT_P10_L2 )
		( AT_P10_L3 )
		( AT_P10_L4 )
		( AT_P10_L5 )
		( AT_P10_L6 )
		( AT_P10_L7 )
		( AT_P10_L8 )
		( AT_P10_L9 )
		( AT_P10_L11 )
		( AT_P11_L0 )
		( AT_P11_L1 )
		( AT_P11_L2 )
		( AT_P11_L3 )
		( AT_P11_L4 )
		( AT_P11_L5 )
		( AT_P11_L6 )
		( AT_P11_L7 )
		( AT_P11_L8 )
		( AT_P11_L9 )
		( AT_P11_L11 )
		( AT_P12_L0 )
		( AT_P12_L1 )
		( AT_P12_L2 )
		( AT_P12_L3 )
		( AT_P12_L4 )
		( AT_P12_L5 )
		( AT_P12_L6 )
		( AT_P12_L8 )
		( AT_P12_L9 )
		( AT_P12_L10 )
		( AT_P12_L11 )
		( AT_P13_L0 )
		( AT_P13_L1 )
		( AT_P13_L2 )
		( AT_P13_L3 )
		( AT_P13_L4 )
		( AT_P13_L5 )
		( AT_P13_L7 )
		( AT_P13_L8 )
		( AT_P13_L9 )
		( AT_P13_L10 )
		( AT_P13_L11 )
		( AT_P14_L0 )
		( AT_P14_L2 )
		( AT_P14_L3 )
		( AT_P14_L4 )
		( AT_P14_L5 )
		( AT_P14_L6 )
		( AT_P14_L7 )
		( AT_P14_L8 )
		( AT_P14_L9 )
		( AT_P14_L10 )
		( AT_P14_L11 )
		( FUEL_T0_LEVEL98 )
		( FUEL_T0_LEVEL97 )
		( FUEL_T0_LEVEL96 )
		( FUEL_T0_LEVEL95 )
		( FUEL_T0_LEVEL94 )
		( FUEL_T0_LEVEL93 )
		( FUEL_T0_LEVEL92 )
		( FUEL_T0_LEVEL90 )
		( FUEL_T0_LEVEL89 )
		( FUEL_T0_LEVEL88 )
		( FUEL_T0_LEVEL86 )
		( AT_T0_L8 )
		( FUEL_T0_LEVEL100 )
		( AT_P14_L1 )
		( AT_P13_L6 )
		( AT_P12_L7 )
		( AT_P11_L10 )
		( AT_P10_L10 )
		( AT_P9_L2 )
		( AT_P7_L4 )
		( AT_P6_L1 )
		( AT_P5_L7 )
		( AT_P4_L6 )
		( AT_P3_L2 )
		( AT_P2_L1 )
		( AT_P1_L7 )
		( AT_P8_L8 )
		( AT_P0_L8 )
	) 
	(:action LOAD_P14_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P14_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L11 ))
		)
	)
	(:action LOAD_P14_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P14_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L10 ))
		)
	)
	(:action LOAD_P14_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P14_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L9 ))
		)
	)
	(:action LOAD_P14_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P14_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L8 ))
		)
	)
	(:action LOAD_P14_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P14_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L7 ))
		)
	)
	(:action LOAD_P14_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P14_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L6 ))
		)
	)
	(:action LOAD_P14_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P14_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L5 ))
		)
	)
	(:action LOAD_P14_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P14_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L4 ))
		)
	)
	(:action LOAD_P14_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P14_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L3 ))
		)
	)
	(:action LOAD_P14_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P14_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L2 ))
		)
	)
	(:action LOAD_P14_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P14_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L0 ))
		)
	)
	(:action LOAD_P13_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P13_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L11 ))
		)
	)
	(:action LOAD_P13_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P13_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L10 ))
		)
	)
	(:action LOAD_P13_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P13_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L9 ))
		)
	)
	(:action LOAD_P13_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P13_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L8 ))
		)
	)
	(:action LOAD_P13_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P13_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L7 ))
		)
	)
	(:action LOAD_P13_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P13_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L5 ))
		)
	)
	(:action LOAD_P13_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P13_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L4 ))
		)
	)
	(:action LOAD_P13_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P13_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L3 ))
		)
	)
	(:action LOAD_P13_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P13_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L2 ))
		)
	)
	(:action LOAD_P13_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P13_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L1 ))
		)
	)
	(:action LOAD_P13_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P13_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L0 ))
		)
	)
	(:action LOAD_P12_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P12_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L11 ))
		)
	)
	(:action LOAD_P12_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P12_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L10 ))
		)
	)
	(:action LOAD_P12_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P12_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L9 ))
		)
	)
	(:action LOAD_P12_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P12_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L8 ))
		)
	)
	(:action LOAD_P12_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P12_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L6 ))
		)
	)
	(:action LOAD_P12_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P12_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L5 ))
		)
	)
	(:action LOAD_P12_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P12_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L4 ))
		)
	)
	(:action LOAD_P12_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P12_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L3 ))
		)
	)
	(:action LOAD_P12_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P12_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L2 ))
		)
	)
	(:action LOAD_P12_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P12_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L1 ))
		)
	)
	(:action LOAD_P12_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P12_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L0 ))
		)
	)
	(:action LOAD_P11_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P11_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L11 ))
		)
	)
	(:action LOAD_P11_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P11_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L9 ))
		)
	)
	(:action LOAD_P11_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P11_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L8 ))
		)
	)
	(:action LOAD_P11_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P11_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L7 ))
		)
	)
	(:action LOAD_P11_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P11_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L6 ))
		)
	)
	(:action LOAD_P11_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P11_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L5 ))
		)
	)
	(:action LOAD_P11_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P11_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L4 ))
		)
	)
	(:action LOAD_P11_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P11_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L3 ))
		)
	)
	(:action LOAD_P11_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P11_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L2 ))
		)
	)
	(:action LOAD_P11_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P11_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L1 ))
		)
	)
	(:action LOAD_P11_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P11_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L0 ))
		)
	)
	(:action LOAD_P10_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P10_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L11 ))
		)
	)
	(:action LOAD_P10_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P10_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L9 ))
		)
	)
	(:action LOAD_P10_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P10_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L8 ))
		)
	)
	(:action LOAD_P10_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P10_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L7 ))
		)
	)
	(:action LOAD_P10_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P10_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L6 ))
		)
	)
	(:action LOAD_P10_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P10_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L5 ))
		)
	)
	(:action LOAD_P10_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P10_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L4 ))
		)
	)
	(:action LOAD_P10_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P10_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L3 ))
		)
	)
	(:action LOAD_P10_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P10_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L2 ))
		)
	)
	(:action LOAD_P10_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P10_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L1 ))
		)
	)
	(:action LOAD_P10_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P10_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L0 ))
		)
	)
	(:action LOAD_P9_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P9_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L11 ))
		)
	)
	(:action LOAD_P9_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P9_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L10 ))
		)
	)
	(:action LOAD_P9_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P9_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L9 ))
		)
	)
	(:action LOAD_P9_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P9_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L8 ))
		)
	)
	(:action LOAD_P9_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P9_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L7 ))
		)
	)
	(:action LOAD_P9_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P9_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L6 ))
		)
	)
	(:action LOAD_P9_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P9_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L5 ))
		)
	)
	(:action LOAD_P9_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P9_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L4 ))
		)
	)
	(:action LOAD_P9_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P9_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L3 ))
		)
	)
	(:action LOAD_P9_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P9_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L1 ))
		)
	)
	(:action LOAD_P9_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P9_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L0 ))
		)
	)
	(:action LOAD_P8_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P8_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L11 ))
		)
	)
	(:action LOAD_P8_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P8_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L10 ))
		)
	)
	(:action LOAD_P8_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P8_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L9 ))
		)
	)
	(:action LOAD_P8_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P8_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L7 ))
		)
	)
	(:action LOAD_P8_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P8_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L6 ))
		)
	)
	(:action LOAD_P8_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P8_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L5 ))
		)
	)
	(:action LOAD_P8_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P8_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L4 ))
		)
	)
	(:action LOAD_P8_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P8_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L3 ))
		)
	)
	(:action LOAD_P8_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P8_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L2 ))
		)
	)
	(:action LOAD_P8_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P8_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L1 ))
		)
	)
	(:action LOAD_P8_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P8_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L0 ))
		)
	)
	(:action LOAD_P7_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P7_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L11 ))
		)
	)
	(:action LOAD_P7_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P7_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L10 ))
		)
	)
	(:action LOAD_P7_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P7_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L9 ))
		)
	)
	(:action LOAD_P7_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P7_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L8 ))
		)
	)
	(:action LOAD_P7_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P7_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L7 ))
		)
	)
	(:action LOAD_P7_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P7_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L6 ))
		)
	)
	(:action LOAD_P7_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P7_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L5 ))
		)
	)
	(:action LOAD_P7_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P7_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L3 ))
		)
	)
	(:action LOAD_P7_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P7_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L2 ))
		)
	)
	(:action LOAD_P7_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P7_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L1 ))
		)
	)
	(:action LOAD_P7_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P7_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L0 ))
		)
	)
	(:action LOAD_P6_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P6_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L11 ))
		)
	)
	(:action LOAD_P6_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P6_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L10 ))
		)
	)
	(:action LOAD_P6_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P6_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L9 ))
		)
	)
	(:action LOAD_P6_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P6_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L8 ))
		)
	)
	(:action LOAD_P6_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P6_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L7 ))
		)
	)
	(:action LOAD_P6_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P6_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L6 ))
		)
	)
	(:action LOAD_P6_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P6_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L5 ))
		)
	)
	(:action LOAD_P6_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P6_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L4 ))
		)
	)
	(:action LOAD_P6_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P6_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L3 ))
		)
	)
	(:action LOAD_P6_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P6_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L2 ))
		)
	)
	(:action LOAD_P6_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P6_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L0 ))
		)
	)
	(:action LOAD_P5_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P5_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L11 ))
		)
	)
	(:action LOAD_P5_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P5_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L10 ))
		)
	)
	(:action LOAD_P5_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P5_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L9 ))
		)
	)
	(:action LOAD_P5_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P5_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L8 ))
		)
	)
	(:action LOAD_P5_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P5_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L6 ))
		)
	)
	(:action LOAD_P5_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P5_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L5 ))
		)
	)
	(:action LOAD_P5_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P5_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L4 ))
		)
	)
	(:action LOAD_P5_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P5_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L3 ))
		)
	)
	(:action LOAD_P5_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P5_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L2 ))
		)
	)
	(:action LOAD_P5_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P5_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L1 ))
		)
	)
	(:action LOAD_P5_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P5_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L0 ))
		)
	)
	(:action LOAD_P4_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P4_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L11 ))
		)
	)
	(:action LOAD_P4_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P4_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L10 ))
		)
	)
	(:action LOAD_P4_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P4_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L9 ))
		)
	)
	(:action LOAD_P4_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P4_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L8 ))
		)
	)
	(:action LOAD_P4_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P4_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L7 ))
		)
	)
	(:action LOAD_P4_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P4_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L5 ))
		)
	)
	(:action LOAD_P4_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P4_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L4 ))
		)
	)
	(:action LOAD_P4_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P4_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L3 ))
		)
	)
	(:action LOAD_P4_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P4_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L2 ))
		)
	)
	(:action LOAD_P4_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P4_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L1 ))
		)
	)
	(:action LOAD_P4_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P4_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L0 ))
		)
	)
	(:action LOAD_P3_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P3_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L11 ))
		)
	)
	(:action LOAD_P3_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P3_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L10 ))
		)
	)
	(:action LOAD_P3_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P3_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L9 ))
		)
	)
	(:action LOAD_P3_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P3_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L8 ))
		)
	)
	(:action LOAD_P3_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P3_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L7 ))
		)
	)
	(:action LOAD_P3_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P3_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L6 ))
		)
	)
	(:action LOAD_P3_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P3_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L5 ))
		)
	)
	(:action LOAD_P3_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P3_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L4 ))
		)
	)
	(:action LOAD_P3_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P3_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L3 ))
		)
	)
	(:action LOAD_P3_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P3_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L1 ))
		)
	)
	(:action LOAD_P3_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P3_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L0 ))
		)
	)
	(:action LOAD_P2_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P2_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L11 ))
		)
	)
	(:action LOAD_P2_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P2_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L10 ))
		)
	)
	(:action LOAD_P2_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P2_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L9 ))
		)
	)
	(:action LOAD_P2_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P2_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L8 ))
		)
	)
	(:action LOAD_P2_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P2_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L7 ))
		)
	)
	(:action LOAD_P2_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P2_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L6 ))
		)
	)
	(:action LOAD_P2_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P2_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L5 ))
		)
	)
	(:action LOAD_P2_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P2_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L4 ))
		)
	)
	(:action LOAD_P2_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P2_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L3 ))
		)
	)
	(:action LOAD_P2_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P2_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L2 ))
		)
	)
	(:action LOAD_P2_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P2_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L0 ))
		)
	)
	(:action LOAD_P1_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P1_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L11 ))
		)
	)
	(:action LOAD_P1_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P1_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L10 ))
		)
	)
	(:action LOAD_P1_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P1_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L9 ))
		)
	)
	(:action LOAD_P1_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P1_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L8 ))
		)
	)
	(:action LOAD_P1_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P1_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L6 ))
		)
	)
	(:action LOAD_P1_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P1_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L5 ))
		)
	)
	(:action LOAD_P1_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P1_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L4 ))
		)
	)
	(:action LOAD_P1_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P1_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L3 ))
		)
	)
	(:action LOAD_P1_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P1_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L2 ))
		)
	)
	(:action LOAD_P1_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P1_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L1 ))
		)
	)
	(:action LOAD_P1_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P1_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L0 ))
		)
	)
	(:action LOAD_P0_T0_L11
		:parameters ()
		:precondition
		(and
			( AT_P0_L11 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L11 ))
		)
	)
	(:action LOAD_P0_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P0_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L10 ))
		)
	)
	(:action LOAD_P0_T0_L9
		:parameters ()
		:precondition
		(and
			( AT_P0_L9 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L9 ))
		)
	)
	(:action LOAD_P0_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P0_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L7 ))
		)
	)
	(:action LOAD_P0_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P0_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L6 ))
		)
	)
	(:action LOAD_P0_T0_L5
		:parameters ()
		:precondition
		(and
			( AT_P0_L5 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L5 ))
		)
	)
	(:action LOAD_P0_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P0_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L4 ))
		)
	)
	(:action LOAD_P0_T0_L3
		:parameters ()
		:precondition
		(and
			( AT_P0_L3 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L3 ))
		)
	)
	(:action LOAD_P0_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P0_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L2 ))
		)
	)
	(:action LOAD_P0_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P0_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L1 ))
		)
	)
	(:action LOAD_P0_T0_L0
		:parameters ()
		:precondition
		(and
			( AT_P0_L0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L0 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL61_LEVEL25_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL61_LEVEL25_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL62_LEVEL24_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL62_LEVEL24_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL62_LEVEL24_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL62_LEVEL24_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL63_LEVEL25_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL63_LEVEL25_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL64_LEVEL24_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL64_LEVEL24_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL64_LEVEL24_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL64_LEVEL24_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL64_LEVEL25_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL64_LEVEL25_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL65_LEVEL24_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL65_LEVEL24_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL65_LEVEL24_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL65_LEVEL24_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL65_LEVEL25_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL65_LEVEL25_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL66_LEVEL20_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL66_LEVEL20_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL66_LEVEL20_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL66_LEVEL20_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL66_LEVEL24_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL66_LEVEL24_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL66_LEVEL24_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL66_LEVEL24_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL67_LEVEL19_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL67_LEVEL19_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL67_LEVEL25_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL67_LEVEL25_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL68_LEVEL20_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL68_LEVEL20_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL68_LEVEL20_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL68_LEVEL20_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL68_LEVEL24_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL68_LEVEL24_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL68_LEVEL24_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL68_LEVEL24_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL68_LEVEL25_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL68_LEVEL25_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL69_LEVEL17_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL69_LEVEL19_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL69_LEVEL19_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL69_LEVEL20_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL69_LEVEL20_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL69_LEVEL20_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL69_LEVEL20_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL69_LEVEL24_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL69_LEVEL24_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL69_LEVEL24_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL69_LEVEL24_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL69_LEVEL25_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL69_LEVEL25_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL70_LEVEL16_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL70_LEVEL16_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL70_LEVEL19_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL70_LEVEL19_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL70_LEVEL20_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL70_LEVEL20_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL70_LEVEL20_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL70_LEVEL20_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL70_LEVEL24_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL70_LEVEL24_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL70_LEVEL24_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL70_LEVEL24_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL70_LEVEL25_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL70_LEVEL25_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL71_LEVEL15_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL71_LEVEL15_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL71_LEVEL17_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL71_LEVEL19_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL71_LEVEL19_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL71_LEVEL24_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL71_LEVEL24_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL71_LEVEL24_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL71_LEVEL24_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL71_LEVEL25_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL71_LEVEL25_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL72_LEVEL16_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL72_LEVEL16_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL72_LEVEL17_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL72_LEVEL20_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL72_LEVEL20_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL72_LEVEL20_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL72_LEVEL20_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL72_LEVEL24_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL72_LEVEL24_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL72_LEVEL24_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL72_LEVEL24_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL72_LEVEL25_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL72_LEVEL25_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL73_LEVEL13_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL73_LEVEL13_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL73_LEVEL15_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL73_LEVEL15_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL73_LEVEL16_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL73_LEVEL16_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL73_LEVEL17_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL73_LEVEL19_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL73_LEVEL19_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL73_LEVEL20_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL73_LEVEL20_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL73_LEVEL20_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL73_LEVEL20_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL73_LEVEL24_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL73_LEVEL24_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL73_LEVEL24_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL73_LEVEL24_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL73_LEVEL25_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL73_LEVEL25_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL74_LEVEL15_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL74_LEVEL15_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL74_LEVEL16_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL74_LEVEL16_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL74_LEVEL19_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL74_LEVEL19_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL74_LEVEL20_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL74_LEVEL20_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL74_LEVEL20_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL74_LEVEL20_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL74_LEVEL24_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL74_LEVEL24_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL74_LEVEL24_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL74_LEVEL24_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL75_LEVEL13_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL75_LEVEL13_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL75_LEVEL15_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL75_LEVEL15_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL75_LEVEL17_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL75_LEVEL19_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL75_LEVEL19_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL75_LEVEL20_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL75_LEVEL20_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL75_LEVEL20_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL75_LEVEL20_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL76_LEVEL10_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL76_LEVEL10_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL76_LEVEL13_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL76_LEVEL13_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL76_LEVEL16_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL76_LEVEL16_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL76_LEVEL17_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL76_LEVEL19_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL76_LEVEL19_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL76_LEVEL20_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL76_LEVEL20_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL76_LEVEL20_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL76_LEVEL20_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL77_LEVEL9_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL77_LEVEL9_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL77_LEVEL13_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL77_LEVEL13_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL77_LEVEL15_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL77_LEVEL15_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL77_LEVEL16_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL77_LEVEL16_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL77_LEVEL17_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL77_LEVEL19_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL77_LEVEL19_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL77_LEVEL20_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL77_LEVEL20_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL77_LEVEL20_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL77_LEVEL20_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL78_LEVEL8_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL78_LEVEL8_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL78_LEVEL10_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL78_LEVEL10_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL78_LEVEL15_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL78_LEVEL15_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL78_LEVEL16_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL78_LEVEL16_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL78_LEVEL17_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL78_LEVEL19_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL78_LEVEL19_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL78_LEVEL20_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL78_LEVEL20_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL78_LEVEL20_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL78_LEVEL20_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL79_LEVEL9_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL79_LEVEL9_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL79_LEVEL10_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL79_LEVEL10_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL79_LEVEL13_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL79_LEVEL13_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL79_LEVEL15_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL79_LEVEL15_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL79_LEVEL16_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL79_LEVEL16_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL79_LEVEL17_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL79_LEVEL19_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL79_LEVEL19_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL80_LEVEL8_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL80_LEVEL8_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL80_LEVEL9_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL80_LEVEL9_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL80_LEVEL10_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL80_LEVEL10_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL80_LEVEL13_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL80_LEVEL13_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL80_LEVEL15_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL80_LEVEL15_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL80_LEVEL16_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL80_LEVEL16_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL80_LEVEL17_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL81_LEVEL8_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL81_LEVEL8_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL81_LEVEL9_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL81_LEVEL9_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL81_LEVEL13_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL81_LEVEL13_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL81_LEVEL15_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL81_LEVEL15_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL81_LEVEL16_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL81_LEVEL16_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL81_LEVEL17_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL82_LEVEL4_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL82_LEVEL4_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL82_LEVEL8_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL82_LEVEL8_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL82_LEVEL10_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL82_LEVEL10_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL82_LEVEL13_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL82_LEVEL13_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL82_LEVEL15_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL82_LEVEL15_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL82_LEVEL16_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL82_LEVEL16_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL83_LEVEL3_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL83_LEVEL3_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL83_LEVEL9_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL83_LEVEL9_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL83_LEVEL10_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL83_LEVEL10_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL83_LEVEL13_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL83_LEVEL13_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL83_LEVEL15_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL83_LEVEL15_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL84_LEVEL2_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL84_LEVEL2_LEVEL86
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL86 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL84_LEVEL3_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL84_LEVEL3_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL84_LEVEL4_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL84_LEVEL4_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL84_LEVEL8_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL84_LEVEL8_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL84_LEVEL9_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL84_LEVEL9_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL84_LEVEL10_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL84_LEVEL10_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL84_LEVEL13_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL84_LEVEL13_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL84_LEVEL16_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL85_LEVEL2_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL85_LEVEL3_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL85_LEVEL3_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL85_LEVEL4_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL85_LEVEL4_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL85_LEVEL8_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL85_LEVEL8_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL85_LEVEL9_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL85_LEVEL9_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL85_LEVEL10_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL85_LEVEL10_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL85_LEVEL13_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL85_LEVEL13_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL86_LEVEL2_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL86_LEVEL2_LEVEL88
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL88 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL86_LEVEL3_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL86_LEVEL3_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL86_LEVEL4_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL86_LEVEL4_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL86_LEVEL8_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL86_LEVEL8_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL86_LEVEL9_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL86_LEVEL9_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL86_LEVEL10_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL86_LEVEL10_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL86 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL87_LEVEL2_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL87_LEVEL2_LEVEL89
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL89 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL87_LEVEL3_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL87_LEVEL3_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL87_LEVEL4_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL87_LEVEL4_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL87_LEVEL8_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL87_LEVEL8_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL87_LEVEL9_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL87_LEVEL9_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL87_LEVEL10_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL87_LEVEL10_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL87_LEVEL13_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL88_LEVEL2_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL88_LEVEL2_LEVEL90
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL90 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL88_LEVEL3_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL88_LEVEL3_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL88_LEVEL4_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL88_LEVEL4_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL88_LEVEL8_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL88_LEVEL8_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL88_LEVEL9_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL88_LEVEL9_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL88_LEVEL10_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL88_LEVEL10_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL88 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL89_LEVEL2_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL89_LEVEL2_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL89_LEVEL3_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL89_LEVEL3_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL89_LEVEL4_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL89_LEVEL4_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL89_LEVEL8_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL89_LEVEL8_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL89_LEVEL9_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL89_LEVEL9_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL89 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL90_LEVEL2_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL90_LEVEL2_LEVEL92
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL92 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL90_LEVEL3_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL90_LEVEL3_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL90_LEVEL4_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL90_LEVEL4_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL90_LEVEL8_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL90_LEVEL8_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL90_LEVEL10_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL90_LEVEL10_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL90 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL91_LEVEL2_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL91_LEVEL2_LEVEL93
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL93 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL91_LEVEL3_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL91_LEVEL3_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL91_LEVEL4_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL91_LEVEL4_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL92_LEVEL2_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL92_LEVEL2_LEVEL94
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL94 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL92_LEVEL3_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL92_LEVEL3_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL92_LEVEL4_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL92_LEVEL4_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL92_LEVEL8_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL92_LEVEL8_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL92 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL93_LEVEL2_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL93_LEVEL2_LEVEL95
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL95 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL93_LEVEL3_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL93_LEVEL3_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL93_LEVEL4_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL93_LEVEL4_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL93 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL94_LEVEL2_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL94_LEVEL2_LEVEL96
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL96 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL94_LEVEL3_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL94_LEVEL3_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL94_LEVEL4_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL94_LEVEL4_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL94 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL95_LEVEL2_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL95 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL95_LEVEL2_LEVEL97
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL97 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL95 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL97 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL95_LEVEL3_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL95 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL95_LEVEL3_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL95 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL96_LEVEL2_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL96 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL96_LEVEL2_LEVEL98
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL96 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL98 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL96_LEVEL4_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL96 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL96_LEVEL4_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL96 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL97_LEVEL3_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL97 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL97_LEVEL3_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL97 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL98_LEVEL2_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL98 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL98_LEVEL2_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL98 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action UNLOAD_P14_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P14_L11 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P14_L10 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P14_L9 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P14_L8 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P14_L7 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P14_L6 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P14_L5 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P14_L4 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P14_L3 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P14_L2 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P14_L1 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P14_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P14_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P14_L0 )
			(not ( IN_P14_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P13_L11 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P13_L10 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P13_L9 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P13_L8 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P13_L7 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P13_L6 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P13_L5 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P13_L4 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P13_L3 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P13_L2 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P13_L1 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P13_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P13_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P13_L0 )
			(not ( IN_P13_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P12_L11 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P12_L10 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P12_L9 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P12_L8 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P12_L7 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P12_L6 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P12_L5 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P12_L4 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P12_L3 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P12_L2 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P12_L1 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P12_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P12_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P12_L0 )
			(not ( IN_P12_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P11_L11 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P11_L10 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P11_L9 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P11_L8 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P11_L7 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P11_L6 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P11_L5 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P11_L4 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P11_L3 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P11_L2 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P11_L1 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P11_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P11_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P11_L0 )
			(not ( IN_P11_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P10_L11 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P10_L10 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P10_L9 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P10_L8 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P10_L7 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P10_L6 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P10_L5 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P10_L4 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P10_L3 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P10_L2 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P10_L1 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P10_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P10_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P10_L0 )
			(not ( IN_P10_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P9_L11 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P9_L10 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P9_L9 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P9_L8 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P9_L7 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P9_L6 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P9_L5 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P9_L4 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P9_L3 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P9_L2 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P9_L1 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P9_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P9_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P9_L0 )
			(not ( IN_P9_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P8_L11 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P8_L10 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P8_L9 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P8_L7 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P8_L6 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P8_L5 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P8_L4 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P8_L3 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P8_L2 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P8_L1 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P8_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P8_L0 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P7_L11 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P7_L10 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P7_L9 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P7_L8 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P7_L7 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P7_L6 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P7_L5 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P7_L4 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P7_L3 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P7_L2 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P7_L1 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P7_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P7_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P7_L0 )
			(not ( IN_P7_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P6_L11 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P6_L10 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P6_L9 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P6_L8 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P6_L7 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P6_L6 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P6_L5 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P6_L4 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P6_L3 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P6_L2 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P6_L1 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P6_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P6_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P6_L0 )
			(not ( IN_P6_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P5_L11 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P5_L10 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P5_L9 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P5_L8 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P5_L7 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P5_L6 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P5_L5 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P5_L4 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P5_L3 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P5_L2 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P5_L1 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P5_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P5_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P5_L0 )
			(not ( IN_P5_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P4_L11 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P4_L10 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P4_L9 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P4_L8 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P4_L7 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P4_L6 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P4_L5 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P4_L4 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P4_L3 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P4_L2 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P4_L1 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P4_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P4_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P4_L0 )
			(not ( IN_P4_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P3_L11 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P3_L10 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P3_L9 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P3_L8 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P3_L7 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P3_L6 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P3_L5 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P3_L4 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P3_L3 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P3_L2 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P3_L1 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P3_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P3_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P3_L0 )
			(not ( IN_P3_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P2_L11 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P2_L10 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P2_L9 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P2_L8 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P2_L7 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P2_L6 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P2_L5 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P2_L4 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P2_L3 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P2_L2 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P2_L1 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P2_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P2_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P2_L0 )
			(not ( IN_P2_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P1_L11 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P1_L10 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P1_L9 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P1_L8 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P1_L7 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P1_L6 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P1_L5 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P1_L4 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P1_L3 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P1_L2 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P1_L1 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P1_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P1_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P1_L0 )
			(not ( IN_P1_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L11
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L11 )
		)
		:effect
		(and
			( AT_P0_L11 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L10
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( AT_P0_L10 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L9
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L9 )
		)
		:effect
		(and
			( AT_P0_L9 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L7
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( AT_P0_L7 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L6
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( AT_P0_L6 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L5
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L5 )
		)
		:effect
		(and
			( AT_P0_L5 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L4
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( AT_P0_L4 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L3
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L3 )
		)
		:effect
		(and
			( AT_P0_L3 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L2
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( AT_P0_L2 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L1
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( AT_P0_L1 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L0
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L0 )
		)
		:effect
		(and
			( AT_P0_L0 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action LOAD_P14_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P14_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P14_T0 )
			(not ( AT_P14_L1 ))
		)
	)
	(:action LOAD_P13_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P13_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P13_T0 )
			(not ( AT_P13_L6 ))
		)
	)
	(:action LOAD_P12_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P12_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P12_T0 )
			(not ( AT_P12_L7 ))
		)
	)
	(:action LOAD_P11_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P11_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P11_T0 )
			(not ( AT_P11_L10 ))
		)
	)
	(:action LOAD_P10_T0_L10
		:parameters ()
		:precondition
		(and
			( AT_P10_L10 )
			( AT_T0_L10 )
		)
		:effect
		(and
			( IN_P10_T0 )
			(not ( AT_P10_L10 ))
		)
	)
	(:action LOAD_P9_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P9_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P9_T0 )
			(not ( AT_P9_L2 ))
		)
	)
	(:action LOAD_P7_T0_L4
		:parameters ()
		:precondition
		(and
			( AT_P7_L4 )
			( AT_T0_L4 )
		)
		:effect
		(and
			( IN_P7_T0 )
			(not ( AT_P7_L4 ))
		)
	)
	(:action LOAD_P6_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P6_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P6_T0 )
			(not ( AT_P6_L1 ))
		)
	)
	(:action LOAD_P5_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P5_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P5_T0 )
			(not ( AT_P5_L7 ))
		)
	)
	(:action LOAD_P4_T0_L6
		:parameters ()
		:precondition
		(and
			( AT_P4_L6 )
			( AT_T0_L6 )
		)
		:effect
		(and
			( IN_P4_T0 )
			(not ( AT_P4_L6 ))
		)
	)
	(:action LOAD_P3_T0_L2
		:parameters ()
		:precondition
		(and
			( AT_P3_L2 )
			( AT_T0_L2 )
		)
		:effect
		(and
			( IN_P3_T0 )
			(not ( AT_P3_L2 ))
		)
	)
	(:action LOAD_P2_T0_L1
		:parameters ()
		:precondition
		(and
			( AT_P2_L1 )
			( AT_T0_L1 )
		)
		:effect
		(and
			( IN_P2_T0 )
			(not ( AT_P2_L1 ))
		)
	)
	(:action LOAD_P1_T0_L7
		:parameters ()
		:precondition
		(and
			( AT_P1_L7 )
			( AT_T0_L7 )
		)
		:effect
		(and
			( IN_P1_T0 )
			(not ( AT_P1_L7 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL0_LEVEL2_LEVEL2
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL2 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL2 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL0_LEVEL2_LEVEL2
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL2 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL0_LEVEL3_LEVEL3
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL3 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL3 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL0_LEVEL3_LEVEL3
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL3 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL3 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL0_LEVEL4_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL0_LEVEL4_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL0_LEVEL8_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL0_LEVEL8_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL0_LEVEL9_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL0_LEVEL9_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL0_LEVEL10_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL0_LEVEL10_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL0_LEVEL13_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL0_LEVEL13_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL0_LEVEL15_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL0_LEVEL15_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL0_LEVEL16_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL0_LEVEL16_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL0_LEVEL17_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL0_LEVEL19_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL0_LEVEL19_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL0_LEVEL20_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL0_LEVEL20_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL0_LEVEL20_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL0_LEVEL20_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL0_LEVEL24_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL0_LEVEL24_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL0_LEVEL24_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL0_LEVEL24_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL0_LEVEL25_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL0_LEVEL25_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL0 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL1_LEVEL2_LEVEL3
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL3 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL3 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL1_LEVEL2_LEVEL3
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL3 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL1_LEVEL3_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL1_LEVEL3_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL1_LEVEL4_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL1_LEVEL4_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL1_LEVEL8_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL1_LEVEL8_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL1_LEVEL9_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL1_LEVEL9_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL1_LEVEL10_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL1_LEVEL10_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL1_LEVEL13_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL1_LEVEL13_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL1_LEVEL15_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL1_LEVEL15_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL1_LEVEL16_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL1_LEVEL16_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL1_LEVEL17_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL1_LEVEL19_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL1_LEVEL19_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL1_LEVEL20_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL1_LEVEL20_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL1_LEVEL20_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL1_LEVEL20_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL1_LEVEL24_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL1_LEVEL24_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL1_LEVEL24_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL1_LEVEL24_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL1_LEVEL25_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL1_LEVEL25_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL1 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL2_LEVEL2_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL2_LEVEL2_LEVEL4
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL4 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL2_LEVEL3_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL2_LEVEL3_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL2_LEVEL4_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL2_LEVEL4_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL2_LEVEL8_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL2_LEVEL8_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL2_LEVEL9_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL2_LEVEL9_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL2_LEVEL10_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL2_LEVEL10_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL2_LEVEL13_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL2_LEVEL13_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL2_LEVEL15_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL2_LEVEL15_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL2_LEVEL16_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL2_LEVEL16_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL2_LEVEL17_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL2_LEVEL19_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL2_LEVEL19_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL2_LEVEL20_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL2_LEVEL20_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL2_LEVEL20_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL2_LEVEL20_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL2_LEVEL24_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL2_LEVEL24_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL2_LEVEL24_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL2_LEVEL24_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL2_LEVEL25_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL2_LEVEL25_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL2 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL3_LEVEL2_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL3_LEVEL2_LEVEL5
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL5 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL3_LEVEL3_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL3_LEVEL3_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL3_LEVEL4_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL3_LEVEL4_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL3_LEVEL8_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL3_LEVEL8_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL3_LEVEL9_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL3_LEVEL9_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL3_LEVEL10_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL3_LEVEL10_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL3_LEVEL13_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL3_LEVEL13_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL3_LEVEL15_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL3_LEVEL15_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL3_LEVEL16_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL3_LEVEL16_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL3_LEVEL17_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL3_LEVEL19_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL3_LEVEL19_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL3_LEVEL20_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL3_LEVEL20_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL3_LEVEL20_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL3_LEVEL20_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL3_LEVEL24_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL3_LEVEL24_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL3_LEVEL24_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL3_LEVEL24_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL3_LEVEL25_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL3_LEVEL25_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL3 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL4_LEVEL2_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL4_LEVEL2_LEVEL6
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL6 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL4_LEVEL3_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL4_LEVEL3_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL4_LEVEL4_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL4_LEVEL4_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL4_LEVEL8_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL4_LEVEL8_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL4_LEVEL9_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL4_LEVEL9_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL4_LEVEL10_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL4_LEVEL10_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL4_LEVEL13_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL4_LEVEL13_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL4_LEVEL15_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL4_LEVEL15_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL4_LEVEL16_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL4_LEVEL16_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL4_LEVEL17_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL4_LEVEL19_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL4_LEVEL19_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL4_LEVEL20_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL4_LEVEL20_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL4_LEVEL20_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL4_LEVEL20_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL4_LEVEL24_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL4_LEVEL24_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL4_LEVEL24_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL4_LEVEL24_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL4_LEVEL25_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL4_LEVEL25_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL4 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL5_LEVEL2_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL5_LEVEL2_LEVEL7
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL7 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL5_LEVEL3_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL5_LEVEL3_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL5_LEVEL4_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL5_LEVEL4_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL5_LEVEL8_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL5_LEVEL8_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL5_LEVEL9_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL5_LEVEL9_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL5_LEVEL10_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL5_LEVEL10_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL5_LEVEL13_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL5_LEVEL13_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL5_LEVEL15_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL5_LEVEL15_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL5_LEVEL16_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL5_LEVEL16_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL5_LEVEL17_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL5_LEVEL19_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL5_LEVEL19_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL5_LEVEL20_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL5_LEVEL20_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL5_LEVEL20_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL5_LEVEL20_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL5_LEVEL24_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL5_LEVEL24_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL5_LEVEL24_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL5_LEVEL24_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL5_LEVEL25_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL5_LEVEL25_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL5 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL6_LEVEL2_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL6_LEVEL2_LEVEL8
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL8 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL6_LEVEL3_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL6_LEVEL3_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL6_LEVEL4_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL6_LEVEL4_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL6_LEVEL8_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL6_LEVEL8_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL6_LEVEL9_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL6_LEVEL9_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL6_LEVEL10_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL6_LEVEL10_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL6_LEVEL13_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL6_LEVEL13_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL6_LEVEL15_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL6_LEVEL15_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL6_LEVEL16_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL6_LEVEL16_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL6_LEVEL17_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL6_LEVEL19_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL6_LEVEL19_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL6_LEVEL20_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL6_LEVEL20_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL6_LEVEL20_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL6_LEVEL20_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL6_LEVEL24_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL6_LEVEL24_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL6_LEVEL24_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL6_LEVEL24_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL6_LEVEL25_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL6_LEVEL25_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL6 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL7_LEVEL2_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL7_LEVEL2_LEVEL9
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL9 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL7_LEVEL3_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL7_LEVEL3_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL7_LEVEL4_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL7_LEVEL4_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL7_LEVEL8_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL7_LEVEL8_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL7_LEVEL9_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL7_LEVEL9_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL7_LEVEL10_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL7_LEVEL10_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL7_LEVEL13_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL7_LEVEL13_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL7_LEVEL15_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL7_LEVEL15_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL7_LEVEL16_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL7_LEVEL16_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL7_LEVEL17_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL7_LEVEL19_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL7_LEVEL19_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL7_LEVEL20_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL7_LEVEL20_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL7_LEVEL20_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL7_LEVEL20_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL7_LEVEL24_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL7_LEVEL24_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL7_LEVEL24_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL7_LEVEL24_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL7_LEVEL25_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL7_LEVEL25_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL7 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL8_LEVEL2_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL8_LEVEL2_LEVEL10
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL10 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL8_LEVEL3_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL8_LEVEL3_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL8_LEVEL4_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL8_LEVEL4_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL8_LEVEL8_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL8_LEVEL8_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL8_LEVEL9_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL8_LEVEL9_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL8_LEVEL10_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL8_LEVEL10_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL8_LEVEL13_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL8_LEVEL13_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL8_LEVEL15_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL8_LEVEL15_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL8_LEVEL16_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL8_LEVEL16_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL8_LEVEL17_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL8_LEVEL19_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL8_LEVEL19_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL8_LEVEL20_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL8_LEVEL20_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL8_LEVEL20_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL8_LEVEL20_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL8_LEVEL24_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL8_LEVEL24_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL8_LEVEL24_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL8_LEVEL24_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL8_LEVEL25_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL8_LEVEL25_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL8 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL9_LEVEL2_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL9_LEVEL2_LEVEL11
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL11 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL9_LEVEL3_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL9_LEVEL3_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL9_LEVEL4_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL9_LEVEL4_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL9_LEVEL8_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL9_LEVEL8_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL9_LEVEL9_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL9_LEVEL9_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL9_LEVEL10_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL9_LEVEL10_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL9_LEVEL13_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL9_LEVEL13_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL9_LEVEL15_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL9_LEVEL15_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL9_LEVEL16_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL9_LEVEL16_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL9_LEVEL17_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL9_LEVEL19_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL9_LEVEL19_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL9_LEVEL20_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL9_LEVEL20_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL9_LEVEL20_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL9_LEVEL20_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL9_LEVEL24_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL9_LEVEL24_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL9_LEVEL24_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL9_LEVEL24_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL9_LEVEL25_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL9_LEVEL25_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL9 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL10_LEVEL2_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL10_LEVEL2_LEVEL12
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL12 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL10_LEVEL3_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL10_LEVEL3_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL10_LEVEL4_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL10_LEVEL4_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL10_LEVEL8_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL10_LEVEL8_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL10_LEVEL9_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL10_LEVEL9_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL10_LEVEL10_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL10_LEVEL10_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL10_LEVEL13_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL10_LEVEL13_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL10_LEVEL15_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL10_LEVEL15_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL10_LEVEL16_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL10_LEVEL16_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL10_LEVEL17_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL10_LEVEL19_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL10_LEVEL19_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL10_LEVEL20_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL10_LEVEL20_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL10_LEVEL20_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL10_LEVEL20_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL10_LEVEL24_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL10_LEVEL24_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL10_LEVEL24_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL10_LEVEL24_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL10_LEVEL25_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL10_LEVEL25_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL10 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL11_LEVEL2_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL11_LEVEL2_LEVEL13
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL13 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL11_LEVEL3_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL11_LEVEL3_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL11_LEVEL4_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL11_LEVEL4_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL11_LEVEL8_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL11_LEVEL8_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL11_LEVEL9_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL11_LEVEL9_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL11_LEVEL10_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL11_LEVEL10_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL11_LEVEL13_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL11_LEVEL13_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL11_LEVEL15_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL11_LEVEL15_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL11_LEVEL16_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL11_LEVEL16_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL11_LEVEL17_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL11_LEVEL19_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL11_LEVEL19_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL11_LEVEL20_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL11_LEVEL20_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL11_LEVEL20_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL11_LEVEL20_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL11_LEVEL24_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL11_LEVEL24_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL11_LEVEL24_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL11_LEVEL24_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL11_LEVEL25_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL11_LEVEL25_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL11 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL12_LEVEL2_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL12_LEVEL2_LEVEL14
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL14 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL12_LEVEL3_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL12_LEVEL3_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL12_LEVEL4_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL12_LEVEL4_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL12_LEVEL8_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL12_LEVEL8_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL12_LEVEL9_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL12_LEVEL9_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL12_LEVEL10_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL12_LEVEL10_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL12_LEVEL13_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL12_LEVEL13_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL12_LEVEL15_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL12_LEVEL15_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL12_LEVEL16_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL12_LEVEL16_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL12_LEVEL17_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL12_LEVEL19_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL12_LEVEL19_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL12_LEVEL20_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL12_LEVEL20_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL12_LEVEL20_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL12_LEVEL20_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL12_LEVEL24_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL12_LEVEL24_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL12_LEVEL24_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL12_LEVEL24_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL12_LEVEL25_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL12_LEVEL25_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL12 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL13_LEVEL2_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL13_LEVEL2_LEVEL15
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL15 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL13_LEVEL3_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL13_LEVEL3_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL13_LEVEL4_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL13_LEVEL4_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL13_LEVEL8_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL13_LEVEL8_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL13_LEVEL9_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL13_LEVEL9_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL13_LEVEL10_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL13_LEVEL10_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL13_LEVEL13_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL13_LEVEL13_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL13_LEVEL15_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL13_LEVEL15_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL13_LEVEL16_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL13_LEVEL16_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL13_LEVEL17_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL13_LEVEL19_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL13_LEVEL19_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL13_LEVEL20_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL13_LEVEL20_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL13_LEVEL20_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL13_LEVEL20_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL13_LEVEL24_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL13_LEVEL24_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL13_LEVEL24_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL13_LEVEL24_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL13_LEVEL25_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL13_LEVEL25_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL13 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL14_LEVEL2_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL14_LEVEL2_LEVEL16
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL16 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL14_LEVEL3_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL14_LEVEL3_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL14_LEVEL4_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL14_LEVEL4_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL14_LEVEL8_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL14_LEVEL8_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL14_LEVEL9_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL14_LEVEL9_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL14_LEVEL10_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL14_LEVEL10_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL14_LEVEL13_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL14_LEVEL13_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL14_LEVEL15_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL14_LEVEL15_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL14_LEVEL16_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL14_LEVEL16_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL14_LEVEL17_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL14_LEVEL19_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL14_LEVEL19_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL14_LEVEL20_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL14_LEVEL20_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL14_LEVEL20_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL14_LEVEL20_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL14_LEVEL24_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL14_LEVEL24_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL14_LEVEL24_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL14_LEVEL24_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL14_LEVEL25_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL14_LEVEL25_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL14 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL15_LEVEL2_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL15_LEVEL2_LEVEL17
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL17 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL15_LEVEL3_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL15_LEVEL3_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL15_LEVEL4_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL15_LEVEL4_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL15_LEVEL8_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL15_LEVEL8_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL15_LEVEL9_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL15_LEVEL9_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL15_LEVEL10_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL15_LEVEL10_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL15_LEVEL13_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL15_LEVEL13_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL15_LEVEL15_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL15_LEVEL15_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL15_LEVEL16_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL15_LEVEL16_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL15_LEVEL17_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL15_LEVEL19_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL15_LEVEL19_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL15_LEVEL20_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL15_LEVEL20_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL15_LEVEL20_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL15_LEVEL20_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL15_LEVEL24_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL15_LEVEL24_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL15_LEVEL24_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL15_LEVEL24_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL15_LEVEL25_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL15_LEVEL25_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL15 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL16_LEVEL2_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL16_LEVEL2_LEVEL18
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL18 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL16_LEVEL3_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL16_LEVEL3_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL16_LEVEL4_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL16_LEVEL4_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL16_LEVEL8_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL16_LEVEL8_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL16_LEVEL9_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL16_LEVEL9_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL16_LEVEL10_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL16_LEVEL10_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL16_LEVEL13_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL16_LEVEL13_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL16_LEVEL15_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL16_LEVEL15_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL16_LEVEL16_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL16_LEVEL16_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL16_LEVEL17_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL16_LEVEL19_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL16_LEVEL19_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL16_LEVEL20_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL16_LEVEL20_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL16_LEVEL20_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL16_LEVEL20_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL16_LEVEL24_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL16_LEVEL24_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL16_LEVEL24_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL16_LEVEL24_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL16_LEVEL25_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL16_LEVEL25_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL16 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL17_LEVEL2_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL17_LEVEL2_LEVEL19
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL19 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL17_LEVEL3_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL17_LEVEL3_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL17_LEVEL4_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL17_LEVEL4_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL17_LEVEL8_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL17_LEVEL8_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL17_LEVEL9_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL17_LEVEL9_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL17_LEVEL10_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL17_LEVEL10_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL17_LEVEL13_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL17_LEVEL13_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL17_LEVEL15_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL17_LEVEL15_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL17_LEVEL16_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL17_LEVEL16_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL17_LEVEL17_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL17_LEVEL19_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL17_LEVEL19_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL17_LEVEL20_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL17_LEVEL20_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL17_LEVEL20_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL17_LEVEL20_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL17_LEVEL24_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL17_LEVEL24_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL17_LEVEL24_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL17_LEVEL24_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL17_LEVEL25_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL17_LEVEL25_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL17 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL18_LEVEL2_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL18_LEVEL2_LEVEL20
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL20 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL18_LEVEL3_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL18_LEVEL3_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL18_LEVEL4_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL18_LEVEL4_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL18_LEVEL8_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL18_LEVEL8_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL18_LEVEL9_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL18_LEVEL9_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL18_LEVEL10_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL18_LEVEL10_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL18_LEVEL13_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL18_LEVEL13_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL18_LEVEL15_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL18_LEVEL15_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL18_LEVEL16_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL18_LEVEL16_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL18_LEVEL17_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL18_LEVEL19_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL18_LEVEL19_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL18_LEVEL20_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL18_LEVEL20_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL18_LEVEL20_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL18_LEVEL20_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL18_LEVEL24_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL18_LEVEL24_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL18_LEVEL24_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL18_LEVEL24_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL18_LEVEL25_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL18_LEVEL25_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL18 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL19_LEVEL2_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL19_LEVEL2_LEVEL21
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL21 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL19_LEVEL3_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL19_LEVEL3_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL19_LEVEL4_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL19_LEVEL4_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL19_LEVEL8_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL19_LEVEL8_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL19_LEVEL9_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL19_LEVEL9_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL19_LEVEL10_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL19_LEVEL10_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL19_LEVEL13_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL19_LEVEL13_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL19_LEVEL15_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL19_LEVEL15_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL19_LEVEL16_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL19_LEVEL16_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL19_LEVEL17_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL19_LEVEL19_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL19_LEVEL19_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL19_LEVEL20_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL19_LEVEL20_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL19_LEVEL20_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL19_LEVEL20_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL19_LEVEL24_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL19_LEVEL24_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL19_LEVEL24_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL19_LEVEL24_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL19_LEVEL25_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL19_LEVEL25_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL19 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL20_LEVEL2_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL20_LEVEL2_LEVEL22
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL22 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL20_LEVEL3_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL20_LEVEL3_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL20_LEVEL4_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL20_LEVEL4_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL20_LEVEL8_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL20_LEVEL8_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL20_LEVEL9_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL20_LEVEL9_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL20_LEVEL10_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL20_LEVEL10_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL20_LEVEL13_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL20_LEVEL13_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL20_LEVEL15_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL20_LEVEL15_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL20_LEVEL16_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL20_LEVEL16_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL20_LEVEL17_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL20_LEVEL19_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL20_LEVEL19_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL20_LEVEL20_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL20_LEVEL20_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL20_LEVEL20_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL20_LEVEL20_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL20_LEVEL24_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL20_LEVEL24_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL20_LEVEL24_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL20_LEVEL24_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL20_LEVEL25_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL20_LEVEL25_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL20 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL21_LEVEL2_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL21_LEVEL2_LEVEL23
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL23 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL21_LEVEL3_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL21_LEVEL3_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL21_LEVEL4_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL21_LEVEL4_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL21_LEVEL8_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL21_LEVEL8_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL21_LEVEL9_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL21_LEVEL9_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL21_LEVEL10_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL21_LEVEL10_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL21_LEVEL13_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL21_LEVEL13_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL21_LEVEL15_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL21_LEVEL15_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL21_LEVEL16_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL21_LEVEL16_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL21_LEVEL17_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL21_LEVEL19_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL21_LEVEL19_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL21_LEVEL20_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL21_LEVEL20_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL21_LEVEL20_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL21_LEVEL20_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL21_LEVEL24_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL21_LEVEL24_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL21_LEVEL24_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL21_LEVEL24_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL21_LEVEL25_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL21_LEVEL25_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL21 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL22_LEVEL2_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL22_LEVEL2_LEVEL24
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL24 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL22_LEVEL3_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL22_LEVEL3_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL22_LEVEL4_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL22_LEVEL4_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL22_LEVEL8_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL22_LEVEL8_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL22_LEVEL9_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL22_LEVEL9_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL22_LEVEL10_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL22_LEVEL10_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL22_LEVEL13_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL22_LEVEL13_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL22_LEVEL15_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL22_LEVEL15_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL22_LEVEL16_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL22_LEVEL16_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL22_LEVEL17_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL22_LEVEL19_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL22_LEVEL19_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL22_LEVEL20_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL22_LEVEL20_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL22_LEVEL20_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL22_LEVEL20_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL22_LEVEL24_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL22_LEVEL24_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL22_LEVEL24_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL22_LEVEL24_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL22_LEVEL25_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL22_LEVEL25_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL22 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL23_LEVEL2_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL23_LEVEL2_LEVEL25
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL25 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL23_LEVEL3_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL23_LEVEL3_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL23_LEVEL4_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL23_LEVEL4_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL23_LEVEL8_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL23_LEVEL8_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL23_LEVEL9_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL23_LEVEL9_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL23_LEVEL10_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL23_LEVEL10_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL23_LEVEL13_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL23_LEVEL13_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL23_LEVEL15_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL23_LEVEL15_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL23_LEVEL16_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL23_LEVEL16_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL23_LEVEL17_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL23_LEVEL19_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL23_LEVEL19_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL23_LEVEL20_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL23_LEVEL20_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL23_LEVEL20_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL23_LEVEL20_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL23_LEVEL24_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL23_LEVEL24_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL23_LEVEL24_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL23_LEVEL24_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL23_LEVEL25_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL23_LEVEL25_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL23 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL24_LEVEL2_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL24_LEVEL2_LEVEL26
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL26 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL24_LEVEL3_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL24_LEVEL3_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL24_LEVEL4_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL24_LEVEL4_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL24_LEVEL8_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL24_LEVEL8_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL24_LEVEL9_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL24_LEVEL9_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL24_LEVEL10_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL24_LEVEL10_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL24_LEVEL13_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL24_LEVEL13_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL24_LEVEL15_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL24_LEVEL15_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL24_LEVEL16_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL24_LEVEL16_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL24_LEVEL17_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL24_LEVEL19_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL24_LEVEL19_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL24_LEVEL20_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL24_LEVEL20_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL24_LEVEL20_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL24_LEVEL20_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL24_LEVEL24_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL24_LEVEL24_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL24_LEVEL24_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL24_LEVEL24_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL24_LEVEL25_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL24_LEVEL25_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL24 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL25_LEVEL2_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL25_LEVEL2_LEVEL27
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL27 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL25_LEVEL3_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL25_LEVEL3_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL25_LEVEL4_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL25_LEVEL4_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL25_LEVEL8_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL25_LEVEL8_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL25_LEVEL9_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL25_LEVEL9_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL25_LEVEL10_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL25_LEVEL10_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL25_LEVEL13_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL25_LEVEL13_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL25_LEVEL15_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL25_LEVEL15_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL25_LEVEL16_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL25_LEVEL16_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL25_LEVEL17_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL25_LEVEL19_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL25_LEVEL19_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL25_LEVEL20_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL25_LEVEL20_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL25_LEVEL20_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL25_LEVEL20_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL25_LEVEL24_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL25_LEVEL24_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL25_LEVEL24_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL25_LEVEL24_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL25_LEVEL25_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL25_LEVEL25_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL25 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL26_LEVEL2_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL26_LEVEL2_LEVEL28
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL28 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL26_LEVEL3_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL26_LEVEL3_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL26_LEVEL4_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL26_LEVEL4_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL26_LEVEL8_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL26_LEVEL8_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL26_LEVEL9_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL26_LEVEL9_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL26_LEVEL10_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL26_LEVEL10_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL26_LEVEL13_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL26_LEVEL13_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL26_LEVEL15_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL26_LEVEL15_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL26_LEVEL16_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL26_LEVEL16_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL26_LEVEL17_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL26_LEVEL19_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL26_LEVEL19_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL26_LEVEL20_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL26_LEVEL20_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL26_LEVEL20_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL26_LEVEL20_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL26_LEVEL24_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL26_LEVEL24_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL26_LEVEL24_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL26_LEVEL24_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL26_LEVEL25_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL26_LEVEL25_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL26 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL27_LEVEL2_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL27_LEVEL2_LEVEL29
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL29 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL27_LEVEL3_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL27_LEVEL3_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL27_LEVEL4_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL27_LEVEL4_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL27_LEVEL8_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL27_LEVEL8_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL27_LEVEL9_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL27_LEVEL9_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL27_LEVEL10_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL27_LEVEL10_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL27_LEVEL13_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL27_LEVEL13_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL27_LEVEL15_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL27_LEVEL15_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL27_LEVEL16_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL27_LEVEL16_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL27_LEVEL17_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL27_LEVEL19_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL27_LEVEL19_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL27_LEVEL20_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL27_LEVEL20_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL27_LEVEL20_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL27_LEVEL20_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL27_LEVEL24_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL27_LEVEL24_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL27_LEVEL24_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL27_LEVEL24_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL27_LEVEL25_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL27_LEVEL25_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL27 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL28_LEVEL2_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL28_LEVEL2_LEVEL30
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL30 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL28_LEVEL3_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL28_LEVEL3_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL28_LEVEL4_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL28_LEVEL4_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL28_LEVEL8_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL28_LEVEL8_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL28_LEVEL9_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL28_LEVEL9_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL28_LEVEL10_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL28_LEVEL10_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL28_LEVEL13_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL28_LEVEL13_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL28_LEVEL15_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL28_LEVEL15_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL28_LEVEL16_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL28_LEVEL16_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL28_LEVEL17_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL28_LEVEL19_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL28_LEVEL19_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL28_LEVEL20_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL28_LEVEL20_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL28_LEVEL20_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL28_LEVEL20_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL28_LEVEL24_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL28_LEVEL24_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL28_LEVEL24_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL28_LEVEL24_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL28_LEVEL25_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL28_LEVEL25_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL28 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL29_LEVEL2_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL29_LEVEL2_LEVEL31
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL31 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL29_LEVEL3_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL29_LEVEL3_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL29_LEVEL4_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL29_LEVEL4_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL29_LEVEL8_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL29_LEVEL8_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL29_LEVEL9_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL29_LEVEL9_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL29_LEVEL10_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL29_LEVEL10_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL29_LEVEL13_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL29_LEVEL13_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL29_LEVEL15_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL29_LEVEL15_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL29_LEVEL16_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL29_LEVEL16_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL29_LEVEL17_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL29_LEVEL19_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL29_LEVEL19_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL29_LEVEL20_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL29_LEVEL20_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL29_LEVEL20_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL29_LEVEL20_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL29_LEVEL24_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL29_LEVEL24_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL29_LEVEL24_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL29_LEVEL24_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL29_LEVEL25_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL29_LEVEL25_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL29 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL30_LEVEL2_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL30_LEVEL2_LEVEL32
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL32 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL30_LEVEL3_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL30_LEVEL3_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL30_LEVEL4_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL30_LEVEL4_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL30_LEVEL8_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL30_LEVEL8_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL30_LEVEL9_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL30_LEVEL9_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL30_LEVEL10_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL30_LEVEL10_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL30_LEVEL13_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL30_LEVEL13_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL30_LEVEL15_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL30_LEVEL15_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL30_LEVEL16_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL30_LEVEL16_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL30_LEVEL17_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL30_LEVEL19_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL30_LEVEL19_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL30_LEVEL20_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL30_LEVEL20_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL30_LEVEL20_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL30_LEVEL20_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL30_LEVEL24_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL30_LEVEL24_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL30_LEVEL24_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL30_LEVEL24_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL30_LEVEL25_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL30_LEVEL25_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL30 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL31_LEVEL2_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL31_LEVEL2_LEVEL33
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL33 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL31_LEVEL3_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL31_LEVEL3_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL31_LEVEL4_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL31_LEVEL4_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL31_LEVEL8_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL31_LEVEL8_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL31_LEVEL9_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL31_LEVEL9_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL31_LEVEL10_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL31_LEVEL10_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL31_LEVEL13_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL31_LEVEL13_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL31_LEVEL15_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL31_LEVEL15_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL31_LEVEL16_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL31_LEVEL16_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL31_LEVEL17_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL31_LEVEL19_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL31_LEVEL19_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL31_LEVEL20_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL31_LEVEL20_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL31_LEVEL20_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL31_LEVEL20_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL31_LEVEL24_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL31_LEVEL24_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL31_LEVEL24_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL31_LEVEL24_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL31_LEVEL25_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL31_LEVEL25_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL31 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL32_LEVEL2_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL32_LEVEL2_LEVEL34
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL34 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL32_LEVEL3_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL32_LEVEL3_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL32_LEVEL4_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL32_LEVEL4_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL32_LEVEL8_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL32_LEVEL8_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL32_LEVEL9_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL32_LEVEL9_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL32_LEVEL10_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL32_LEVEL10_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL32_LEVEL13_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL32_LEVEL13_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL32_LEVEL15_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL32_LEVEL15_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL32_LEVEL16_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL32_LEVEL16_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL32_LEVEL17_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL32_LEVEL19_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL32_LEVEL19_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL32_LEVEL20_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL32_LEVEL20_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL32_LEVEL20_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL32_LEVEL20_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL32_LEVEL24_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL32_LEVEL24_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL32_LEVEL24_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL32_LEVEL24_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL32_LEVEL25_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL32_LEVEL25_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL32 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL33_LEVEL2_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL33_LEVEL2_LEVEL35
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL35 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL33_LEVEL3_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL33_LEVEL3_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL33_LEVEL4_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL33_LEVEL4_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL33_LEVEL8_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL33_LEVEL8_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL33_LEVEL9_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL33_LEVEL9_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL33_LEVEL10_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL33_LEVEL10_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL33_LEVEL13_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL33_LEVEL13_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL33_LEVEL15_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL33_LEVEL15_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL33_LEVEL16_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL33_LEVEL16_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL33_LEVEL17_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL33_LEVEL19_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL33_LEVEL19_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL33_LEVEL20_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL33_LEVEL20_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL33_LEVEL20_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL33_LEVEL20_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL33_LEVEL24_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL33_LEVEL24_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL33_LEVEL24_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL33_LEVEL24_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL33_LEVEL25_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL33_LEVEL25_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL33 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL34_LEVEL2_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL34_LEVEL2_LEVEL36
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL36 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL34_LEVEL3_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL34_LEVEL3_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL34_LEVEL4_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL34_LEVEL4_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL34_LEVEL8_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL34_LEVEL8_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL34_LEVEL9_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL34_LEVEL9_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL34_LEVEL10_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL34_LEVEL10_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL34_LEVEL13_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL34_LEVEL13_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL34_LEVEL15_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL34_LEVEL15_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL34_LEVEL16_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL34_LEVEL16_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL34_LEVEL17_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL34_LEVEL19_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL34_LEVEL19_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL34_LEVEL20_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL34_LEVEL20_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL34_LEVEL20_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL34_LEVEL20_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL34_LEVEL24_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL34_LEVEL24_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL34_LEVEL24_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL34_LEVEL24_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL34_LEVEL25_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL34_LEVEL25_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL34 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL35_LEVEL2_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL35_LEVEL2_LEVEL37
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL37 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL35_LEVEL3_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL35_LEVEL3_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL35_LEVEL4_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL35_LEVEL4_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL35_LEVEL8_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL35_LEVEL8_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL35_LEVEL9_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL35_LEVEL9_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL35_LEVEL10_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL35_LEVEL10_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL35_LEVEL13_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL35_LEVEL13_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL35_LEVEL15_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL35_LEVEL15_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL35_LEVEL16_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL35_LEVEL16_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL35_LEVEL17_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL35_LEVEL19_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL35_LEVEL19_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL35_LEVEL20_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL35_LEVEL20_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL35_LEVEL20_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL35_LEVEL20_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL35_LEVEL24_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL35_LEVEL24_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL35_LEVEL24_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL35_LEVEL24_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL35_LEVEL25_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL35_LEVEL25_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL35 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL36_LEVEL2_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL36_LEVEL2_LEVEL38
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL38 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL36_LEVEL3_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL36_LEVEL3_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL36_LEVEL4_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL36_LEVEL4_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL36_LEVEL8_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL36_LEVEL8_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL36_LEVEL9_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL36_LEVEL9_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL36_LEVEL10_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL36_LEVEL10_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL36_LEVEL13_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL36_LEVEL13_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL36_LEVEL15_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL36_LEVEL15_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL36_LEVEL16_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL36_LEVEL16_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL36_LEVEL17_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL36_LEVEL19_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL36_LEVEL19_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL36_LEVEL20_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL36_LEVEL20_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL36_LEVEL20_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL36_LEVEL20_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL36_LEVEL24_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL36_LEVEL24_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL36_LEVEL24_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL36_LEVEL24_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL36_LEVEL25_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL36_LEVEL25_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL36 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL37_LEVEL2_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL37_LEVEL2_LEVEL39
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL39 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL37_LEVEL3_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL37_LEVEL3_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL37_LEVEL4_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL37_LEVEL4_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL37_LEVEL8_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL37_LEVEL8_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL37_LEVEL9_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL37_LEVEL9_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL37_LEVEL10_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL37_LEVEL10_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL37_LEVEL13_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL37_LEVEL13_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL37_LEVEL15_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL37_LEVEL15_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL37_LEVEL16_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL37_LEVEL16_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL37_LEVEL17_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL37_LEVEL19_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL37_LEVEL19_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL37_LEVEL20_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL37_LEVEL20_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL37_LEVEL20_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL37_LEVEL20_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL37_LEVEL24_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL37_LEVEL24_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL37_LEVEL24_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL37_LEVEL24_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL37_LEVEL25_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL37_LEVEL25_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL37 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL38_LEVEL2_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL38_LEVEL2_LEVEL40
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL40 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL38_LEVEL3_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL38_LEVEL3_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL38_LEVEL4_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL38_LEVEL4_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL38_LEVEL8_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL38_LEVEL8_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL38_LEVEL9_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL38_LEVEL9_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL38_LEVEL10_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL38_LEVEL10_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL38_LEVEL13_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL38_LEVEL13_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL38_LEVEL15_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL38_LEVEL15_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL38_LEVEL16_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL38_LEVEL16_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL38_LEVEL17_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL38_LEVEL19_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL38_LEVEL19_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL38_LEVEL20_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL38_LEVEL20_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL38_LEVEL20_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL38_LEVEL20_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL38_LEVEL24_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL38_LEVEL24_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL38_LEVEL24_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL38_LEVEL24_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL38_LEVEL25_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL38_LEVEL25_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL38 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL39_LEVEL2_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL39_LEVEL2_LEVEL41
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL41 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL39_LEVEL3_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL39_LEVEL3_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL39_LEVEL4_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL39_LEVEL4_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL39_LEVEL8_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL39_LEVEL8_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL39_LEVEL9_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL39_LEVEL9_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL39_LEVEL10_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL39_LEVEL10_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL39_LEVEL13_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL39_LEVEL13_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL39_LEVEL15_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL39_LEVEL15_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL39_LEVEL16_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL39_LEVEL16_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL39_LEVEL17_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL39_LEVEL19_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL39_LEVEL19_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL39_LEVEL20_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL39_LEVEL20_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL39_LEVEL20_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL39_LEVEL20_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL39_LEVEL24_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL39_LEVEL24_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL39_LEVEL24_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL39_LEVEL24_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL39_LEVEL25_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL39_LEVEL25_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL39 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL40_LEVEL2_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL40_LEVEL2_LEVEL42
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL42 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL40_LEVEL3_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL40_LEVEL3_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL40_LEVEL4_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL40_LEVEL4_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL40_LEVEL8_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL40_LEVEL8_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL40_LEVEL9_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL40_LEVEL9_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL40_LEVEL10_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL40_LEVEL10_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL40_LEVEL13_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL40_LEVEL13_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL40_LEVEL15_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL40_LEVEL15_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL40_LEVEL16_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL40_LEVEL16_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL40_LEVEL17_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL40_LEVEL19_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL40_LEVEL19_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL40_LEVEL20_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL40_LEVEL20_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL40_LEVEL20_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL40_LEVEL20_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL40_LEVEL24_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL40_LEVEL24_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL40_LEVEL24_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL40_LEVEL24_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL40_LEVEL25_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL40_LEVEL25_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL40 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL41_LEVEL2_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL41_LEVEL2_LEVEL43
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL43 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL41_LEVEL3_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL41_LEVEL3_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL41_LEVEL4_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL41_LEVEL4_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL41_LEVEL8_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL41_LEVEL8_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL41_LEVEL9_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL41_LEVEL9_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL41_LEVEL10_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL41_LEVEL10_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL41_LEVEL13_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL41_LEVEL13_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL41_LEVEL15_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL41_LEVEL15_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL41_LEVEL16_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL41_LEVEL16_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL41_LEVEL17_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL41_LEVEL19_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL41_LEVEL19_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL41_LEVEL20_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL41_LEVEL20_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL41_LEVEL20_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL41_LEVEL20_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL41_LEVEL24_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL41_LEVEL24_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL41_LEVEL24_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL41_LEVEL24_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL41_LEVEL25_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL41_LEVEL25_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL41 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL42_LEVEL2_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL42_LEVEL2_LEVEL44
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL44 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL42_LEVEL3_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL42_LEVEL3_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL42_LEVEL4_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL42_LEVEL4_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL42_LEVEL8_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL42_LEVEL8_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL42_LEVEL9_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL42_LEVEL9_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL42_LEVEL10_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL42_LEVEL10_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL42_LEVEL13_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL42_LEVEL13_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL42_LEVEL15_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL42_LEVEL15_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL42_LEVEL16_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL42_LEVEL16_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL42_LEVEL17_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL42_LEVEL19_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL42_LEVEL19_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL42_LEVEL20_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL42_LEVEL20_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL42_LEVEL20_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL42_LEVEL20_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL42_LEVEL24_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL42_LEVEL24_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL42_LEVEL24_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL42_LEVEL24_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL42_LEVEL25_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL42_LEVEL25_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL42 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL43_LEVEL2_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL43_LEVEL2_LEVEL45
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL45 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL43_LEVEL3_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL43_LEVEL3_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL43_LEVEL4_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL43_LEVEL4_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL43_LEVEL8_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL43_LEVEL8_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL43_LEVEL9_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL43_LEVEL9_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL43_LEVEL10_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL43_LEVEL10_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL43_LEVEL13_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL43_LEVEL13_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL43_LEVEL15_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL43_LEVEL15_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL43_LEVEL16_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL43_LEVEL16_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL43_LEVEL17_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL43_LEVEL19_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL43_LEVEL19_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL43_LEVEL20_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL43_LEVEL20_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL43_LEVEL20_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL43_LEVEL20_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL43_LEVEL24_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL43_LEVEL24_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL43_LEVEL24_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL43_LEVEL24_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL43_LEVEL25_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL43_LEVEL25_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL43 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL44_LEVEL2_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL44_LEVEL2_LEVEL46
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL46 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL44_LEVEL3_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL44_LEVEL3_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL44_LEVEL4_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL44_LEVEL4_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL44_LEVEL8_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL44_LEVEL8_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL44_LEVEL9_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL44_LEVEL9_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL44_LEVEL10_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL44_LEVEL10_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL44_LEVEL13_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL44_LEVEL13_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL44_LEVEL15_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL44_LEVEL15_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL44_LEVEL16_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL44_LEVEL16_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL44_LEVEL17_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL44_LEVEL19_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL44_LEVEL19_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL44_LEVEL20_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL44_LEVEL20_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL44_LEVEL20_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL44_LEVEL20_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL44_LEVEL24_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL44_LEVEL24_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL44_LEVEL24_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL44_LEVEL24_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL44_LEVEL25_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL44_LEVEL25_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL44 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL45_LEVEL2_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL45_LEVEL2_LEVEL47
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL47 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL45_LEVEL3_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL45_LEVEL3_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL45_LEVEL4_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL45_LEVEL4_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL45_LEVEL8_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL45_LEVEL8_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL45_LEVEL9_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL45_LEVEL9_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL45_LEVEL10_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL45_LEVEL10_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL45_LEVEL13_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL45_LEVEL13_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL45_LEVEL15_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL45_LEVEL15_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL45_LEVEL16_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL45_LEVEL16_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL45_LEVEL17_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL45_LEVEL19_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL45_LEVEL19_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL45_LEVEL20_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL45_LEVEL20_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL45_LEVEL20_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL45_LEVEL20_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL45_LEVEL24_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL45_LEVEL24_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL45_LEVEL24_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL45_LEVEL24_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL45_LEVEL25_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL45_LEVEL25_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL45 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL46_LEVEL2_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL46_LEVEL2_LEVEL48
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL48 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL46_LEVEL3_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL46_LEVEL3_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL46_LEVEL4_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL46_LEVEL4_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL46_LEVEL8_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL46_LEVEL8_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL46_LEVEL9_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL46_LEVEL9_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL46_LEVEL10_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL46_LEVEL10_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL46_LEVEL13_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL46_LEVEL13_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL46_LEVEL15_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL46_LEVEL15_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL46_LEVEL16_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL46_LEVEL16_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL46_LEVEL17_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL46_LEVEL19_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL46_LEVEL19_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL46_LEVEL20_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL46_LEVEL20_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL46_LEVEL20_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL46_LEVEL20_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL46_LEVEL24_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL46_LEVEL24_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL46_LEVEL24_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL46_LEVEL24_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL46_LEVEL25_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL46_LEVEL25_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL46 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL47_LEVEL2_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL47_LEVEL2_LEVEL49
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL49 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL47_LEVEL3_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL47_LEVEL3_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL47_LEVEL4_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL47_LEVEL4_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL47_LEVEL8_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL47_LEVEL8_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL47_LEVEL9_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL47_LEVEL9_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL47_LEVEL10_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL47_LEVEL10_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL47_LEVEL13_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL47_LEVEL13_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL47_LEVEL15_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL47_LEVEL15_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL47_LEVEL16_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL47_LEVEL16_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL47_LEVEL17_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL47_LEVEL19_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL47_LEVEL19_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL47_LEVEL20_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL47_LEVEL20_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL47_LEVEL20_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL47_LEVEL20_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL47_LEVEL24_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL47_LEVEL24_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL47_LEVEL24_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL47_LEVEL24_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL47_LEVEL25_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL47_LEVEL25_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL47 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL48_LEVEL2_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL48_LEVEL2_LEVEL50
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL50 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL48_LEVEL3_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL48_LEVEL3_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL48_LEVEL4_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL48_LEVEL4_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL48_LEVEL8_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL48_LEVEL8_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL48_LEVEL9_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL48_LEVEL9_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL48_LEVEL10_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL48_LEVEL10_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL48_LEVEL13_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL48_LEVEL13_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL48_LEVEL15_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL48_LEVEL15_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL48_LEVEL16_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL48_LEVEL16_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL48_LEVEL17_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL48_LEVEL19_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL48_LEVEL19_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL48_LEVEL20_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL48_LEVEL20_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL48_LEVEL20_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL48_LEVEL20_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL48_LEVEL24_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL48_LEVEL24_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL48_LEVEL24_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL48_LEVEL24_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL48_LEVEL25_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL48_LEVEL25_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL48 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL49_LEVEL2_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL49_LEVEL2_LEVEL51
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL51 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL49_LEVEL3_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL49_LEVEL3_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL49_LEVEL4_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL49_LEVEL4_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL49_LEVEL8_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL49_LEVEL8_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL49_LEVEL9_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL49_LEVEL9_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL49_LEVEL10_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL49_LEVEL10_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL49_LEVEL13_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL49_LEVEL13_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL49_LEVEL15_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL49_LEVEL15_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL49_LEVEL16_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL49_LEVEL16_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL49_LEVEL17_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL49_LEVEL19_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL49_LEVEL19_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL49_LEVEL20_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL49_LEVEL20_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL49_LEVEL20_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL49_LEVEL20_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL49_LEVEL24_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL49_LEVEL24_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL49_LEVEL24_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL49_LEVEL24_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL49_LEVEL25_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL49_LEVEL25_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL49 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL50_LEVEL2_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL50_LEVEL2_LEVEL52
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL52 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL50_LEVEL3_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL50_LEVEL3_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL50_LEVEL4_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL50_LEVEL4_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL50_LEVEL8_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL50_LEVEL8_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL50_LEVEL9_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL50_LEVEL9_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL50_LEVEL10_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL50_LEVEL10_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL50_LEVEL13_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL50_LEVEL13_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL50_LEVEL15_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL50_LEVEL15_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL50_LEVEL16_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL50_LEVEL16_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL50_LEVEL17_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL50_LEVEL19_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL50_LEVEL19_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL50_LEVEL20_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL50_LEVEL20_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL50_LEVEL20_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL50_LEVEL20_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL50_LEVEL24_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL50_LEVEL24_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL50_LEVEL24_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL50_LEVEL24_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL50_LEVEL25_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL50_LEVEL25_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL50 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL51_LEVEL2_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL51_LEVEL2_LEVEL53
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL53 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL51_LEVEL3_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL51_LEVEL3_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL51_LEVEL4_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL51_LEVEL4_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL51_LEVEL8_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL51_LEVEL8_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL51_LEVEL9_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL51_LEVEL9_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL51_LEVEL10_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL51_LEVEL10_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL51_LEVEL13_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL51_LEVEL13_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL51_LEVEL15_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL51_LEVEL15_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL51_LEVEL16_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL51_LEVEL16_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL51_LEVEL17_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL51_LEVEL19_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL51_LEVEL19_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL51_LEVEL20_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL51_LEVEL20_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL51_LEVEL20_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL51_LEVEL20_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL51_LEVEL24_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL51_LEVEL24_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL51_LEVEL24_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL51_LEVEL24_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL51_LEVEL25_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL51_LEVEL25_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL51 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL52_LEVEL2_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL52_LEVEL2_LEVEL54
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL54 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL52_LEVEL3_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL52_LEVEL3_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL52_LEVEL4_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL52_LEVEL4_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL52_LEVEL8_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL52_LEVEL8_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL52_LEVEL9_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL52_LEVEL9_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL52_LEVEL10_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL52_LEVEL10_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL52_LEVEL13_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL52_LEVEL13_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL52_LEVEL15_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL52_LEVEL15_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL52_LEVEL16_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL52_LEVEL16_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL52_LEVEL17_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL52_LEVEL19_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL52_LEVEL19_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL52_LEVEL20_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL52_LEVEL20_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL52_LEVEL20_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL52_LEVEL20_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL52_LEVEL24_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL52_LEVEL24_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL52_LEVEL24_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL52_LEVEL24_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL52_LEVEL25_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL52_LEVEL25_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL52 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL53_LEVEL2_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL53_LEVEL2_LEVEL55
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL55 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL53_LEVEL3_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL53_LEVEL3_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL53_LEVEL4_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL53_LEVEL4_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL53_LEVEL8_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL53_LEVEL8_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL53_LEVEL9_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL53_LEVEL9_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL53_LEVEL10_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL53_LEVEL10_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL53_LEVEL13_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL53_LEVEL13_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL53_LEVEL15_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL53_LEVEL15_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL53_LEVEL16_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL53_LEVEL16_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL53_LEVEL17_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL53_LEVEL19_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL53_LEVEL19_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL53_LEVEL20_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL53_LEVEL20_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL53_LEVEL20_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL53_LEVEL20_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL53_LEVEL24_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL53_LEVEL24_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL53_LEVEL24_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL53_LEVEL24_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL53_LEVEL25_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL53_LEVEL25_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL53 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL54_LEVEL2_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL54_LEVEL2_LEVEL56
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL56 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL54_LEVEL3_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL54_LEVEL3_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL54_LEVEL4_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL54_LEVEL4_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL54_LEVEL8_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL54_LEVEL8_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL54_LEVEL9_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL54_LEVEL9_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL54_LEVEL10_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL54_LEVEL10_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL54_LEVEL13_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL54_LEVEL13_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL54_LEVEL15_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL54_LEVEL15_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL54_LEVEL16_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL54_LEVEL16_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL54_LEVEL17_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL54_LEVEL19_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL54_LEVEL19_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL54_LEVEL20_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL54_LEVEL20_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL54_LEVEL20_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL54_LEVEL20_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL54_LEVEL24_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL54_LEVEL24_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL54_LEVEL24_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL54_LEVEL24_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL54_LEVEL25_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL54_LEVEL25_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL54 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL55_LEVEL2_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL55_LEVEL2_LEVEL57
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL57 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL55_LEVEL3_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL55_LEVEL3_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL55_LEVEL4_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL55_LEVEL4_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL55_LEVEL8_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL55_LEVEL8_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL55_LEVEL9_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL55_LEVEL9_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL55_LEVEL10_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL55_LEVEL10_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL55_LEVEL13_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL55_LEVEL13_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL55_LEVEL15_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL55_LEVEL15_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL55_LEVEL16_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL55_LEVEL16_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL55_LEVEL17_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL55_LEVEL19_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL55_LEVEL19_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL55_LEVEL20_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL55_LEVEL20_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL55_LEVEL20_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL55_LEVEL20_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL55_LEVEL24_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL55_LEVEL24_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL55_LEVEL24_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL55_LEVEL24_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL55_LEVEL25_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL55_LEVEL25_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL55 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL56_LEVEL2_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL56_LEVEL2_LEVEL58
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL58 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL56_LEVEL3_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL56_LEVEL3_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL56_LEVEL4_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL56_LEVEL4_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL56_LEVEL8_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL56_LEVEL8_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL56_LEVEL9_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL56_LEVEL9_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL56_LEVEL10_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL56_LEVEL10_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL56_LEVEL13_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL56_LEVEL13_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL56_LEVEL15_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL56_LEVEL15_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL56_LEVEL16_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL56_LEVEL16_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL56_LEVEL17_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL56_LEVEL19_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL56_LEVEL19_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL56_LEVEL20_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL56_LEVEL20_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL56_LEVEL20_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL56_LEVEL20_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL56_LEVEL24_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL56_LEVEL24_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL56_LEVEL24_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL56_LEVEL24_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL56_LEVEL25_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL56_LEVEL25_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL56 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL57_LEVEL2_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL57_LEVEL2_LEVEL59
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL59 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL57_LEVEL3_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL57_LEVEL3_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL57_LEVEL4_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL57_LEVEL4_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL57_LEVEL8_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL57_LEVEL8_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL57_LEVEL9_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL57_LEVEL9_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL57_LEVEL10_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL57_LEVEL10_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL57_LEVEL13_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL57_LEVEL13_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL57_LEVEL15_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL57_LEVEL15_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL57_LEVEL16_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL57_LEVEL16_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL57_LEVEL17_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL57_LEVEL19_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL57_LEVEL19_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL57_LEVEL20_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL57_LEVEL20_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL57_LEVEL20_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL57_LEVEL20_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL57_LEVEL24_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL57_LEVEL24_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL57_LEVEL24_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL57_LEVEL24_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL57_LEVEL25_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL57_LEVEL25_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL57 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL58_LEVEL2_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL58_LEVEL2_LEVEL60
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL60 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL58_LEVEL3_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL58_LEVEL3_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL58_LEVEL4_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL58_LEVEL4_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL58_LEVEL8_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL58_LEVEL8_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL58_LEVEL9_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL58_LEVEL9_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL58_LEVEL10_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL58_LEVEL10_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL58_LEVEL13_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL58_LEVEL13_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL58_LEVEL15_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL58_LEVEL15_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL58_LEVEL16_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL58_LEVEL16_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL58_LEVEL17_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL58_LEVEL19_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL58_LEVEL19_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL58_LEVEL20_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL58_LEVEL20_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL58_LEVEL20_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL58_LEVEL20_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL58_LEVEL24_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL58_LEVEL24_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL58_LEVEL24_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL58_LEVEL24_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL58_LEVEL25_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL58_LEVEL25_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL58 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL59_LEVEL2_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL59_LEVEL2_LEVEL61
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL61 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL59_LEVEL3_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL59_LEVEL3_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL59_LEVEL4_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL59_LEVEL4_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL59_LEVEL8_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL59_LEVEL8_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL59_LEVEL9_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL59_LEVEL9_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL59_LEVEL10_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL59_LEVEL10_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL59_LEVEL13_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL59_LEVEL13_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL59_LEVEL15_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL59_LEVEL15_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL59_LEVEL16_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL59_LEVEL16_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL59_LEVEL17_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL59_LEVEL19_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL59_LEVEL19_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL59_LEVEL20_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL59_LEVEL20_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL59_LEVEL20_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL59_LEVEL20_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL59_LEVEL24_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL59_LEVEL24_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL59_LEVEL24_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL59_LEVEL24_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL59_LEVEL25_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL59_LEVEL25_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL59 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL60_LEVEL2_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL60_LEVEL2_LEVEL62
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL62 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL60_LEVEL3_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL60_LEVEL3_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL60_LEVEL4_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL60_LEVEL4_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL60_LEVEL8_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL60_LEVEL8_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL60_LEVEL9_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL60_LEVEL9_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL60_LEVEL10_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL60_LEVEL10_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL60_LEVEL13_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL60_LEVEL13_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL60_LEVEL15_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL60_LEVEL15_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL60_LEVEL16_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL60_LEVEL16_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL60_LEVEL17_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL60_LEVEL19_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL60_LEVEL19_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL60_LEVEL20_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL60_LEVEL20_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL60_LEVEL20_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL60_LEVEL20_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL60_LEVEL24_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL60_LEVEL24_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL60_LEVEL24_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL60_LEVEL24_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL60_LEVEL25_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL60_LEVEL25_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL60 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL61_LEVEL2_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL61_LEVEL2_LEVEL63
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL63 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL61_LEVEL3_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL61_LEVEL3_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL61_LEVEL4_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL61_LEVEL4_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL61_LEVEL8_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL61_LEVEL8_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL61_LEVEL9_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL61_LEVEL9_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL61_LEVEL10_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL61_LEVEL10_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL61_LEVEL13_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL61_LEVEL13_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL61_LEVEL15_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL61_LEVEL15_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL61_LEVEL16_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL61_LEVEL16_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL61_LEVEL17_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL61_LEVEL19_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL61_LEVEL19_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL61_LEVEL20_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL61_LEVEL20_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL61_LEVEL20_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL61_LEVEL20_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL61_LEVEL24_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL61_LEVEL24_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL61_LEVEL24_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL61_LEVEL24_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL61 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL62_LEVEL2_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL62_LEVEL2_LEVEL64
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL64 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL62_LEVEL3_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL62_LEVEL3_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL62_LEVEL4_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL62_LEVEL4_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL62_LEVEL8_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL62_LEVEL8_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL62_LEVEL9_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL62_LEVEL9_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL62_LEVEL10_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL62_LEVEL10_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL62_LEVEL13_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL62_LEVEL13_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL62_LEVEL15_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL62_LEVEL15_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL62_LEVEL16_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL62_LEVEL16_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL62_LEVEL17_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL62_LEVEL19_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL62_LEVEL19_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL62_LEVEL20_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL62_LEVEL20_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL62_LEVEL20_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL62_LEVEL20_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL62_LEVEL25_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL62_LEVEL25_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL62 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL63_LEVEL2_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL63_LEVEL2_LEVEL65
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL65 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL63_LEVEL3_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL63_LEVEL3_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL63_LEVEL4_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL63_LEVEL4_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL63_LEVEL8_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL63_LEVEL8_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL63_LEVEL9_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL63_LEVEL9_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL63_LEVEL10_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL63_LEVEL10_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL63_LEVEL13_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL63_LEVEL13_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL63_LEVEL15_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL63_LEVEL15_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL63_LEVEL16_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL63_LEVEL16_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL63_LEVEL17_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL63_LEVEL19_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL63_LEVEL19_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL63_LEVEL20_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL63_LEVEL20_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL63_LEVEL20_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL63_LEVEL20_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL63_LEVEL24_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL63_LEVEL24_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL63_LEVEL24_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL63_LEVEL24_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL63 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL64_LEVEL2_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL64_LEVEL2_LEVEL66
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL66 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL64_LEVEL3_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL64_LEVEL3_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL64_LEVEL4_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL64_LEVEL4_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL64_LEVEL8_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL64_LEVEL8_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL64_LEVEL9_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL64_LEVEL9_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL64_LEVEL10_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL64_LEVEL10_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL64_LEVEL13_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL64_LEVEL13_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL64_LEVEL15_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL64_LEVEL15_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL64_LEVEL16_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL64_LEVEL16_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL64_LEVEL17_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL64_LEVEL19_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL64_LEVEL19_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL64_LEVEL20_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL64_LEVEL20_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL64_LEVEL20_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL64_LEVEL20_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL64 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL65_LEVEL2_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL65_LEVEL2_LEVEL67
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL67 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL65_LEVEL3_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL65_LEVEL3_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL65_LEVEL4_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL65_LEVEL4_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL65_LEVEL8_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL65_LEVEL8_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL65_LEVEL9_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL65_LEVEL9_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL65_LEVEL10_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL65_LEVEL10_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL65_LEVEL13_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL65_LEVEL13_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL65_LEVEL15_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL65_LEVEL15_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL65_LEVEL16_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL65_LEVEL16_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL65_LEVEL17_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL65_LEVEL19_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL65_LEVEL19_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL65_LEVEL20_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL65_LEVEL20_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL65_LEVEL20_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL65_LEVEL20_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL65 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL66_LEVEL2_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL66_LEVEL2_LEVEL68
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL68 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL66_LEVEL3_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL66_LEVEL3_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL66_LEVEL4_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL66_LEVEL4_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL66_LEVEL8_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL66_LEVEL8_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL66_LEVEL9_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL66_LEVEL9_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL66_LEVEL10_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL66_LEVEL10_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL66_LEVEL13_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL66_LEVEL13_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL66_LEVEL15_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL66_LEVEL15_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL66_LEVEL16_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL66_LEVEL16_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL66_LEVEL17_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL66_LEVEL19_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL66_LEVEL19_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL66_LEVEL25_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL66_LEVEL25_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL66 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL67_LEVEL2_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL67_LEVEL2_LEVEL69
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL69 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL67_LEVEL3_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL67_LEVEL3_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL67_LEVEL4_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL67_LEVEL4_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL67_LEVEL8_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL67_LEVEL8_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL67_LEVEL9_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL67_LEVEL9_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL67_LEVEL10_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL67_LEVEL10_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL67_LEVEL13_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL67_LEVEL13_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL67_LEVEL15_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL67_LEVEL15_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL67_LEVEL16_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL67_LEVEL16_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL67_LEVEL17_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL67_LEVEL20_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL67_LEVEL20_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL67_LEVEL20_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL67_LEVEL20_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL67_LEVEL24_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL67_LEVEL24_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL67_LEVEL24_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL67_LEVEL24_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL67 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL68_LEVEL2_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL68_LEVEL2_LEVEL70
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL70 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL68_LEVEL3_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL68_LEVEL3_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL68_LEVEL4_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL68_LEVEL4_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL68_LEVEL8_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL68_LEVEL8_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL68_LEVEL9_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL68_LEVEL9_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL68_LEVEL10_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL68_LEVEL10_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL68_LEVEL13_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL68_LEVEL13_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL68_LEVEL15_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL68_LEVEL15_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL68_LEVEL16_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL68_LEVEL16_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL68_LEVEL17_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL68_LEVEL19_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL68_LEVEL19_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL68 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL69_LEVEL2_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL69_LEVEL2_LEVEL71
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL71 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL69_LEVEL3_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL69_LEVEL3_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL69_LEVEL4_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL69_LEVEL4_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL69_LEVEL8_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL69_LEVEL8_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL69_LEVEL9_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL69_LEVEL9_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL69_LEVEL10_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL69_LEVEL10_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL69_LEVEL13_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL69_LEVEL13_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL69_LEVEL15_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL69_LEVEL15_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL69_LEVEL16_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL69_LEVEL16_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL69 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL70_LEVEL2_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL70_LEVEL2_LEVEL72
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL72 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL70_LEVEL3_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL70_LEVEL3_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL70_LEVEL4_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL70_LEVEL4_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL70_LEVEL8_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL70_LEVEL8_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL70_LEVEL9_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL70_LEVEL9_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL70_LEVEL10_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL70_LEVEL10_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL70_LEVEL13_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL70_LEVEL13_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL70_LEVEL15_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL70_LEVEL15_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL70_LEVEL17_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL70 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL71_LEVEL2_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL71_LEVEL2_LEVEL73
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL73 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL71_LEVEL3_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL71_LEVEL3_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL71_LEVEL4_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL71_LEVEL4_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL71_LEVEL8_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL71_LEVEL8_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL71_LEVEL9_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL71_LEVEL9_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL71_LEVEL10_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL71_LEVEL10_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL71_LEVEL13_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL71_LEVEL13_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL71_LEVEL16_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL71_LEVEL16_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL71_LEVEL20_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL71_LEVEL20_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL71_LEVEL20_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL71_LEVEL20_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL71 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL72_LEVEL2_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL72_LEVEL2_LEVEL74
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL74 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL72_LEVEL3_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL72_LEVEL3_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL72_LEVEL4_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL72_LEVEL4_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL72_LEVEL8_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL72_LEVEL8_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL72_LEVEL9_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL72_LEVEL9_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL72_LEVEL10_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL72_LEVEL10_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL72_LEVEL13_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL72_LEVEL13_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL72_LEVEL15_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL72_LEVEL15_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL72_LEVEL19_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL72_LEVEL19_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL72 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL73_LEVEL2_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL73_LEVEL2_LEVEL75
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL75 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL73_LEVEL3_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL73_LEVEL3_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL73_LEVEL4_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL73_LEVEL4_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL73_LEVEL8_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL73_LEVEL8_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL73_LEVEL9_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL73_LEVEL9_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL73_LEVEL10_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL73_LEVEL10_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL73 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL74_LEVEL2_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL74_LEVEL2_LEVEL76
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL76 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL74_LEVEL3_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL74_LEVEL3_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL74_LEVEL4_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL74_LEVEL4_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL74_LEVEL8_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL74_LEVEL8_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL74_LEVEL9_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL74_LEVEL9_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL74_LEVEL10_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL74_LEVEL10_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL74_LEVEL13_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL74_LEVEL13_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL74_LEVEL17_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL74 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL75_LEVEL2_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL75_LEVEL2_LEVEL77
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL77 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL75_LEVEL3_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL75_LEVEL3_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL75_LEVEL4_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL75_LEVEL4_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL75_LEVEL8_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL75_LEVEL8_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL75_LEVEL9_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL75_LEVEL9_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL75_LEVEL10_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL75_LEVEL10_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL75_LEVEL16_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L11_L3_LEVEL75_LEVEL16_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L2_L8_LEVEL75_LEVEL25_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L8_L2_LEVEL75_LEVEL25_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL75 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL76_LEVEL2_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL76_LEVEL2_LEVEL78
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL78 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL76_LEVEL3_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL76_LEVEL3_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL76_LEVEL4_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL76_LEVEL4_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL76_LEVEL8_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL76_LEVEL8_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL76_LEVEL9_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL76_LEVEL9_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL76_LEVEL15_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL76_LEVEL15_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L0_L9_LEVEL76_LEVEL24_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L6_L10_LEVEL76_LEVEL24_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L0_LEVEL76_LEVEL24_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L10_L6_LEVEL76_LEVEL24_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL76 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL77_LEVEL2_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL77_LEVEL2_LEVEL79
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL79 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL77_LEVEL3_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL77_LEVEL3_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL77_LEVEL4_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL77_LEVEL4_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL77_LEVEL8_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL77_LEVEL8_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL77_LEVEL10_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL77_LEVEL10_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL77 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL78_LEVEL2_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL78_LEVEL2_LEVEL80
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL80 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL78_LEVEL3_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL78_LEVEL3_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL78_LEVEL4_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL78_LEVEL4_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL78_LEVEL9_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL78_LEVEL9_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL78_LEVEL13_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L10_L7_LEVEL78_LEVEL13_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL78 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL79_LEVEL2_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL79_LEVEL2_LEVEL81
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL81 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL79_LEVEL3_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL79_LEVEL3_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL79_LEVEL4_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL79_LEVEL4_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL79_LEVEL8_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL79_LEVEL8_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL79 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL80_LEVEL2_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL80_LEVEL2_LEVEL82
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL82 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL80_LEVEL3_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL80_LEVEL3_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL80_LEVEL4_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL80_LEVEL4_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L0_L10_LEVEL80_LEVEL20_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L0 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L1_L9_LEVEL80_LEVEL20_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L9_L1_LEVEL80_LEVEL20_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L10_L0_LEVEL80_LEVEL20_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L0 )
			( FUEL_T0_LEVEL80 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL81_LEVEL2_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL81_LEVEL2_LEVEL83
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL83 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL81_LEVEL3_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL81_LEVEL3_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL81_LEVEL4_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL81_LEVEL4_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L5_L9_LEVEL81_LEVEL10_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L9_L5_LEVEL81_LEVEL10_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L7_L11_LEVEL81_LEVEL19_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L11_L7_LEVEL81_LEVEL19_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL81 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL82_LEVEL2_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL82_LEVEL2_LEVEL84
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL84 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL84 ))
		)
	)
	(:action DRIVE_T0_L1_L2_LEVEL82_LEVEL3_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L1 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L2_L1_LEVEL82_LEVEL3_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L1 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL82_LEVEL9_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL82_LEVEL9_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL82 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL83_LEVEL2_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L9_L3_LEVEL83_LEVEL2_LEVEL85
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL85 ))
		)
	)
	(:action DRIVE_T0_L2_L6_LEVEL83_LEVEL4_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L2 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L6_L2_LEVEL83_LEVEL4_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L2 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L6_L9_LEVEL83_LEVEL8_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L6 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L9_L6_LEVEL83_LEVEL8_LEVEL91
		:parameters ()
		:precondition
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL91 )
		)
		:effect
		(and
			( AT_T0_L6 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L9 ))
			(not ( FUEL_T0_LEVEL91 ))
		)
	)
	(:action DRIVE_T0_L4_L8_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L4 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L5_L8_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L5 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L8_L4_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L4 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L8_L5_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L5 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L10_L11_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L11_L10_LEVEL83_LEVEL17_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL83 )
			(not ( AT_T0_L11 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L11_LEVEL84_LEVEL16_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L11 )
			( FUEL_T0_LEVEL84 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L3_L9_LEVEL85_LEVEL2_LEVEL87
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL87 )
		)
		:effect
		(and
			( AT_T0_L9 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL87 ))
		)
	)
	(:action DRIVE_T0_L3_L10_LEVEL85_LEVEL15_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L3 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L10_L3_LEVEL85_LEVEL15_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L3 )
			( FUEL_T0_LEVEL85 )
			(not ( AT_T0_L10 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L7_L10_LEVEL87_LEVEL13_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L10 )
			( FUEL_T0_LEVEL87 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L7_L8_LEVEL91_LEVEL9_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L7 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action DRIVE_T0_L8_L7_LEVEL91_LEVEL9_LEVEL100
		:parameters ()
		:precondition
		(and
			( AT_T0_L8 )
			( FUEL_T0_LEVEL100 )
		)
		:effect
		(and
			( AT_T0_L7 )
			( FUEL_T0_LEVEL91 )
			(not ( AT_T0_L8 ))
			(not ( FUEL_T0_LEVEL100 ))
		)
	)
	(:action UNLOAD_P8_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P8_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P8_L8 )
			(not ( IN_P8_T0 ))
		)
	)
	(:action UNLOAD_P0_T0_L8
		:parameters ()
		:precondition
		(and
			( IN_P0_T0 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( AT_P0_L8 )
			(not ( IN_P0_T0 ))
		)
	)
	(:action LOAD_P8_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P8_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P8_T0 )
			(not ( AT_P8_L8 ))
		)
	)
	(:action LOAD_P0_T0_L8
		:parameters ()
		:precondition
		(and
			( AT_P0_L8 )
			( AT_T0_L8 )
		)
		:effect
		(and
			( IN_P0_T0 )
			(not ( AT_P0_L8 ))
		)
	)

)
