(define
	(problem grounded-TRANSPORT-TWO-CITIES-SEQUENTIAL-6NODES-1000SIZE-3DEGREE-100MINDISTANCE-2TRUCKS-4PACKAGES-2008SEED)
	(:domain grounded-TRANSPORT)
	(:init
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-1-LOC-1_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-1-LOC-6_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-4_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-1-LOC-2_PNUM0 )
		( AT-VEHICLE_TRUCK-1_CITY-2-LOC-5 )
		( AT-VEHICLE_TRUCK-2_CITY-2-LOC-2 )
		( CAPACITY_TRUCK-2_CAPACITY-3 )
		( CAPACITY_TRUCK-1_CAPACITY-4 )
	)
	(:goal
		(and 
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-2-LOC-3_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-2-LOC-1_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-2-LOC-5_PNUM2 )
		)
	)

)
