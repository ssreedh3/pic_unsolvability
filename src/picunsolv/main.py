import sys
from picunsolv.driver import Driver
from picunsolv.model_parser import Parser

# Assuming fully grounded model
domain_file = sys.argv[1]
inst_file = sys.argv[2]
# TODO: Add support for prop_list
#prop_list = sys.argv[3]

parser = Parser()
model = parser.parse_model(domain_file, inst_file)
prop_set = parser.get_prop_set()
driver_obj = Driver(model, prop_set)
import time
start = time.time()
min_set = driver_obj.find_minimal_set()
print ("Completed Search >>>>>")
print ("Total time:", time.time() - start)
print (min_set)





