from picunsolv.constants import *
from picunsolv.utils import powerset_to_limit

import copy

class PimGenerator:
    def __init__(self):
        pass

    def get_conj_fluent(self, conjunct):
        if len(conjunct) > 1:
            return CONJ_PREFIX + '&'.join(conjunct)
        elif len(conjunct) == 1:
            return list(conjunct)[0]
        else:
            return ''

    def get_semi_relaxed_model(self, model, conjuncts, m=2, debug=False):
        # Perform a test to make sure conjunct set meets the requirements
        conjuncts_set = [set(a) for a in list(powerset_to_limit(conjuncts, m))]
        #print (conjuncts_set)
        compiled_model = {} #copy.deepcopy(model)
        compiled_model[INSTANCE] = copy.deepcopy(model[INSTANCE])
        compiled_model[DOMAIN] = {}

        # Add the conjuncts into initial state
        for conj in conjuncts_set:
            if conj <= compiled_model[INSTANCE][INIT]:
                conj_var_name = self.get_conj_fluent(conj)
                if conj_var_name != '':
                    compiled_model[INSTANCE][INIT].add(conj_var_name)

            if conj <= compiled_model[INSTANCE][GOAL]:
                conj_var_name = self.get_conj_fluent(conj)
                if conj_var_name != '':
                    compiled_model[INSTANCE][GOAL].add(conj_var_name)

        for act in model[DOMAIN]:
            act_id = 0
            for conj in conjuncts_set:
                if len(conj) == m or len(conj & model[DOMAIN][act][DELS]) > 0 or len(conj & model[DOMAIN][act][ADDS]) > 0:
                    continue
                new_act = {}
                new_act[POS_PREC] = set()
                new_act[ADDS] = set()
                new_act[DELS] = set()
                for pic_conj in [set(a) for a in list(powerset_to_limit(model[DOMAIN][act][POS_PREC]|conj, m))]:
                    pic_var = self.get_conj_fluent(pic_conj)
                    if pic_var != '':
                        new_act[POS_PREC].add(pic_var)

                for pic_conj in [set(a) for a in list(powerset_to_limit(model[DOMAIN][act][ADDS]|conj, m))]:
                    if len(pic_conj & model[DOMAIN][act][ADDS]) > 0:
                        pic_var = self.get_conj_fluent(pic_conj)
                        if pic_var != '':
                            new_act[ADDS].add(pic_var)
                compiled_model[DOMAIN][act+str(act_id)] = new_act
                act_id += 1
        return compiled_model
