
# barman-unsolvable
#mkdir -p ../domains/ipc_comp_grounded/barman-unsolvable/
#id=0
#for prob in `ls ../domains/ipc_comp_ungrounded/barman-unsolvable/|grep  "b.pddl"`
#do
#    dom=`echo $prob|sed 's/reformulated-BARMAN/reformulated-domain-BARMAN/g'|sed 's/b.pddl/.pddl/'`
#    mkdir -p ../domains/ipc_comp_grounded/barman-unsolvable/${id}/
#    ./run_the_grounder.sh ../domains/ipc_comp_ungrounded/barman-unsolvable/${dom} ../domains/ipc_comp_ungrounded/barman-unsolvable/${prob} ../domains/ipc_comp_grounded/barman-unsolvable/${id}/
#    id=`expr ${id} + 1`
#done

#mkdir -p ../domains/ipc_comp_grounded/gripper-unsolvable/
#id=0

#for prob in `ls ../domains/ipc_comp_ungrounded/gripper-unsolvable/|grep "unsolvable.pddl"`
#do
#    dom="reformulated-domain.pddl"
#    mkdir -p ../domains/ipc_comp_grounded/gripper-unsolvable/${id}/
#    ./run_the_grounder.sh ../domains/ipc_comp_ungrounded/gripper-unsolvable/${dom} ../domains/ipc_comp_ungrounded/gripper-unsolvable/${prob} ../domains/ipc_comp_grounded/gripper-unsolvable/${id}/
#    id=`expr ${id} + 1`
#done


mkdir -p ../domains/ipc_comp_grounded/transport/
id=0
for prob in `ls ../domains/ipc_comp_ungrounded/transport/transport-final-unsolvable/|grep "unsolvable.pddl"`
do
    dom=`echo $prob|sed 's/reformulated-TRANSPORT/reformulated-domain-TRANSPORT/g'|sed 's/-unsolvable.pddl/.pddl/'`
    mkdir -p ../domains/ipc_comp_grounded/transport/${id}/
    ./run_the_grounder.sh ../domains/ipc_comp_ungrounded/transport/transport-final-unsolvable/${dom} ../domains/ipc_comp_ungrounded/transport/transport-final-unsolvable/${prob} ../domains/ipc_comp_grounded/transport/${id}/
    id=`expr ${id} + 1`
done
