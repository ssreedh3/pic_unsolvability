#/usr/bin/env bash

# path to fast downward #
FD_PATH=$(locate fast-downward.py | head -n 1)

# find optimal plan using fd on input domain and problem #
#TODO: Change to a non-optimal planner
rm -f output output.sas sas_plan
${FD_PATH} --alias seq-sat-lama-2011 $1 $2 | grep -e \([0-9]\) | awk '{$NF=""; print $0}'
