mkdir -p ../domains/grounded

# Bottleneck
mkdir -p ../domains/grounded/bottleneck/
id=0
for prob in `ls ../domains/unsat-benchmarks/bottleneck/|grep -v domain.pddl`
do
    mkdir -p ../domains/grounded/bottleneck/${id}/
    ./run_the_grounder.sh ../domains/unsat-benchmarks/bottleneck/domain.pddl ../domains/unsat-benchmarks/bottleneck/${prob} ../domains/grounded/bottleneck/${id}/
    id=`expr ${id} + 1`
done


# unsat-nomystery
mkdir -p ../domains/grounded/unsat-nomystery/
id=0
for prob in `ls ../domains/unsat-benchmarks/unsat-nomystery/|grep -v domain.pddl`
do  
        mkdir -p ../domains/grounded/unsat-nomystery/${id}/
        ./run_the_grounder.sh ../domains/unsat-benchmarks/unsat-nomystery/domain.pddl ../domains/unsat-benchmarks/unsat-nomystery/${prob} ../domains/grounded/unsat-nomystery/${id}/
        id=`expr ${id} + 1`
done 

# mystery
mkdir -p ../domains/grounded/mystery/
id=0
for prob in `ls ../domains/unsat-benchmarks/mystery/|grep -v domain.pddl`
do  
    mkdir -p ../domains/grounded/mystery/${id}/
    ./run_the_grounder.sh ../domains/unsat-benchmarks/mystery/domain.pddl ../domains/unsat-benchmarks/mystery/${prob} ../domains/grounded/mystery/${id}/
    id=`expr ${id} + 1`
done

# rovers
mkdir -p ../domains/grounded/unsat-rovers/
id=0                                       
for prob in `ls ../domains/unsat-benchmarks/unsat-rovers/|grep -v domain.pddl`
do                                           
    mkdir -p ../domains/grounded/unsat-rovers/${id}/
    ./run_the_grounder.sh ../domains/unsat-benchmarks/unsat-rovers/domain.pddl ../domains/unsat-benchmarks/unsat-rovers/${prob} ../domains/grounded/unsat-rovers/${id}/
    id=`expr ${id} + 1`         
done

# tiles

mkdir -p ../domains/grounded/unsat-tiles
id=0                                       
for prob in `ls ../domains/unsat-benchmarks/unsat-tiles/|grep -v domain.pddl`
do                                           
    mkdir -p ../domains/grounded/unsat-tiles/${id}/
    ./run_the_grounder.sh ../domains/unsat-benchmarks/unsat-tiles/domain.pddl ../domains/unsat-benchmarks/unsat-tiles/${prob} ../domains/grounded/unsat-tiles/${id}/
    id=`expr ${id} + 1`         
done


# tpp

mkdir -p ../domains/grounded/unsat-tpp/
id=0                                       
for prob in `ls ../domains/unsat-benchmarks/unsat-tpp/|grep -v domain.pddl`
do                                           
    mkdir -p ../domains/grounded/unsat-tpp/${id}/
    ./run_the_grounder.sh ../domains/unsat-benchmarks/unsat-tpp/domain.pddl ../domains/unsat-benchmarks/unsat-tpp/${prob} ../domains/grounded/unsat-tpp/${id}/
    id=`expr ${id} + 1`         
done

