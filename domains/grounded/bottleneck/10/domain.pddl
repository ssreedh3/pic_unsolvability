(define
	(domain grounded-RESOURCES)
	(:requirements :strips :action-costs)
	(:predicates
		( PERSON-AT_P5_LOC-1-5 )
		( PERSON-AT_P4_LOC-1-4 )
		( PERSON-AT_P3_LOC-1-3 )
		( PERSON-AT_P2_LOC-1-2 )
		( PERSON-AT_P1_LOC-1-1 )
		( PERSON-AT_P0_LOC-1-0 )
		( PERSON-AT_P5_LOC-1-4 )
		( PERSON-AT_P5_LOC-1-3 )
		( PERSON-AT_P4_LOC-1-3 )
		( PERSON-AT_P5_LOC-1-2 )
		( PERSON-AT_P4_LOC-1-2 )
		( PERSON-AT_P3_LOC-1-2 )
		( PERSON-AT_P5_LOC-1-1 )
		( PERSON-AT_P4_LOC-1-1 )
		( PERSON-AT_P3_LOC-1-1 )
		( PERSON-AT_P2_LOC-1-1 )
		( PERSON-AT_P5_LOC-1-0 )
		( PERSON-AT_P4_LOC-1-0 )
		( PERSON-AT_P3_LOC-1-0 )
		( PERSON-AT_P2_LOC-1-0 )
		( PERSON-AT_P1_LOC-1-0 )
		( PERSON-AT_P4_LOC-1-5 )
		( PERSON-AT_P3_LOC-1-4 )
		( PERSON-AT_P2_LOC-1-3 )
		( PERSON-AT_P1_LOC-1-2 )
		( PERSON-AT_P0_LOC-1-1 )
		( PERSON-AT_P5_LOC-2-5 )
		( PERSON-AT_P4_LOC-2-5 )
		( PERSON-AT_P5_LOC-2-4 )
		( PERSON-AT_P4_LOC-2-4 )
		( PERSON-AT_P3_LOC-2-4 )
		( PERSON-AT_P5_LOC-2-3 )
		( PERSON-AT_P4_LOC-2-3 )
		( PERSON-AT_P3_LOC-2-3 )
		( PERSON-AT_P2_LOC-2-3 )
		( PERSON-AT_P5_LOC-2-2 )
		( PERSON-AT_P4_LOC-2-2 )
		( PERSON-AT_P3_LOC-2-2 )
		( PERSON-AT_P2_LOC-2-2 )
		( PERSON-AT_P1_LOC-2-2 )
		( PERSON-AT_P5_LOC-2-1 )
		( PERSON-AT_P4_LOC-2-1 )
		( PERSON-AT_P3_LOC-2-1 )
		( PERSON-AT_P2_LOC-2-1 )
		( PERSON-AT_P1_LOC-2-1 )
		( PERSON-AT_P0_LOC-2-1 )
		( PERSON-AT_P5_LOC-2-0 )
		( PERSON-AT_P4_LOC-2-0 )
		( PERSON-AT_P3_LOC-2-0 )
		( PERSON-AT_P2_LOC-2-0 )
		( PERSON-AT_P1_LOC-2-0 )
		( PERSON-AT_P0_LOC-2-0 )
		( PERSON-AT_P3_LOC-2-5 )
		( PERSON-AT_P2_LOC-2-4 )
		( PERSON-AT_P1_LOC-2-3 )
		( PERSON-AT_P0_LOC-2-2 )
		( PERSON-AT_P3_LOC-1-5 )
		( PERSON-AT_P2_LOC-1-4 )
		( PERSON-AT_P1_LOC-1-3 )
		( PERSON-AT_P0_LOC-1-2 )
		( PERSON-AT_P5_LOC-3-3 )
		( PERSON-AT_P4_LOC-3-3 )
		( PERSON-AT_P3_LOC-3-3 )
		( PERSON-AT_P2_LOC-3-3 )
		( PERSON-AT_P1_LOC-3-3 )
		( PERSON-AT_P5_LOC-3-2 )
		( PERSON-AT_P4_LOC-3-2 )
		( PERSON-AT_P3_LOC-3-2 )
		( PERSON-AT_P2_LOC-3-2 )
		( PERSON-AT_P1_LOC-3-2 )
		( PERSON-AT_P0_LOC-3-2 )
		( PERSON-AT_P5_LOC-3-1 )
		( PERSON-AT_P4_LOC-3-1 )
		( PERSON-AT_P3_LOC-3-1 )
		( PERSON-AT_P2_LOC-3-1 )
		( PERSON-AT_P1_LOC-3-1 )
		( PERSON-AT_P0_LOC-3-1 )
		( PERSON-AT_P5_LOC-3-0 )
		( PERSON-AT_P4_LOC-3-0 )
		( PERSON-AT_P3_LOC-3-0 )
		( PERSON-AT_P2_LOC-3-0 )
		( PERSON-AT_P1_LOC-3-0 )
		( PERSON-AT_P0_LOC-3-0 )
		( PERSON-AT_P0_LOC-3-3 )
		( PERSON-AT_P2_LOC-2-5 )
		( PERSON-AT_P1_LOC-2-4 )
		( PERSON-AT_P0_LOC-2-3 )
		( PERSON-AT_P2_LOC-1-5 )
		( PERSON-AT_P1_LOC-1-4 )
		( PERSON-AT_P0_LOC-1-3 )
		( PERSON-AT_P5_LOC-4-3 )
		( PERSON-AT_P4_LOC-4-3 )
		( PERSON-AT_P3_LOC-4-3 )
		( PERSON-AT_P2_LOC-4-3 )
		( PERSON-AT_P1_LOC-4-3 )
		( PERSON-AT_P0_LOC-4-3 )
		( PERSON-AT_P5_LOC-4-2 )
		( PERSON-AT_P4_LOC-4-2 )
		( PERSON-AT_P3_LOC-4-2 )
		( PERSON-AT_P2_LOC-4-2 )
		( PERSON-AT_P1_LOC-4-2 )
		( PERSON-AT_P0_LOC-4-2 )
		( PERSON-AT_P5_LOC-4-1 )
		( PERSON-AT_P4_LOC-4-1 )
		( PERSON-AT_P3_LOC-4-1 )
		( PERSON-AT_P2_LOC-4-1 )
		( PERSON-AT_P1_LOC-4-1 )
		( PERSON-AT_P0_LOC-4-1 )
		( PERSON-AT_P5_LOC-4-0 )
		( PERSON-AT_P4_LOC-4-0 )
		( PERSON-AT_P3_LOC-4-0 )
		( PERSON-AT_P2_LOC-4-0 )
		( PERSON-AT_P1_LOC-4-0 )
		( PERSON-AT_P0_LOC-4-0 )
		( PERSON-AT_P5_LOC-4-4 )
		( PERSON-AT_P4_LOC-4-4 )
		( PERSON-AT_P3_LOC-4-4 )
		( PERSON-AT_P2_LOC-4-4 )
		( PERSON-AT_P1_LOC-4-4 )
		( PERSON-AT_P0_LOC-4-4 )
		( PERSON-AT_P1_LOC-2-5 )
		( PERSON-AT_P0_LOC-2-4 )
		( PERSON-AT_P1_LOC-1-5 )
		( PERSON-AT_P0_LOC-1-4 )
		( PERSON-AT_P5_LOC-5-4 )
		( PERSON-AT_P4_LOC-5-4 )
		( PERSON-AT_P3_LOC-5-4 )
		( PERSON-AT_P2_LOC-5-4 )
		( PERSON-AT_P1_LOC-5-4 )
		( PERSON-AT_P0_LOC-5-4 )
		( PERSON-AT_P5_LOC-5-3 )
		( PERSON-AT_P4_LOC-5-3 )
		( PERSON-AT_P3_LOC-5-3 )
		( PERSON-AT_P2_LOC-5-3 )
		( PERSON-AT_P1_LOC-5-3 )
		( PERSON-AT_P0_LOC-5-3 )
		( PERSON-AT_P5_LOC-5-2 )
		( PERSON-AT_P4_LOC-5-2 )
		( PERSON-AT_P3_LOC-5-2 )
		( PERSON-AT_P2_LOC-5-2 )
		( PERSON-AT_P1_LOC-5-2 )
		( PERSON-AT_P0_LOC-5-2 )
		( PERSON-AT_P5_LOC-5-1 )
		( PERSON-AT_P4_LOC-5-1 )
		( PERSON-AT_P3_LOC-5-1 )
		( PERSON-AT_P2_LOC-5-1 )
		( PERSON-AT_P1_LOC-5-1 )
		( PERSON-AT_P0_LOC-5-1 )
		( PERSON-AT_P5_LOC-5-0 )
		( PERSON-AT_P4_LOC-5-0 )
		( PERSON-AT_P3_LOC-5-0 )
		( PERSON-AT_P2_LOC-5-0 )
		( PERSON-AT_P1_LOC-5-0 )
		( PERSON-AT_P0_LOC-5-0 )
		( PERSON-AT_P5_LOC-5-5 )
		( PERSON-AT_P4_LOC-5-5 )
		( PERSON-AT_P3_LOC-5-5 )
		( PERSON-AT_P2_LOC-5-5 )
		( PERSON-AT_P1_LOC-5-5 )
		( PERSON-AT_P0_LOC-5-5 )
		( PERSON-AT_P5_LOC-4-5 )
		( PERSON-AT_P4_LOC-4-5 )
		( PERSON-AT_P3_LOC-4-5 )
		( PERSON-AT_P2_LOC-4-5 )
		( PERSON-AT_P1_LOC-4-5 )
		( PERSON-AT_P0_LOC-4-5 )
		( PERSON-AT_P0_LOC-2-5 )
		( PERSON-AT_P0_LOC-1-5 )
		( ACTIVE_LOC-1-4 )
		( ACTIVE_LOC-2-4 )
		( ACTIVE_LOC-4-4 )
		( ACTIVE_LOC-5-4 )
		( ACTIVE_LOC-2-5 )
		( ACTIVE_LOC-5-5 )
		( ACTIVE_LOC-1-5 )
		( ACTIVE_LOC-4-0 )
		( ACTIVE_LOC-4-1 )
		( ACTIVE_LOC-4-2 )
		( ACTIVE_LOC-4-3 )
		( ACTIVE_LOC-4-5 )
		( ACTIVE_LOC-5-1 )
		( ACTIVE_LOC-5-2 )
		( ACTIVE_LOC-5-3 )
		( ACTIVE_LOC-1-3 )
		( ACTIVE_LOC-2-3 )
		( ACTIVE_LOC-5-0 )
		( ACTIVE_LOC-3-0 )
		( ACTIVE_LOC-3-1 )
		( ACTIVE_LOC-3-2 )
		( ACTIVE_LOC-3-3 )
		( ACTIVE_LOC-1-2 )
		( ACTIVE_LOC-2-2 )
		( ACTIVE_LOC-2-0 )
		( ACTIVE_LOC-2-1 )
		( ACTIVE_LOC-1-1 )
		( ACTIVE_LOC-1-0 )
		( PERSON-AT_P0_LOC-0-0 )
		( PERSON-AT_P1_LOC-0-1 )
		( PERSON-AT_P2_LOC-0-2 )
		( PERSON-AT_P3_LOC-0-3 )
		( PERSON-AT_P4_LOC-0-4 )
		( PERSON-AT_P5_LOC-0-5 )
	) 
	(:action MOVE_P0_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P0_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-4 )
			(not ( PERSON-AT_P0_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P0_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-4 )
			(not ( PERSON-AT_P0_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P0_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P0_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-4 )
			(not ( PERSON-AT_P0_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P1_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P1_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-4 )
			(not ( PERSON-AT_P1_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P2_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P2_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-4 )
			(not ( PERSON-AT_P2_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P3_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P3_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-4 )
			(not ( PERSON-AT_P3_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P4_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P4_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-4 )
			(not ( PERSON-AT_P4_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P5_LOC-4-5_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P5_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-4 )
			(not ( PERSON-AT_P5_LOC-4-5 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P0_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-4 )
			(not ( PERSON-AT_P0_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P1_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P1_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-4 )
			(not ( PERSON-AT_P1_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P2_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P2_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-4 )
			(not ( PERSON-AT_P2_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P3_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P3_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-4 )
			(not ( PERSON-AT_P3_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P4_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P4_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-4 )
			(not ( PERSON-AT_P4_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P5_LOC-5-5_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P5_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-4 )
			(not ( PERSON-AT_P5_LOC-5-5 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P0_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-5 )
			(not ( PERSON-AT_P0_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P0_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-5 )
			(not ( PERSON-AT_P0_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P1_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P1_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-5 )
			(not ( PERSON-AT_P1_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P2_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P2_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-5 )
			(not ( PERSON-AT_P2_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P3_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P3_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-5 )
			(not ( PERSON-AT_P3_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P4_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P4_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-5 )
			(not ( PERSON-AT_P4_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P5_LOC-4-5_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P5_LOC-4-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-5 )
			(not ( PERSON-AT_P5_LOC-4-5 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P0_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-5 )
			(not ( PERSON-AT_P0_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P0_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-0 )
			(not ( PERSON-AT_P0_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P1_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P1_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-0 )
			(not ( PERSON-AT_P1_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P2_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P2_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-0 )
			(not ( PERSON-AT_P2_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P3_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P3_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-0 )
			(not ( PERSON-AT_P3_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P4_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P4_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-0 )
			(not ( PERSON-AT_P4_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P5_LOC-5-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P5_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-0 )
			(not ( PERSON-AT_P5_LOC-5-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P0_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P5_LOC-5-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P5_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-1 )
			(not ( PERSON-AT_P5_LOC-5-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P5_LOC-5-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P5_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-2 )
			(not ( PERSON-AT_P5_LOC-5-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P5_LOC-5-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P5_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-3 )
			(not ( PERSON-AT_P5_LOC-5-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P0_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-4 )
			(not ( PERSON-AT_P0_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P1_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P1_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-4 )
			(not ( PERSON-AT_P1_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P2_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P2_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-4 )
			(not ( PERSON-AT_P2_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P3_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P3_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-4 )
			(not ( PERSON-AT_P3_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P4_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P4_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-4 )
			(not ( PERSON-AT_P4_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P5_LOC-5-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P5_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-4 )
			(not ( PERSON-AT_P5_LOC-5-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P0_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-5 )
			(not ( PERSON-AT_P0_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P1_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P1_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-5 )
			(not ( PERSON-AT_P1_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P2_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P2_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-5 )
			(not ( PERSON-AT_P2_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P3_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P3_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-5 )
			(not ( PERSON-AT_P3_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P4_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P4_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-5 )
			(not ( PERSON-AT_P4_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P5_LOC-5-5_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P5_LOC-5-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-5 )
			(not ( PERSON-AT_P5_LOC-5-5 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P0_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P0_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-5 )
			(not ( PERSON-AT_P0_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P0_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-5 )
			(not ( PERSON-AT_P0_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P0_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-5 )
			(not ( PERSON-AT_P0_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P1_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P1_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-5 )
			(not ( PERSON-AT_P1_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P2_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P2_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-5 )
			(not ( PERSON-AT_P2_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P3_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P3_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-5 )
			(not ( PERSON-AT_P3_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P4_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P4_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-5 )
			(not ( PERSON-AT_P4_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P5_LOC-4-4_LOC-4-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-5 )
			( PERSON-AT_P5_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-5 )
			(not ( PERSON-AT_P5_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-5 ))
		)
	)
	(:action MOVE_P0_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P0_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-1 )
			(not ( PERSON-AT_P0_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P1_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P1_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-1 )
			(not ( PERSON-AT_P1_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P2_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P2_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-1 )
			(not ( PERSON-AT_P2_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P3_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P3_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-1 )
			(not ( PERSON-AT_P3_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P4_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P4_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-1 )
			(not ( PERSON-AT_P4_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P5_LOC-5-0_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P5_LOC-5-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-1 )
			(not ( PERSON-AT_P5_LOC-5-0 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P0_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P0_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-2 )
			(not ( PERSON-AT_P0_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P1_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P1_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-2 )
			(not ( PERSON-AT_P1_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P2_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P2_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-2 )
			(not ( PERSON-AT_P2_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P3_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P3_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-2 )
			(not ( PERSON-AT_P3_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P4_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P4_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-2 )
			(not ( PERSON-AT_P4_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P5_LOC-5-1_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P5_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-2 )
			(not ( PERSON-AT_P5_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P0_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P0_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-3 )
			(not ( PERSON-AT_P0_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P1_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P1_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-3 )
			(not ( PERSON-AT_P1_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P2_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P2_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-3 )
			(not ( PERSON-AT_P2_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P3_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P3_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-3 )
			(not ( PERSON-AT_P3_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P4_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P4_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-3 )
			(not ( PERSON-AT_P4_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P5_LOC-5-2_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P5_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-3 )
			(not ( PERSON-AT_P5_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P0_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P0_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-4 )
			(not ( PERSON-AT_P0_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P1_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P1_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-4 )
			(not ( PERSON-AT_P1_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P2_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P2_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-4 )
			(not ( PERSON-AT_P2_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P3_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P3_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-4 )
			(not ( PERSON-AT_P3_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P4_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P4_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-4 )
			(not ( PERSON-AT_P4_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P5_LOC-5-3_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P5_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-4 )
			(not ( PERSON-AT_P5_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P0_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P0_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-5 )
			(not ( PERSON-AT_P0_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P1_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P1_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-5 )
			(not ( PERSON-AT_P1_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P2_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P2_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-5 )
			(not ( PERSON-AT_P2_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P3_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P3_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-5 )
			(not ( PERSON-AT_P3_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P4_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P4_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-5 )
			(not ( PERSON-AT_P4_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P5_LOC-5-4_LOC-5-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-5 )
			( PERSON-AT_P5_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-5 )
			(not ( PERSON-AT_P5_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-5 ))
		)
	)
	(:action MOVE_P0_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P1_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P1_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-4 )
			(not ( PERSON-AT_P1_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P1_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P1_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-4 )
			(not ( PERSON-AT_P1_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P0_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P5_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P5_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-3 )
			(not ( PERSON-AT_P5_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P0_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-0 )
			(not ( PERSON-AT_P0_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P1_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P1_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-0 )
			(not ( PERSON-AT_P1_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P2_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P2_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-0 )
			(not ( PERSON-AT_P2_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P3_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P3_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-0 )
			(not ( PERSON-AT_P3_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P4_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P4_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-0 )
			(not ( PERSON-AT_P4_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P5_LOC-5-1_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P5_LOC-5-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-0 )
			(not ( PERSON-AT_P5_LOC-5-1 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P0_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P0_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-1 )
			(not ( PERSON-AT_P0_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P1_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P1_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-1 )
			(not ( PERSON-AT_P1_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P2_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P2_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-1 )
			(not ( PERSON-AT_P2_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P3_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P3_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-1 )
			(not ( PERSON-AT_P3_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P4_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P4_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-1 )
			(not ( PERSON-AT_P4_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P5_LOC-5-2_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P5_LOC-5-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-1 )
			(not ( PERSON-AT_P5_LOC-5-2 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P0_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P0_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-2 )
			(not ( PERSON-AT_P0_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P1_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P1_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-2 )
			(not ( PERSON-AT_P1_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P2_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P2_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-2 )
			(not ( PERSON-AT_P2_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P3_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P3_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-2 )
			(not ( PERSON-AT_P3_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P4_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P4_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-2 )
			(not ( PERSON-AT_P4_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P5_LOC-5-3_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P5_LOC-5-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-2 )
			(not ( PERSON-AT_P5_LOC-5-3 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P0_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P0_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-3 )
			(not ( PERSON-AT_P0_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P1_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P1_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-3 )
			(not ( PERSON-AT_P1_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P2_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P2_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-3 )
			(not ( PERSON-AT_P2_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P3_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P3_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-3 )
			(not ( PERSON-AT_P3_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P4_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P4_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-3 )
			(not ( PERSON-AT_P4_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P5_LOC-5-4_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P5_LOC-5-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-3 )
			(not ( PERSON-AT_P5_LOC-5-4 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P0_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-4 )
			(not ( PERSON-AT_P0_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P1_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P1_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-5 )
			(not ( PERSON-AT_P1_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P0_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-0 )
			(not ( PERSON-AT_P0_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P1_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P1_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-0 )
			(not ( PERSON-AT_P1_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P2_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P2_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-0 )
			(not ( PERSON-AT_P2_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P3_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P3_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-0 )
			(not ( PERSON-AT_P3_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P4_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P4_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-0 )
			(not ( PERSON-AT_P4_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P5_LOC-4-0_LOC-5-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-0 )
			( PERSON-AT_P5_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-0 )
			(not ( PERSON-AT_P5_LOC-4-0 ))
			(not ( ACTIVE_LOC-5-0 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-1 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-1 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-1 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-1 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-1 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P5_LOC-4-1_LOC-5-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-1 )
			( PERSON-AT_P5_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-1 )
			(not ( PERSON-AT_P5_LOC-4-1 ))
			(not ( ACTIVE_LOC-5-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-2 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-2 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-2 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-2 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-2 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P5_LOC-4-2_LOC-5-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-2 )
			( PERSON-AT_P5_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-2 )
			(not ( PERSON-AT_P5_LOC-4-2 ))
			(not ( ACTIVE_LOC-5-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-3 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-3 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-3 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-3 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-3 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P5_LOC-4-3_LOC-5-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-3 )
			( PERSON-AT_P5_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-3 )
			(not ( PERSON-AT_P5_LOC-4-3 ))
			(not ( ACTIVE_LOC-5-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P0_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-5-4 )
			(not ( PERSON-AT_P0_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P1_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P1_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-5-4 )
			(not ( PERSON-AT_P1_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P2_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P2_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-5-4 )
			(not ( PERSON-AT_P2_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P3_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P3_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-5-4 )
			(not ( PERSON-AT_P3_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P4_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P4_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-5-4 )
			(not ( PERSON-AT_P4_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P5_LOC-4-4_LOC-5-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-5-4 )
			( PERSON-AT_P5_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-5-4 )
			(not ( PERSON-AT_P5_LOC-4-4 ))
			(not ( ACTIVE_LOC-5-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P0_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-4 )
			(not ( PERSON-AT_P0_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P1_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P1_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-5 )
			(not ( PERSON-AT_P1_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P5_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P5_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-0 )
			(not ( PERSON-AT_P5_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P5_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P5_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-1 )
			(not ( PERSON-AT_P5_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P5_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P5_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-2 )
			(not ( PERSON-AT_P5_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P5_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P5_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-3 )
			(not ( PERSON-AT_P5_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-4 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P1_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P1_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-5 )
			(not ( PERSON-AT_P1_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-4 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P1_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P1_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-5 )
			(not ( PERSON-AT_P1_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P5_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P5_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-1 )
			(not ( PERSON-AT_P5_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P5_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P5_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-2 )
			(not ( PERSON-AT_P5_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P5_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P5_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-3 )
			(not ( PERSON-AT_P5_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-4 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-4 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-4 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-4 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-4 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P5_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P5_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-4 )
			(not ( PERSON-AT_P5_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P2_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-4 )
			(not ( PERSON-AT_P2_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P2_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-4 )
			(not ( PERSON-AT_P2_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-0 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-0 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-0 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-0 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-0 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P5_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P5_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-0 )
			(not ( PERSON-AT_P5_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P5_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P5_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-1 )
			(not ( PERSON-AT_P5_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P5_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P5_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-2 )
			(not ( PERSON-AT_P5_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P1_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P1_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-4 )
			(not ( PERSON-AT_P1_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P2_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P2_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-5 )
			(not ( PERSON-AT_P2_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-0 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-0 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-0 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-0 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-0 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P5_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P5_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-0 )
			(not ( PERSON-AT_P5_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P5_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P5_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-1 )
			(not ( PERSON-AT_P5_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P5_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P5_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-2 )
			(not ( PERSON-AT_P5_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P5_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P5_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-4-3 )
			(not ( PERSON-AT_P5_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P1_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P1_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-4 )
			(not ( PERSON-AT_P1_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P2_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P2_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-5 )
			(not ( PERSON-AT_P2_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P5_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P5_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-0 )
			(not ( PERSON-AT_P5_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P5_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P5_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-1 )
			(not ( PERSON-AT_P5_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P5_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P5_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-2 )
			(not ( PERSON-AT_P5_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P5_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P5_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-3 )
			(not ( PERSON-AT_P5_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-4 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P2_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P2_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-5 )
			(not ( PERSON-AT_P2_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-4 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P2_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P2_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-5 )
			(not ( PERSON-AT_P2_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P5_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P5_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-1 )
			(not ( PERSON-AT_P5_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P5_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P5_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-2 )
			(not ( PERSON-AT_P5_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P5_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P5_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-3 )
			(not ( PERSON-AT_P5_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P3_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-4 )
			(not ( PERSON-AT_P3_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P3_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-4 )
			(not ( PERSON-AT_P3_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P5_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P5_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-0 )
			(not ( PERSON-AT_P5_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P5_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P5_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-1 )
			(not ( PERSON-AT_P5_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P5_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P5_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-2 )
			(not ( PERSON-AT_P5_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P2_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-4 )
			(not ( PERSON-AT_P2_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P3_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P3_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-5 )
			(not ( PERSON-AT_P3_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P5_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P5_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-0 )
			(not ( PERSON-AT_P5_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P5_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P5_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-1 )
			(not ( PERSON-AT_P5_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P5_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P5_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-2 )
			(not ( PERSON-AT_P5_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P5_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P5_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-3-3 )
			(not ( PERSON-AT_P5_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-0 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-0 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-0 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-0 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P5_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P5_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-0 )
			(not ( PERSON-AT_P5_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P5_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P5_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-1 )
			(not ( PERSON-AT_P5_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P5_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P5_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-2 )
			(not ( PERSON-AT_P5_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P5_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P5_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-3 )
			(not ( PERSON-AT_P5_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P2_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-4 )
			(not ( PERSON-AT_P2_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P3_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P3_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-4 )
			(not ( PERSON-AT_P3_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P4_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P5_LOC-2-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P5_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-4 )
			(not ( PERSON-AT_P5_LOC-2-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P3_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P3_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-5 )
			(not ( PERSON-AT_P3_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P4_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P4_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-5 )
			(not ( PERSON-AT_P4_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P5_LOC-2-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P5_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-5 )
			(not ( PERSON-AT_P5_LOC-2-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-4 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P3_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P3_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-5 )
			(not ( PERSON-AT_P3_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P5_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P5_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-1 )
			(not ( PERSON-AT_P5_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P5_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P5_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-2 )
			(not ( PERSON-AT_P5_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P5_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P5_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-3 )
			(not ( PERSON-AT_P5_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-4 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-4 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-4 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P5_LOC-2-3_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P5_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-4 )
			(not ( PERSON-AT_P5_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P3_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P3_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-5 )
			(not ( PERSON-AT_P3_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P4_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P4_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-5 )
			(not ( PERSON-AT_P4_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P5_LOC-2-4_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P5_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-5 )
			(not ( PERSON-AT_P5_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P5_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P5_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-0 )
			(not ( PERSON-AT_P5_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P5_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P5_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-1 )
			(not ( PERSON-AT_P5_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P5_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P5_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-2 )
			(not ( PERSON-AT_P5_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P5_LOC-2-4_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P5_LOC-2-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-3 )
			(not ( PERSON-AT_P5_LOC-2-4 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P4_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-4 )
			(not ( PERSON-AT_P4_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P5_LOC-2-5_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P5_LOC-2-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-4 )
			(not ( PERSON-AT_P5_LOC-2-5 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P5_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P5_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-0 )
			(not ( PERSON-AT_P5_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P5_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P5_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-1 )
			(not ( PERSON-AT_P5_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P5_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P5_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-2 )
			(not ( PERSON-AT_P5_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P5_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P5_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-3 )
			(not ( PERSON-AT_P5_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P3_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-4 )
			(not ( PERSON-AT_P3_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P4_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P4_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-4 )
			(not ( PERSON-AT_P4_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P5_LOC-1-4_LOC-2-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-4 )
			( PERSON-AT_P5_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-4 )
			(not ( PERSON-AT_P5_LOC-1-4 ))
			(not ( ACTIVE_LOC-2-4 ))
		)
	)
	(:action MOVE_P4_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P4_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-5 )
			(not ( PERSON-AT_P4_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P5_LOC-1-5_LOC-2-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-5 )
			( PERSON-AT_P5_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-2-5 )
			(not ( PERSON-AT_P5_LOC-1-5 ))
			(not ( ACTIVE_LOC-2-5 ))
		)
	)
	(:action MOVE_P0_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P5_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P5_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-1 )
			(not ( PERSON-AT_P5_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P5_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P5_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-2 )
			(not ( PERSON-AT_P5_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P5_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P5_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-3 )
			(not ( PERSON-AT_P5_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-4 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P5_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P5_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-4 )
			(not ( PERSON-AT_P5_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P4_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P4_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-5 )
			(not ( PERSON-AT_P4_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P5_LOC-1-4_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P5_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-5 )
			(not ( PERSON-AT_P5_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-0 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-0 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-0 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-0 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P5_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P5_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-0 )
			(not ( PERSON-AT_P5_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P5_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P5_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-1 )
			(not ( PERSON-AT_P5_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P5_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P5_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-2 )
			(not ( PERSON-AT_P5_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P5_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P5_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-3 )
			(not ( PERSON-AT_P5_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P5_LOC-1-5_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P5_LOC-1-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-4 )
			(not ( PERSON-AT_P5_LOC-1-5 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-0-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-0-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-0-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-0-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-0-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-0-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-0-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-0-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-0-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-0-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-0-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-0-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-0-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-0-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-0-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P5_LOC-0-5_LOC-1-5
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-5 )
			( PERSON-AT_P5_LOC-0-5 )
		)
		:effect
		(and
			( PERSON-AT_P5_LOC-1-5 )
			(not ( PERSON-AT_P5_LOC-0-5 ))
			(not ( ACTIVE_LOC-1-5 ))
		)
	)

)
