from picunsolv.constants import *
from picunsolv.trial_based_pic_identification import TrialBasedIdentification

import unittest


class TrialTestCase(unittest.TestCase):
    def test_basic_trial(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p4'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1'])},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set(['p1'])},
                          'a3': {POS_PREC: set(['p2']),
                                 ADDS: set(['p3'])},
                          'a4': {POS_PREC: set(['p3', 'p1']),
                                 ADDS: set(['p4'])},
                          }
                 }
        relaxed_plan = ['a1', 'a2', 'a3', 'a4']
        predc_map = {'p1':'a1', 'p2':'a2', 'p3':'a3'}
        expected_clause = set([frozenset(sorted(['p1', 'p2'])), frozenset(sorted(['p1', 'p3']))])
        trial_exec = TrialBasedIdentification()
        clause = trial_exec.test_relaxed_plan(model, model, relaxed_plan, predc_map)
        set_clause = set()
        print (clause)
        for cl in clause:
            set_clause.add(frozenset(sorted(list(cl))))
        assert set_clause == expected_clause

    def test_missing_prop(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p4'])},
                 DOMAIN: {'a1': {POS_PREC: set(['p0']),
                                 ADDS: set(['p1'])},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2'])
                                 },
                          'a3': {POS_PREC: set(['p2']),
                                 ADDS: set(['p3'])},
                          'a4': {POS_PREC: set(['p3', 'p1']),
                                 ADDS: set(['p4'])},
                          }
                 }
        relaxed_plan = ['a1', 'a2', 'a3', 'a4']
        predc_map = {'p1':'a1', 'p2':'a2', 'p3':'a3'}
        expected_clause = set([frozenset(sorted(['p0']))])
        trial_exec = TrialBasedIdentification()
        clause = trial_exec.test_relaxed_plan(model, model, relaxed_plan, predc_map)
        set_clause = set()
        print (clause)
        for cl in clause:
            set_clause.add(frozenset(sorted(list(cl))))
        assert set_clause == expected_clause

    def test_parallel_error(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p4'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1'])},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set(['p1'])},
                          'a3': {POS_PREC: set(['p1']),
                                 ADDS: set(['p3'])},
                          'a4': {POS_PREC: set(['p2', 'p3']),
                                 ADDS: set(['p4'])},
                          }
                 }
        relaxed_plan = ['a1', 'a2', 'a3', 'a4']
        predc_map = {'p1':'a1', 'p2':'a2', 'p3':'a3'}
        expected_clause = set([frozenset(sorted(['p1', 'p2'])), frozenset(sorted(['p2', 'p3']))])
        trial_exec = TrialBasedIdentification()
        clause = trial_exec.test_relaxed_plan(model, model, relaxed_plan, predc_map)
        set_clause = set()
        print (clause)
        for cl in clause:
            set_clause.add(frozenset(sorted(list(cl))))
        assert set_clause == expected_clause




if __name__ == '__main__':
    unittest.main()
