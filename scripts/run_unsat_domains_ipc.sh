mkdir -p ../LOGS_SYS_EVAL/$1/
for i in `ls ../domains/ipc_comp_grounded/$1/`
do
    echo $i
    mkdir -p ../LOGS_SYS_EVAL/${1}/${i}/
    python ../src/picunsolv/main.py ../domains/ipc_comp_grounded/${1}/${i}/domain.pddl ../domains/ipc_comp_grounded/${1}/${i}/problem.pddl > ../LOGS_SYS_EVAL/${1}/${i}/results.logs
done
