import copy
import time
from picunsolv.Search import BFSSearch, HillClimb, AstarSearch
from picunsolv.pim_generator import PimGenerator
from picunsolv.rpg import RPG
from picunsolv.Concretizer import Concretizer
from picunsolv.picce_generator import PicCeGenerator
from picunsolv.constants import *
from picunsolv.utils import powerset, run_planner
from picunsolv.trial_based_pic_identification import TrialBasedIdentification
from picunsolv.best_supporter_graph import BestSupporterGraph
from picunsolv.model_writer import ModelWriter
class Driver(object):
    def __init__(self, model, prop_set):
        self.original_model = model
        self.prop_set = prop_set
        self.rpg = RPG()
        self.concretizer = Concretizer(model)
        self.picce_gen = PicCeGenerator()
        self.pim_gen = PimGenerator()
        self.trial_exec = TrialBasedIdentification()
        self.bsg_gen = BestSupporterGraph()
        self.nodes_expanded = 0

        self.time_taken_for_successor_generation = 0
        self.time_taken_relaxed_plan_generation = 0
        self.time_taken_for_relaxed_plan_generation_in_succ = 0
        self.time_taken_for_relaxed_plan_generation_in_succ_actual_plan_extraction = 0
        self.time_taken_for_relaxed_plan_generation_in_succ_cond_free_model = 0
        self.time_taken_for_relaxed_plan_generation_in_succ_compiled_model = 0
        self.time_taken_for_relaxed_plan_generation_in_succ_abs_model = 0
        self.time_taken_for_deduplication = 0
        self.time_taken_relaxed_for_trials = 0
        self.time_taken_for_abstraction = 0
        self.time_taken_for_compilation = 0
        self.time_taken_for_indiv_clauses = 0
        self.time_taken_for_succ_gen_goal_test = 0
        self.no_of_succ_generated = 0


        self.old_clauses = None


    def get_relaxed_plan(self, curr_prop_set, curr_clauses):


        start_time_for_compiled_model = time.time()
        original_compiled_model = self.picce_gen.get_semi_relaxed_model(self.original_model, curr_clauses)
        self.time_taken_for_relaxed_plan_generation_in_succ_compiled_model += (time.time() - start_time_for_compiled_model)

        start_time_for_cond_free_model = time.time()
        updated_model = self.bsg_gen.get_condition_free_models(original_compiled_model)
        self.time_taken_for_relaxed_plan_generation_in_succ_cond_free_model += (time.time() - start_time_for_cond_free_model)
        
        start_time_for_abs_model = time.time()
        updated_abs_model = self.concretizer.get_current_abstraction_for_other_model(updated_model, curr_prop_set)
        self.time_taken_for_relaxed_plan_generation_in_succ_abs_model +=  (time.time() - start_time_for_abs_model)

        start_time_for_relaxed_plan = time.time()
        relaxed_plan, predc_map, abs_updated_model = self.bsg_gen.get_relaxed_plan(updated_abs_model, is_updated_model=True)
        self.time_taken_for_relaxed_plan_generation_in_succ_actual_plan_extraction += (time.time() - start_time_for_relaxed_plan)

        start_time_for_dedup = time.time()
        dedup_plan = self.get_deuplicated_plan_length(relaxed_plan, updated_abs_model)
        self.time_taken_for_deduplication += (time.time() - start_time_for_dedup)
        return relaxed_plan, len(dedup_plan)


    def find_minimal_set(self):

        plan, plan_cost = self.get_relaxed_plan(self.original_model[INSTANCE][GOAL], [])
        start_node = [copy.deepcopy(self.original_model[INSTANCE][GOAL]), plan, plan_cost, []]
        search_start_time = time.time()
        node = AstarSearch(start_node, self.goal_test, self.successor_generator, cost=self.get_cost, heuristic=self.get_heuristic, start_time = search_start_time, search_limit = 5*60)

        print("Final>> Time spent on successor generation", self.time_taken_for_successor_generation)
        print("Final>> Time spent on relaxed plan generation", self.time_taken_relaxed_plan_generation)
        print("Final>> Time spent on relaxed plan generation in succ", self.time_taken_for_relaxed_plan_generation_in_succ)
        print("Final>> Time spent on relaxed plan generation in succ - actual plan extraction", self.time_taken_for_relaxed_plan_generation_in_succ_actual_plan_extraction)
        print("Final>> Time spent on relaxed plan generation in succ - extract cond model", self.time_taken_for_relaxed_plan_generation_in_succ_cond_free_model)
        print("Final>> Time spent on relaxed plan generation in succ - extract compiled model", self.time_taken_for_relaxed_plan_generation_in_succ_compiled_model)
        print("Final>> Time spent on relaxed plan generation in succ - extract abs model", self.time_taken_for_relaxed_plan_generation_in_succ_abs_model)
        print("Final>> Time spent on indiv fluent identification in succ", self.time_taken_for_indiv_clauses)
        print("Final>> Time spent on goal test in succ", self.time_taken_for_succ_gen_goal_test)
        print("Final>> Time spent on pic extraction", self.time_taken_relaxed_for_trials)
        print("Final>> Time spent on plan deduplication", self.time_taken_for_deduplication)
        print("Final>> Time spent on abstraction", self.time_taken_for_abstraction)
        print("Final>> Time spent on compilation", self.time_taken_for_compilation)

        print("Final>> No of nodes expanded", self.nodes_expanded)
        print("Final>> No of successors generated", self.no_of_succ_generated)

        return node

    def successor_generator(self, curr_node):
        self.nodes_expanded += 1
        start_time_for_succ_gen = time.time()
        curr_clauses = curr_node[-1]
        start_time_for_compilation = time.time()
        original_compiled_model = self.picce_gen.get_semi_relaxed_model(self.original_model, curr_clauses)
        updated_model = self.bsg_gen.get_condition_free_models(original_compiled_model)
        self.time_taken_for_compilation += (time.time() - start_time_for_compilation)

        start_time_for_abstraction = time.time()
        updated_abs_model = self.concretizer.get_current_abstraction_for_other_model(updated_model, curr_node[0])
        self.time_taken_for_abstraction += (time.time() - start_time_for_abstraction)

        start_time_for_relaxed_plan = time.time()
        relaxed_plan, predc_map, abs_updated_model = self.bsg_gen.get_relaxed_plan(updated_abs_model, is_updated_model=True)
        self.time_taken_relaxed_plan_generation += (time.time() - start_time_for_relaxed_plan)

        start_time_for_trials = time.time()
        clauses_list = self.trial_exec.test_relaxed_plan(self.original_model, updated_model, relaxed_plan, curr_clauses)
        self.time_taken_relaxed_for_trials += (time.time() - start_time_for_trials)

        if len(clauses_list) == 0:
            print ("Something is wrong here...", curr_node)
            exit(1)

        succ_states = []
        for clauses in clauses_list:
            self.no_of_succ_generated += 1
            start_time_for_cl_list = time.time()
            new_model = copy.deepcopy(curr_node[0])

            NO_NEW_CL = True
            for cl in clauses:
                if cl not in curr_clauses:
                    NO_NEW_CL = False

                for fct in cl:
                    new_model.add(fct)
            if DEBUG_LEVEL > 0:

                if NO_NEW_CL:
                    print("duplicate clause", clauses)
                    for act in relaxed_plan:
                        print(act, updated_abs_model[DOMAIN][act])
                    exit(0)
            self.time_taken_for_indiv_clauses += (time.time() - start_time_for_cl_list)

            dummy_succ_node = [new_model,  curr_node[-1] + clauses]

            start_time_for_goal_test = time.time()
            goal_test_status = self.goal_test(dummy_succ_node)
            self.time_taken_for_succ_gen_goal_test += (time.time() - start_time_for_goal_test)

            if not goal_test_status:
                start_time_for_relaxed_plan_in_succ = time.time()
                plan, plan_cost = self.get_relaxed_plan(new_model, curr_node[-1] + clauses)
                curr_relax_plan_time = time.time() - start_time_for_relaxed_plan_in_succ
                print("MIDDLE>> Time spent on indiv relaxed plan generation", curr_relax_plan_time)
                self.time_taken_for_relaxed_plan_generation_in_succ += curr_relax_plan_time
                succ_node = [new_model, plan, plan_cost, curr_node[-1] + clauses]
            else:
                succ_node = [new_model, [], HEURISTIC_UPPER_BOUND, curr_node[-1] + clauses]
            succ_states.append(succ_node)

        if len(succ_states) == 0:
            print ("Something is wrong here ...2")
            print (clauses_list)
            print ("prev_CLAUSE, ",curr_clauses)
            exit(1)

        self.time_taken_for_successor_generation += (time.time() - start_time_for_succ_gen)
        return succ_states

    def goal_test(self, curr_node, exhaustive=True):
        # TODO: Run exhaustive search
        curr_model = self.concretizer.get_current_abstraction(curr_node[0])
        compiled_model = self.picce_gen.get_semi_relaxed_model(curr_model, curr_node[-1])
        if not self.rpg.test_reachability(compiled_model):

            mw = ModelWriter(curr_model)
            mw.write_files("/tmp/unsdomain.pddl", "/tmp/unsprob.pddl")
            return True

        return False

    def successor_generator_just_abs(self, curr_node):
        succ_states = []
        for fl in self.prop_set - curr_node[0]: 
            succ_node = [curr_node[0] | set([fl]), curr_node[-1] +  [fl]]
            succ_states.append(succ_node)

        return succ_states


    def goal_test_with_plan(self, curr_node, exhaustive=True):
        curr_model = self.concretizer.get_current_abstraction(curr_node[0])
        if len(run_planner(curr_model)) == 0:
            return True
        return False


    def get_deuplicated_plan_length(self, plan, conj_model):
        # find the indexes of actions and the corresponding
        IS_CONVERGED = False
        current_plan = copy.deepcopy(plan)
        #print (conj_model)


        succ_map = self.trial_exec.get_succ_map(plan, conj_model)
        #print ("Original Succ map", succ_map)
        while not IS_CONVERGED:
            IS_CONVERGED = True
            act_ids_remaining = []
            act_to_be_merged = {}
            new_plan = []
            for act_id in range(len(current_plan)):
                act = current_plan[act_id]
                can_be_merged = False
                for prev_act_id in range(act_id):
                    prev_act = current_plan[prev_act_id]
                    if prev_act_id  in act_ids_remaining and self.trial_exec.check_if_same_action(act, prev_act) and not can_be_merged:
                        if not self.trial_exec.check_if_path_exists_new(act_id, prev_act_id, succ_map):
                            can_be_merged = True
                            IS_CONVERGED = False
                            if prev_act_id not in act_to_be_merged:
                                act_to_be_merged[prev_act_id] = set()
                            act_to_be_merged[prev_act_id].add(act)
                        #else:
                        #    print ("Can not be merged!!!", act_id, prev_act_id)
                if not can_be_merged:
                    act_ids_remaining.append(act_id)


            for act_id in range(len(current_plan)):
                act = current_plan[act_id]
                if act_id in act_ids_remaining:
                    if act_id in act_to_be_merged:
                        new_act_name = act + MERGE_SEPARATOR + MERGE_SEPARATOR.join(list(act_to_be_merged[act_id]))
                    else:
                        new_act_name = act
                    new_plan.append(new_act_name)


            current_plan = copy.deepcopy(new_plan)
            succ_map = self.trial_exec.get_succ_map(new_plan, conj_model)
            #print("Succ map", succ_map)
        return current_plan

    def get_heuristic(self, curr_node):
        return (HEURISTIC_UPPER_BOUND - curr_node[2])#/HEURISTIC_UPPER_BOUND


    def get_cost(self, curr_node):
        total_cost = 0
        for cl in curr_node:
            if len(cl) == 1:
                total_cost += FLUENT_COST
            else:
                total_cost += CONJ_COST
        return total_cost

