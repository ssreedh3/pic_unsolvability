rm /tmp/grounder_obs
rm  pr-domain.pddl
rm pr-problem.pddl
touch  /tmp/grounder_obs
~/mycode/obs-compiler/pr2plan -d $1 -i $2 -o /tmp/grounder_obs
sed -i '/(= (total-cost) 0)/d' pr-problem.pddl
sed -i '/(:metric minimize (total-cost))/d' pr-problem.pddl
sed -i '/( EXPLAINED_FULL_OBS_SEQUENCE )/d' pr-problem.pddl
sed -i '/( NOT_EXPLAINED_FULL_OBS_SEQUENCE )/d' pr-problem.pddl
sed -i '/( EXPLAINED_FULL_OBS_SEQUENCE )/d' pr-domain.pddl
sed -i '/( NOT_EXPLAINED_FULL_OBS_SEQUENCE )/d' pr-domain.pddl
sed -i '/(increase (total-cost) 1)/d' pr-domain.pddl
sed -i '/(:functions (total-cost))/d' pr-domain.pddl
mv pr-domain.pddl ${3}/domain.pddl
mv pr-problem.pddl ${3}/problem.pddl
