import unittest
from picunsolv.driver import Driver
from picunsolv.constants import *


class PlanMergeTestCase(unittest.TestCase):
    def test_basic(self):
        model = {INSTANCE: {INIT: set(), GOAL: set(['p4'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1']),
                                 DELS: set()},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2']),
                                 DELS: set()},
                          'a3': {POS_PREC: set(['p2', 'p1']),
                                 ADDS: set(['p3']),
                                 DELS: set()
                                 },
                          'a4': {POS_PREC: set(['p3', 'p2']),
                                 ADDS: set(['p4']),
                                 DELS: set()
                                 }
                          }
                 }
        prop_set = set()
        curr_driver = Driver(model, prop_set)
        original_compiled_model = curr_driver.picce_gen.get_semi_relaxed_model(curr_driver.original_model, [set(['p1','p2'])])
        updated_model = curr_driver.bsg_gen.get_condition_free_models(original_compiled_model)
        updated_plan = curr_driver.get_deuplicated_plan_length(['a1', 'a2#1', 'a3', 'a2', 'a4'], updated_model)
        print ("plan", updated_plan)
        assert updated_plan == ['a1', 'a2#1+a2', 'a3', 'a4']




if __name__ == '__main__':
    unittest.main()
