import tempfile
from itertools import chain, combinations
from picunsolv.model_writer import ModelWriter
import os

PLANNING_CMD = "./run_planner.sh {} {}"


# Taken from ITERTOOLS recipes
def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def powerset_to_limit(iterable, m):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    if m >= len(s):
        return chain.from_iterable(combinations(s, r) for r in range(m+1))
    else:
        return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def run_planner(model):
#    fd, problem_file = tempfile.mkstemp()
#    os.close(fd)
#    fd, domain_file = tempfile.mkstemp()
#    os.close(fd)
    problem_file = "/tmp/problem.pddl"
    domain_file = "/tmp/domain.pddl"
    mw = ModelWriter(model)
    mw.write_files(domain_file, problem_file)

    # Run the planner
    plan = [act.strip() for act in os.popen(PLANNING_CMD.format(domain_file, problem_file)).read().split()]

#    os.remove(domain_file)
#    os.remove(problem_file)
    return plan

