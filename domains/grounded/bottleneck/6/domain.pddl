(define
	(domain grounded-RESOURCES)
	(:requirements :strips :action-costs)
	(:predicates
		( PERSON-AT_P4_LOC-1-4 )
		( PERSON-AT_P3_LOC-1-3 )
		( PERSON-AT_P2_LOC-1-2 )
		( PERSON-AT_P1_LOC-1-1 )
		( PERSON-AT_P0_LOC-1-0 )
		( PERSON-AT_P4_LOC-1-3 )
		( PERSON-AT_P4_LOC-1-2 )
		( PERSON-AT_P3_LOC-1-2 )
		( PERSON-AT_P4_LOC-1-1 )
		( PERSON-AT_P3_LOC-1-1 )
		( PERSON-AT_P2_LOC-1-1 )
		( PERSON-AT_P4_LOC-1-0 )
		( PERSON-AT_P3_LOC-1-0 )
		( PERSON-AT_P2_LOC-1-0 )
		( PERSON-AT_P1_LOC-1-0 )
		( PERSON-AT_P3_LOC-1-4 )
		( PERSON-AT_P2_LOC-1-3 )
		( PERSON-AT_P1_LOC-1-2 )
		( PERSON-AT_P0_LOC-1-1 )
		( PERSON-AT_P4_LOC-2-3 )
		( PERSON-AT_P3_LOC-2-3 )
		( PERSON-AT_P2_LOC-2-3 )
		( PERSON-AT_P4_LOC-2-2 )
		( PERSON-AT_P3_LOC-2-2 )
		( PERSON-AT_P2_LOC-2-2 )
		( PERSON-AT_P1_LOC-2-2 )
		( PERSON-AT_P4_LOC-2-1 )
		( PERSON-AT_P3_LOC-2-1 )
		( PERSON-AT_P2_LOC-2-1 )
		( PERSON-AT_P1_LOC-2-1 )
		( PERSON-AT_P0_LOC-2-1 )
		( PERSON-AT_P4_LOC-2-0 )
		( PERSON-AT_P3_LOC-2-0 )
		( PERSON-AT_P2_LOC-2-0 )
		( PERSON-AT_P1_LOC-2-0 )
		( PERSON-AT_P0_LOC-2-0 )
		( PERSON-AT_P1_LOC-2-3 )
		( PERSON-AT_P0_LOC-2-2 )
		( PERSON-AT_P2_LOC-1-4 )
		( PERSON-AT_P1_LOC-1-3 )
		( PERSON-AT_P0_LOC-1-2 )
		( PERSON-AT_P4_LOC-3-3 )
		( PERSON-AT_P3_LOC-3-3 )
		( PERSON-AT_P2_LOC-3-3 )
		( PERSON-AT_P1_LOC-3-3 )
		( PERSON-AT_P4_LOC-3-2 )
		( PERSON-AT_P3_LOC-3-2 )
		( PERSON-AT_P2_LOC-3-2 )
		( PERSON-AT_P1_LOC-3-2 )
		( PERSON-AT_P0_LOC-3-2 )
		( PERSON-AT_P4_LOC-3-1 )
		( PERSON-AT_P3_LOC-3-1 )
		( PERSON-AT_P2_LOC-3-1 )
		( PERSON-AT_P1_LOC-3-1 )
		( PERSON-AT_P0_LOC-3-1 )
		( PERSON-AT_P4_LOC-3-0 )
		( PERSON-AT_P3_LOC-3-0 )
		( PERSON-AT_P2_LOC-3-0 )
		( PERSON-AT_P1_LOC-3-0 )
		( PERSON-AT_P0_LOC-3-0 )
		( PERSON-AT_P4_LOC-3-4 )
		( PERSON-AT_P3_LOC-3-4 )
		( PERSON-AT_P2_LOC-3-4 )
		( PERSON-AT_P1_LOC-3-4 )
		( PERSON-AT_P0_LOC-3-3 )
		( PERSON-AT_P0_LOC-2-3 )
		( PERSON-AT_P1_LOC-1-4 )
		( PERSON-AT_P0_LOC-1-3 )
		( PERSON-AT_P4_LOC-4-4 )
		( PERSON-AT_P3_LOC-4-4 )
		( PERSON-AT_P2_LOC-4-4 )
		( PERSON-AT_P1_LOC-4-4 )
		( PERSON-AT_P4_LOC-4-3 )
		( PERSON-AT_P3_LOC-4-3 )
		( PERSON-AT_P2_LOC-4-3 )
		( PERSON-AT_P1_LOC-4-3 )
		( PERSON-AT_P0_LOC-4-3 )
		( PERSON-AT_P4_LOC-4-2 )
		( PERSON-AT_P3_LOC-4-2 )
		( PERSON-AT_P2_LOC-4-2 )
		( PERSON-AT_P1_LOC-4-2 )
		( PERSON-AT_P0_LOC-4-2 )
		( PERSON-AT_P4_LOC-4-1 )
		( PERSON-AT_P3_LOC-4-1 )
		( PERSON-AT_P2_LOC-4-1 )
		( PERSON-AT_P1_LOC-4-1 )
		( PERSON-AT_P0_LOC-4-1 )
		( PERSON-AT_P4_LOC-4-0 )
		( PERSON-AT_P3_LOC-4-0 )
		( PERSON-AT_P2_LOC-4-0 )
		( PERSON-AT_P1_LOC-4-0 )
		( PERSON-AT_P0_LOC-4-0 )
		( PERSON-AT_P0_LOC-4-4 )
		( PERSON-AT_P0_LOC-3-4 )
		( PERSON-AT_P0_LOC-1-4 )
		( ACTIVE_LOC-1-3 )
		( ACTIVE_LOC-3-3 )
		( ACTIVE_LOC-4-3 )
		( ACTIVE_LOC-4-4 )
		( ACTIVE_LOC-3-0 )
		( ACTIVE_LOC-3-1 )
		( ACTIVE_LOC-3-2 )
		( ACTIVE_LOC-3-4 )
		( ACTIVE_LOC-1-4 )
		( ACTIVE_LOC-4-1 )
		( ACTIVE_LOC-4-2 )
		( ACTIVE_LOC-1-2 )
		( ACTIVE_LOC-2-2 )
		( ACTIVE_LOC-4-0 )
		( ACTIVE_LOC-2-3 )
		( ACTIVE_LOC-2-0 )
		( ACTIVE_LOC-2-1 )
		( ACTIVE_LOC-1-1 )
		( ACTIVE_LOC-1-0 )
		( PERSON-AT_P0_LOC-0-0 )
		( PERSON-AT_P1_LOC-0-1 )
		( PERSON-AT_P2_LOC-0-2 )
		( PERSON-AT_P3_LOC-0-3 )
		( PERSON-AT_P4_LOC-0-4 )
	) 
	(:action MOVE_P0_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-4_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-3-4 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P0_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-4 )
			(not ( PERSON-AT_P0_LOC-3-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-4-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-4-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-4_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P0_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-4 )
			(not ( PERSON-AT_P0_LOC-4-4 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P1_LOC-4-4_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P1_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-4 )
			(not ( PERSON-AT_P1_LOC-4-4 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P2_LOC-4-4_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P2_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-4 )
			(not ( PERSON-AT_P2_LOC-4-4 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P3_LOC-4-4_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P3_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-4 )
			(not ( PERSON-AT_P3_LOC-4-4 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P4_LOC-4-4_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P4_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-4 )
			(not ( PERSON-AT_P4_LOC-4-4 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-4 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-4 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P0_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-0_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-4-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-4-0 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-4 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-4 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-4 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-4 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-4 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-4_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-3-4 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-4_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-3-4 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-4_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-3-4 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-4_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-3-4 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P0_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-0 )
			(not ( PERSON-AT_P0_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P1_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P1_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-0 )
			(not ( PERSON-AT_P1_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P2_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P2_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-0 )
			(not ( PERSON-AT_P2_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P3_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P3_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-0 )
			(not ( PERSON-AT_P3_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P4_LOC-4-1_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P4_LOC-4-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-0 )
			(not ( PERSON-AT_P4_LOC-4-1 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P0_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-4-2_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-4-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-4-2 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-4-3_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-4-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-4-3 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-4-4_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-4-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-4-4 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-0 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-0 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-0 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-0 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-4-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-0 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-0 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-4-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-1 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-1 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-1 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-1 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-4-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-1 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-1 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-4-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-2 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-2 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-2 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-2 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-4-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-2 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-2 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-4-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-4-3 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-3 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-3 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-3 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-4-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-3 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-3 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-4-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P1_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-4-4 )
			(not ( PERSON-AT_P1_LOC-3-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P2_LOC-3-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P2_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-4-4 )
			(not ( PERSON-AT_P2_LOC-3-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P3_LOC-3-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P3_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-4-4 )
			(not ( PERSON-AT_P3_LOC-3-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P4_LOC-3-4_LOC-4-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-4-4 )
			( PERSON-AT_P4_LOC-3-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-4-4 )
			(not ( PERSON-AT_P4_LOC-3-4 ))
			(not ( ACTIVE_LOC-4-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-3 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-4 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-3 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-0_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-3-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-3-0 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-3 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-4 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-4 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-4 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-3-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-4 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-4 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-4 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P0_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-3-1_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-3-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-3-1 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-3-2_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-3-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-3-2 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-3-3_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-3-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-3-3 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P0_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-0 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-0 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-0 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-0 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-3-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-0 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-0 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-3-0 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-1 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-1 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-1 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-1 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-3-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-1 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-1 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-3-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-3-2 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-2 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-2 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-2 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-3-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-2 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-2 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-3-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-3-3 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-3-3 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-3-3 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-3-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-3-3 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-3-3 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-3-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-0 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-0 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-0 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-0 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-2 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-3 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-4 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P0_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-0_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-2-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-2-0 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-2 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-3 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-2-1_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-2-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-2-1 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-2-2_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-2-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-2-2 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-2-3_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-2-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-2-3 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P0_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P0_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-0 )
			(not ( PERSON-AT_P0_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P1_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P1_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-0 )
			(not ( PERSON-AT_P1_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P2_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-0 )
			(not ( PERSON-AT_P2_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P3_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P3_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-0 )
			(not ( PERSON-AT_P3_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P4_LOC-1-0_LOC-2-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-0 )
			( PERSON-AT_P4_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-0 )
			(not ( PERSON-AT_P4_LOC-1-0 ))
			(not ( ACTIVE_LOC-2-0 ))
		)
	)
	(:action MOVE_P0_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P0_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-2-1 )
			(not ( PERSON-AT_P0_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-1 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-1 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-1 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-2-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-1 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-1 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-2-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P1_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-2-2 )
			(not ( PERSON-AT_P1_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-2 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-2 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-2-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-2 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-2 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-2-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P2_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-2-3 )
			(not ( PERSON-AT_P2_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-2-3 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-2-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-2-3 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-2-3 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-2-3 ))
		)
	)
	(:action MOVE_P0_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P0_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-1 )
			(not ( PERSON-AT_P0_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-0_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-1-0 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-1-0 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-2 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-3 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-4 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)
	(:action MOVE_P1_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P1_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-0 )
			(not ( PERSON-AT_P1_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P2_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-0 )
			(not ( PERSON-AT_P2_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P3_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P3_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-0 )
			(not ( PERSON-AT_P3_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P4_LOC-1-1_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P4_LOC-1-1 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-0 )
			(not ( PERSON-AT_P4_LOC-1-1 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P2_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P2_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-1 )
			(not ( PERSON-AT_P2_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P3_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-1 )
			(not ( PERSON-AT_P3_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P4_LOC-1-2_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P4_LOC-1-2 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-1 )
			(not ( PERSON-AT_P4_LOC-1-2 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P3_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P3_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-2 )
			(not ( PERSON-AT_P3_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-3_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P4_LOC-1-3 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-2 )
			(not ( PERSON-AT_P4_LOC-1-3 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P4_LOC-1-4_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P4_LOC-1-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-3 )
			(not ( PERSON-AT_P4_LOC-1-4 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P0_LOC-0-0_LOC-1-0
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-0 )
			( PERSON-AT_P0_LOC-0-0 )
		)
		:effect
		(and
			( PERSON-AT_P0_LOC-1-0 )
			(not ( PERSON-AT_P0_LOC-0-0 ))
			(not ( ACTIVE_LOC-1-0 ))
		)
	)
	(:action MOVE_P1_LOC-0-1_LOC-1-1
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-1 )
			( PERSON-AT_P1_LOC-0-1 )
		)
		:effect
		(and
			( PERSON-AT_P1_LOC-1-1 )
			(not ( PERSON-AT_P1_LOC-0-1 ))
			(not ( ACTIVE_LOC-1-1 ))
		)
	)
	(:action MOVE_P2_LOC-0-2_LOC-1-2
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-2 )
			( PERSON-AT_P2_LOC-0-2 )
		)
		:effect
		(and
			( PERSON-AT_P2_LOC-1-2 )
			(not ( PERSON-AT_P2_LOC-0-2 ))
			(not ( ACTIVE_LOC-1-2 ))
		)
	)
	(:action MOVE_P3_LOC-0-3_LOC-1-3
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-3 )
			( PERSON-AT_P3_LOC-0-3 )
		)
		:effect
		(and
			( PERSON-AT_P3_LOC-1-3 )
			(not ( PERSON-AT_P3_LOC-0-3 ))
			(not ( ACTIVE_LOC-1-3 ))
		)
	)
	(:action MOVE_P4_LOC-0-4_LOC-1-4
		:parameters ()
		:precondition
		(and
			( ACTIVE_LOC-1-4 )
			( PERSON-AT_P4_LOC-0-4 )
		)
		:effect
		(and
			( PERSON-AT_P4_LOC-1-4 )
			(not ( PERSON-AT_P4_LOC-0-4 ))
			(not ( ACTIVE_LOC-1-4 ))
		)
	)

)
