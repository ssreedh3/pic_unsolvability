from picunsolv.constants import *

class Concretizer(object):
    def __init__(self, original_model):
        self.original_model = original_model

    def get_current_abstraction(self, fluent_set):
        # Go through initial state, goal and action parts
        # and do intersection
        new_model = {}
        new_model[INSTANCE] = {}
        #new_model[INSTANCE][INIT] = self.original_model[INSTANCE][INIT] & fluent_set

        # Add all conjunctions (because it has to be part of the abstraction)
        new_model[INSTANCE][INIT] = set()
        for pred in self.original_model[INSTANCE][INIT]:
            if CONJ_PREFIX in pred or pred in fluent_set:
                new_model[INSTANCE][INIT].add(pred)

        new_model[INSTANCE][GOAL] = set()
        #self.original_model[INSTANCE][GOAL] & fluent_set
        for pred in self.original_model[INSTANCE][GOAL]:
            if CONJ_PREFIX in pred or pred in fluent_set:
                new_model[INSTANCE][GOAL].add(pred)


        new_model[DOMAIN] = {}
        for act in self.original_model[DOMAIN]:
            new_model[DOMAIN][act] = {}
            for part in self.original_model[DOMAIN][act]:
                if part == PARARMETERS:
                    new_model[DOMAIN][act][part] = self.original_model[DOMAIN][act][part]
                    # TODO: Assuming no conditional effects in the
                elif part not in [COND_ADDS, COND_DELS]:
                    new_model[DOMAIN][act][part] = set()
                    for pred in self.original_model[DOMAIN][act][part]:
                        if CONJ_PREFIX in pred or pred in fluent_set:
                            new_model[DOMAIN][act][part].add(pred)
                    #new_model[DOMAIN][act][part] = self.original_model[DOMAIN][act][part] & fluent_set
        return new_model


    def get_current_abstraction_for_other_model(self, other_model, fluent_set):
        # Go through initial state, goal and action parts
        # and do intersection
        new_model = {}
        new_model[INSTANCE] = {}
        #new_model[INSTANCE][INIT] = self.original_model[INSTANCE][INIT] & fluent_set

        # Add all conjunctions (because it has to be part of the abstraction)
        new_model[INSTANCE][INIT] = set()
        for pred in other_model[INSTANCE][INIT]:
            if CONJ_PREFIX in pred or pred in fluent_set:
                new_model[INSTANCE][INIT].add(pred)

        new_model[INSTANCE][GOAL] = set()
        #self.original_model[INSTANCE][GOAL] & fluent_set
        for pred in other_model[INSTANCE][GOAL]:
            if CONJ_PREFIX in pred or pred in fluent_set:
                new_model[INSTANCE][GOAL].add(pred)


        new_model[DOMAIN] = {}
        for act in other_model[DOMAIN]:
            new_model[DOMAIN][act] = {}
            for part in other_model[DOMAIN][act]:
                if part == PARARMETERS:
                    new_model[DOMAIN][act][part] = other_model[DOMAIN][act][part]
                    # TODO: Assuming no conditional effects in the
                elif part not in [COND_ADDS, COND_DELS]:
                    new_model[DOMAIN][act][part] = set()
                    for pred in other_model[DOMAIN][act][part]:
                        if CONJ_PREFIX in pred or pred in fluent_set:
                            new_model[DOMAIN][act][part].add(pred)
                    #new_model[DOMAIN][act][part] = self.original_model[DOMAIN][act][part] & fluent_set
        return new_model


