from picunsolv.constants import *
from picunsolv.utils import powerset

import copy

class PicCeGenerator:
    def __init__(self):
        pass

    def get_conj_fluent(self, conjunct):
        if len(conjunct) > 1:
            #print ("Here we are getting a conj var", conjunct)
            #exit(1)
            return CONJ_PREFIX + CONJ_SEPARATOR.join(sorted(list(conjunct)))
        elif len(conjunct) == 1:
            return list(conjunct)[0]
        else:
            return ''

    def get_powc(self, fluent_set, conjunct_set):
        new_set = set()
        new_set |= fluent_set
        for conj in conjunct_set:
            if conj <= fluent_set:
                pic_fl = self.get_conj_fluent(conj)
                if pic_fl != '':
                    new_set.add(pic_fl)
        return new_set
            

    def get_semi_relaxed_model(self, model, conjuncts_set, debug=False):
        # Perform a test to make sure conjunct set meets the requirements
        #conjuncts_set = [set(a) for a in list(powerset(conjuncts))]
        #print (conjuncts_set)
        compiled_model = {INSTANCE:{}, DOMAIN:{}} #copy.deepcopy(model)

        # Add the conjuncts into initial state and goal state
        compiled_model[INSTANCE][INIT] = self.get_powc(model[INSTANCE][INIT], conjuncts_set)
        compiled_model[INSTANCE][GOAL] = self.get_powc(model[INSTANCE][GOAL], conjuncts_set)

        for act in model[DOMAIN]:
            compiled_model[DOMAIN][act] = {}
            compiled_model[DOMAIN][act][POS_PREC] = self.get_powc(model[DOMAIN][act][POS_PREC], conjuncts_set)
            #compiled_model[DOMAIN][act][ADDS] = self.get_powc(model[DOMAIN][act][ADDS]| (model[DOMAIN][act][POS_PREC] - model[DOMAIN][act][DELS]), conjuncts_set)
            compiled_model[DOMAIN][act][ADDS] = self.get_powc(model[DOMAIN][act][ADDS], conjuncts_set)

            compiled_model[DOMAIN][act][DELS] = set()

            for conj in conjuncts_set:
                if len(conj & model[DOMAIN][act][DELS]) > 0 or \
                        len(conj & model[DOMAIN][act][ADDS]) == 0:
                    #print ("skipping",conj, "for act", act)
                    continue

                if COND_ADDS not in compiled_model[DOMAIN][act]:
                    compiled_model[DOMAIN][act][COND_ADDS] = set()
                pic_fl = self.get_conj_fluent(conj)
                if pic_fl != '':
                    compiled_model[DOMAIN][act][COND_ADDS].add((frozenset(self.get_powc(model[DOMAIN][act][POS_PREC]|(conj - model[DOMAIN][act][ADDS]),conjuncts_set)), frozenset(), frozenset([pic_fl])))
        return compiled_model
