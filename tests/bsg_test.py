from picunsolv.constants import *
from picunsolv.best_supporter_graph import BestSupporterGraph

import unittest


class BsgTestCase(unittest.TestCase):
    def test_basic_relaxed_plan(self):
        model = {INSTANCE:{INIT: set(), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1'])},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2'])},
                          'a3': {POS_PREC: set(['p2']),
                                 ADDS: set(['p3'])}
                          }
                 }
        expected_relaxed_plan = ['a1', 'a2', 'a3']
        expected_predc_map = {'p1':'a1', 'p2':'a2', 'p3':'a3',}

        bsg_gen = BestSupporterGraph()

        relaxed_plan, bsg_map, updated_model = bsg_gen.get_relaxed_plan(model)

        self.assertEqual(expected_relaxed_plan, relaxed_plan)
        #self.assertEqual(expected_predc_map, predc_map)

    def test_loopy_relaxed_plan(self):
        model = {INSTANCE:{INIT: set(), GOAL: set(['p3'])},
                 DOMAIN: {'a1': {POS_PREC: set(),
                                 ADDS: set(['p1'])},
                          'a2': {POS_PREC: set(['p1']),
                                 ADDS: set(['p2'])},
                          'a3': {POS_PREC: set(['p2']),
                                 ADDS: set(['p3'])},
                          'a4': {POS_PREC: set(['p3']),
                                 ADDS: set(['p2'])},
                          }
                 }
        expected_relaxed_plan = ['a1', 'a2', 'a3']
        expected_predc_map = {'p1':'a1', 'p2':'a2', 'p3':'a3',}

        bsg_gen = BestSupporterGraph()

        relaxed_plan, predc_map, updated_model = bsg_gen.get_relaxed_plan(model)

        self.assertEqual(expected_relaxed_plan, relaxed_plan)
        #self.assertEqual(expected_predc_map, predc_map)


if __name__ == '__main__':
    unittest.main()
