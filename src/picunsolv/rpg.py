from picunsolv.constants import *
import copy

MAX_DEPTH = 10000
class RPG:
    def __init__(self):
        pass

    def test_reachability(self, model):
        init_state = model[INSTANCE][INIT]
        goal = model[INSTANCE][GOAL]
        is_converged = False
        curr_state = copy.deepcopy(init_state)
        print ("Starting...")
        #print (goal)
        while not is_converged:
            old_state = copy.deepcopy(curr_state)
            for act in model[DOMAIN]:
                if model[DOMAIN][act][POS_PREC] <= old_state and len(model[DOMAIN][act].get(NEG_PREC, set()) & old_state) == 0:
                    curr_state |= model[DOMAIN][act][ADDS]
                    for cond, cond_neg, eff in model[DOMAIN][act].get(COND_ADDS, set()):
                        if set(cond) <= curr_state and len(set(cond_neg) & curr_state) == 0:
                            curr_state |= set(eff)

            if goal <= curr_state:
                if 'CONJ_person-at_p0_loc-4-0&person-at_p1_loc-4-1&person-at_p2_loc-4-2&person-at_p3_loc-4-3&person-at_p4_loc-4-4' in curr_state:
                    print ("################the fact was added")
                #print (curr_state)
                return True
            #print (curr_state)
            #print(old_state)
            if old_state == curr_state:
                is_converged = True
        return False

    def return_set_level(self, model):
        init_state = model[INSTANCE][INIT]
        goal = model[INSTANCE][GOAL]
        #print ("Goal", goal)
        is_converged = False
        unique_actions_found = set()
        action_reachability_map = {}
        curr_state = copy.deepcopy(init_state)
        print ("Starting...")

        curr_act_level = 1
        while not is_converged:
            old_state = copy.deepcopy(curr_state)

            for act in model[DOMAIN]:
                if model[DOMAIN][act][POS_PREC] <= old_state and len(model[DOMAIN][act].get(NEG_PREC, set()) & old_state) == 0:
                    if act not in action_reachability_map:
                        action_reachability_map[act] = curr_act_level

                    curr_state |= model[DOMAIN][act][ADDS]
                    for cond, cond_neg, eff in model[DOMAIN][act].get(COND_ADDS, set()):
                        if set(cond) <= curr_state and len(set(cond_neg) & curr_state) == 0:
                            curr_state |= set(eff)
            curr_act_level += 1

            #print(curr_state)
            if goal <= curr_state:
                return action_reachability_map

            if old_state == curr_state:
                is_converged = True
        return action_reachability_map

