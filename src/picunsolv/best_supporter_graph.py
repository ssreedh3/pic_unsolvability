# Todo: Assuming all actions are unit cost
from queue import Queue
from picunsolv.constants import *
from picunsolv.rpg import RPG
from picunsolv.utils import *
from numpy.random import shuffle
import copy

class BestSupporterGraph():
    # Todo: Assume 's' is the initial state in the paper
    def __init__(self):
        pass

    def get_condition_free_models(self, model):
        new_model = {}
        new_model[INSTANCE] = copy.deepcopy(model[INSTANCE])
        new_model[DOMAIN] = {}
        for act in model[DOMAIN]:
            new_model[DOMAIN][act] = {}
            new_model[DOMAIN][act][POS_PREC] = copy.deepcopy(model[DOMAIN][act][POS_PREC])
            new_model[DOMAIN][act][ADDS] = copy.deepcopy(model[DOMAIN][act][ADDS])
            id = 0
            for cond, neg_cond, adds in model[DOMAIN][act].get(COND_ADDS, set()):
                id += 1
                new_act = act +COND_ACT_NAME_SEPARTOR+ str(id)
                new_model[DOMAIN][new_act] = {}
                new_model[DOMAIN][new_act][POS_PREC] = model[DOMAIN][act][POS_PREC] | set(cond)
                new_model[DOMAIN][new_act][ADDS] = copy.deepcopy(model[DOMAIN][act][ADDS])
                new_model[DOMAIN][new_act][ADDS] = model[DOMAIN][act][ADDS] | set(adds)
        return new_model

    def get_prop_to_act_map(self, model):
        # Todo: Make it more effecient
        prop_map = {}
        for act in model[DOMAIN]:
            for prop in model[DOMAIN][act][ADDS]:
                if prop not in prop_map:
                    prop_map[prop] = set()
                prop_map[prop].add(act)
        for prop in model[INSTANCE][INIT]:
            if prop not in prop_map:
                prop_map[prop] = set()
            prop_map[prop].add(INIT)
        return prop_map

    def get_cost_of_supporter(self, act):
        return self.bsp_cost_map.get(act, float("inf"))

    def get_best_supporter_action(self, current_prop, prop_map, model):
        if current_prop in self.bsp_map:
            return self.bsp_map[current_prop]

        best_act = None
        best_val = MAX_RELAXED_PLAN_COST
        for act in prop_map[current_prop]:
            current_cost = self.get_cost_of_supporter(act)
            #print("act", act, self.bsp_cost_map)
            if best_val > current_cost:
                best_val = current_cost
                best_act = act
        assert best_act is not None, "Current prop"+str(current_prop)+str(prop_map[current_prop])
        self.bsp_map[current_prop] = best_act
        return self.bsp_map[current_prop]





    def get_best_supporter(self, prop, model, prop_map):
        if prop in model[INSTANCE][INIT]:
            return (set(), set(), INIT)

        supporter_action = self.get_best_supporter_action(prop, prop_map, model)
        if supporter_action == INIT:
            return (set(), set(), INIT)
        supporter_set = set()
        supporter_set.add(supporter_action)
        bsg_set, curr_edges = self.get_best_supporter_set(supporter_action, model[DOMAIN][supporter_action][POS_PREC], model, prop_map)
        return (supporter_set| bsg_set, curr_edges, supporter_action)


    def get_best_supporter_set(self, act, prop_list, model, prop_map):
        best_supporter_set = set()
        edge_set = set()
        for prop in prop_list:
            new_supp, new_edges, supp_act =self.get_best_supporter(prop, model, prop_map)
            best_supporter_set |= new_supp
            edge_set |= new_edges
            edge_set.add((act, supp_act))

        return best_supporter_set, edge_set

    def topological_sort(self, bsp_set, edges):
        # Following Kahn's algorithm
        nodes_with_no_incom = copy.deepcopy(bsp_set)

        # Add queue and init
        nodes_with_no_incom.add(GOAL)
        reverse_graph_map = {}
        for edge in edges:
            b, a = edge
            if a not in reverse_graph_map:
                reverse_graph_map[a] = set()
            reverse_graph_map[a].add(b)
            if a in nodes_with_no_incom:
                nodes_with_no_incom.remove(a)
        #print (bsp_set)
        #print (edges)
        #print ("no incom nodes",nodes_with_no_incom)
        sorted_graph = []
        sort_queue = Queue()
        for node in nodes_with_no_incom:
            sort_queue.put(node)
        while not sort_queue.empty():
            node = sort_queue.get()
            sorted_graph.append(node)
            for other_node in reverse_graph_map:
                if node in reverse_graph_map[other_node]:
                    reverse_graph_map[other_node].remove(node)
                    if len(reverse_graph_map[other_node]) == 0:
                        sort_queue.put(other_node)
        if INIT in sorted_graph:
            sorted_graph.remove(INIT)
        #print (sorted_graph)
        assert len(sorted_graph) == len(bsp_set)+1, "relaxed plan is missing actions"+str(bsp_set)+str(sorted_graph)

        return list(reversed(sorted_graph))


    def initialize_best_supporter_graph(self, model, prop_map):
        converged = False
        iteration_cnt = 0
        while not converged:
            iteration_cnt += 1
            converged = True
            new_bsg_map = {}
            new_bsg_map[INIT] = 0
            for act in model[DOMAIN]:
                new_bsg_map[act] = RELAXED_ACTION_COST

                # Make sure all the precs are generated by some action
                precs_possible = True
                for prec in model[DOMAIN][act][POS_PREC]:
                    best_val = MAX_RELAXED_PLAN_COST
                    if prec not in prop_map:
                        precs_possible = False
                    else:
                        for supp_act in prop_map[prec]:
                            if best_val > self.bsp_cost_map[supp_act]:
                                best_val = self.bsp_cost_map[supp_act]

                        new_bsg_map[act] += best_val
                if not precs_possible:
                    new_bsg_map[act] = MAX_RELAXED_PLAN_COST
                if new_bsg_map[act] != self.bsp_cost_map[act]:
                    converged = False

            self.bsp_cost_map = copy.deepcopy(new_bsg_map)
            if iteration_cnt >= 500:
                converged = True

    def get_dedup_set(self, orig_prop_set):
        prop_set = copy.deepcopy(orig_prop_set)

        DEDUP_COMPLETED = False
        while not DEDUP_COMPLETED:
            new_prop_set = set()
            DEDUP_COMPLETED = True
            for prec in prop_set:
                curr_prep_set = set()
                if CONJ_PREFIX in prec:
                    curr_prep_set |= set(prec.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR))
                else:
                    curr_prep_set.add(prec)
                super_set_found = False
                for other_prec in prop_set - set([prec]):
                    if CONJ_PREFIX in other_prec and set(other_prec.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR)) >= curr_prep_set:
                        super_set_found = True
                if not super_set_found:
                    new_prop_set.add(prec)
                else:
                    DEDUP_COMPLETED = False
            prop_set = copy.deepcopy(new_prop_set)


        return prop_set

    def generate_best_supporter_graph(self, model, bsp_set):
        edges = set()
        for act in bsp_set:
            for prec in self.get_dedup_set(model[DOMAIN][act][POS_PREC]):
                if prec not in model[INSTANCE][INIT]:
                    edge = (act, self.bsp_map[prec])
                    edges.add(edge)
        return edges

    def schedule_best_supporters(self, best_supporter_set, edges):
        sorted_graph = self.topological_sort(best_supporter_set, edges)
        return sorted_graph

    def get_relaxed_plan(self, orig_model, is_updated_model=False):


        self.bsp_cost_map = {}

        self.bsp_map = {}

        if not is_updated_model:
            updated_model = self.get_condition_free_models(orig_model)
        else:
            updated_model = orig_model

        prop_map = self.get_prop_to_act_map(updated_model)

        for act in updated_model[DOMAIN]:
            self.bsp_cost_map[act] = 0
        self.bsp_cost_map[INIT] = 0
        rpg_action_map = RPG()
        self.bsp_cost_map = rpg_action_map.return_set_level(updated_model)
        self.bsp_cost_map[INIT] = 0

        try:
            best_supporter_set, bsg_edge_list = self.get_best_supporter_set(GOAL, self.get_dedup_set(updated_model[INSTANCE][GOAL]), updated_model, prop_map)

        except RecursionError:
           print ("Something wrong with best supporter action calculation....")
           print(self.bsp_cost_map)
           exit(1)

        relaxed_plan = self.schedule_best_supporters(best_supporter_set, bsg_edge_list)
        return relaxed_plan[:-1], self.bsp_map, updated_model

