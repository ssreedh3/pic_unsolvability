mkdir -p ../LOGS_SYS_TIME/$1/
for i in `ls ../domains/grounded/$1/`
do
    echo $i
    mkdir -p ../LOGS_SYS_TIME/${1}/${i}/
    timeout 30m python ../src/picunsolv/main.py ../domains/grounded/${1}/${i}/domain.pddl ../domains/grounded/${1}/${i}/problem.pddl > ../LOGS_SYS_TIME/${1}/${i}/results.logs
done
