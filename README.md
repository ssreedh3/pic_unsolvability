# README #

The code was tested on python version 3.5.0

#
### Installation
```
pip install -r requirements.txt 
python setup.py develop
```

#
### File structure

* driver.py - Main driver class. Defines the goal test and the successor generator.
* Search.py - The various search algorithms being used. Currently includes an implementation for Breadth First, A* and 
hill climbing.
* model_parser.py/model_writer.py - The wrapper over tarski that creates a simpler represenation of the tarski object  
(just simpler to operate on python dictionaries for projecting out variables).
* rpg.py - The relaxed planning graph. Calculates reachability and set_level (using as a stand-in for h_max  
since we are dealing with unit action cost domains)
* picce_generator.py - The class for generating picce domains
* pim_generator.py - The class for generating pim domains
* Concretizer.py - Creates the abstract versions of the given planning problems
* trial_based_pic_identification.py - The class that includes all functions that for a given relaxed plan finds  
the pic fluents related to failure in the full model
* best_supporter_graph.py - Class for extracting best supporter graph for the given problem.


