from picunsolv.constants import *
import copy

class TrialBasedIdentification():
    def __init__(self):
        pass

    def check_if_act_deletes_all(self, preconditions, act, model):
        predec_act_orig_name = act.split(COND_ACT_NAME_SEPARTOR)[0]
        deleted_facts = model[DOMAIN][predec_act_orig_name].get(DELS, set())
        base_prec_facts = set()
        deleted_precs = set()
        for fact_conj in preconditions:
            if CONJ_PREFIX in fact_conj:
                for pred in set(fact_conj.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR)):
                    base_prec_facts.add(pred)
            else:
                base_prec_facts.add(fact_conj)
            if len(base_prec_facts & deleted_facts) > 0:
                deleted_precs.add(fact_conj)
        return len(deleted_precs) > 0,  deleted_precs
        #else:
        #    return False, set()

    def get_dedup_set(self, orig_prop_set):
        # TODO: Move it to a common util
        prop_set = copy.deepcopy(orig_prop_set)

        DEDUP_COMPLETED = False
        while not DEDUP_COMPLETED:
            new_prop_set = set()
            DEDUP_COMPLETED = True
            for prec in prop_set:
                curr_prep_set = set()
                if CONJ_PREFIX in prec:
                    curr_prep_set |= set(prec.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR))
                else:
                    curr_prep_set.add(prec)
                super_set_found = False
                for other_prec in prop_set - set([prec]):
                    if CONJ_PREFIX in other_prec and set(other_prec.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR)) >= curr_prep_set:
                        super_set_found = True
                if not super_set_found:
                    new_prop_set.add(prec)
                else:
                    DEDUP_COMPLETED = False
            prop_set = copy.deepcopy(new_prop_set)


        return prop_set


    def find_successors(self, curr_id, succ_map):
        CONVERGED = False
        cur_act = set([curr_id])
        while not CONVERGED:
            new_act_set = set([curr_id])
            for ac in cur_act:
                if ac in succ_map:
                    for new_ac in succ_map[ac]:
                        if new_ac > ac:
                            new_act_set.add(new_ac)
            if cur_act == new_act_set:
                CONVERGED = True
            cur_act = copy.deepcopy(new_act_set)
        return cur_act

    def check_if_path_exists_new(self, curr_id, failure_level, succ_map):
        print ("checking if path exists")

        successor_set = self.find_successors(curr_id, succ_map)
        if failure_level in successor_set:
            return True
        #print (successor_set)
        return False

    def find_common_succ(self, act_id, failure_level, succ_map):
        succ_for_act_id = self.find_successors(act_id, succ_map)
        succ_for_del = self.find_successors(failure_level, succ_map)
        return succ_for_act_id & succ_for_del

    def check_if_same_action(self, fail_name, del_name):
        #CONJ_PREFIX = "CONJ_"
        #CONJ_SEPARATOR = '&'
        if fail_name.split(MERGE_SEPARATOR)[0].split(COND_ACT_NAME_SEPARTOR)[0] != \
                del_name.split(MERGE_SEPARATOR)[0].split(COND_ACT_NAME_SEPARTOR)[0]:
            return False
        return True

    def find_failure_paths_new(self, act_id, failure_level, succ_map):

        CONVERGED = False
        deleter_path = set()
        cur_act = set([act_id])
        path_maps = {act_id: set()}
        while not CONVERGED:
            new_act_set = set([act_id])
            for ac in cur_act:
                if ac in succ_map:
                    for succ_act in succ_map[ac]:
                        path_maps[succ_act] = succ_map[ac][succ_act] | path_maps[ac]
                        new_act_set.add(succ_act)
            if failure_level in path_maps:
                print ("failure act path", cur_act, failure_level)
                return path_maps[failure_level]
            if cur_act == new_act_set:
                CONVERGED = True
            cur_act = copy.deepcopy(new_act_set)

        return None

    def check_if_act_supports(self, to_be_supported, adds):
        indiv_preds = set()
        for c_pred in to_be_supported:
            for pred in c_pred.replace(CONJ_PREFIX, '').split(CONJ_SEPARATOR):
                indiv_preds.add(pred)
        return len(indiv_preds& adds) > 0

    def get_succ_map(self, plan, conj_model):
        # Assume plan contains INIT and GOAL act

        succ_act_map = {}
        curr_state = set()

        # Add things for INIT
        curr_state |= conj_model[INSTANCE][INIT]
        prep_to_act_map = {}
        #for prop in conj_model[INSTANCE][INIT]:


        prep_to_act_map = {}
        for act_id in range(len(plan)):
            merged_act = plan[act_id]
            succ_act_map[act_id] = set()
            for act in merged_act.split(MERGE_SEPARATOR):
                for prec in conj_model[DOMAIN][act][POS_PREC]:
                    if prec in prep_to_act_map:
                        succ_act_map[prep_to_act_map[prec]].add(act_id)

            for act in merged_act.split(MERGE_SEPARATOR):
                curr_state |= conj_model[DOMAIN][act][ADDS]
                for prep in conj_model[DOMAIN][act][ADDS]:
                    prep_to_act_map[prep] = act_id
        return succ_act_map


    def test_relaxed_plan(self, model, conj_model, plan, prev_clauses = []):
        # TODO: Assuming no conditional effects
        # Need to allow goals
        curr_state = conj_model[INSTANCE][GOAL]
        succ_act_map = {}
        act_precs_yet_tobe_statisfied = {}
        act_precs_yet_tobe_statisfied[len(plan)] = copy.deepcopy(self.get_dedup_set(curr_state))
        act_id = len(plan)
        deleter_found = False
        parallel_error_stack = []

        #print ("####Plan", plan)
        seq_failure_clauses = []
        ALL_ERRORS = []
        # Look for in line errors before parallel errors
        for act in reversed(plan):
            if not deleter_found:
                act_id -= 1
                succ_act_map[act_id] = {}
                del_status_for_act = False
                poss_failure_levels = []
                #print (act_id)
                for prev_act in act_precs_yet_tobe_statisfied:

                    if len(act_precs_yet_tobe_statisfied[prev_act] & conj_model[DOMAIN][act][ADDS]) > 0:
                        if prev_act not in succ_act_map[act_id]:
                            succ_act_map[act_id][prev_act] = set()
                        succ_act_map[act_id][prev_act] |= act_precs_yet_tobe_statisfied[prev_act] & conj_model[DOMAIN][act][ADDS]

                        act_precs_yet_tobe_statisfied[prev_act] -= conj_model[DOMAIN][act][ADDS]
                    indiv_del_status, indiv_del_fact_list = self.check_if_act_deletes_all(act_precs_yet_tobe_statisfied[prev_act], act, model)
                    if indiv_del_status:
                        del_status_for_act = True
                        for indiv_del_fact in indiv_del_fact_list:
                            poss_failure_levels.append((prev_act, indiv_del_fact))

                act_precs_yet_tobe_statisfied[act_id] = copy.deepcopy(self.get_dedup_set(conj_model[DOMAIN][act][POS_PREC]))
                if del_status_for_act:
                    for failure_level, del_fact in poss_failure_levels:
                        # Find if there is a path from deleter to precondition
                        failure_clauses = []
                        try:

                            if self.check_if_path_exists_new(act_id, failure_level, succ_act_map):
                                #print (act_id, failure_level, del_fact)
                                failure_paths = self.find_failure_paths_new(act_id, failure_level, succ_act_map)
                                for pred in failure_paths:
                                    #curr_var = set([del_fact, pred])
                                    # Make it all indiv sets
                                    curr_var = set()
                                    for fct in del_fact.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR):
                                        curr_var.add(fct)
                                    for fct in pred.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR):
                                        curr_var.add(fct)
                                    if curr_var not in failure_clauses:
                                        failure_clauses.append(curr_var)


                                is_duplicate = True
                                for cl in failure_clauses:
                                    if cl not in prev_clauses:
                                        is_duplicate = False
                                #if not is_duplicate:
                                #    deleter_found = True
                            else:
                                # Push to parallel error stack
                                parallel_error_stack.append((act_id, failure_level, del_fact))
                        except RecursionError:
                            print("Recursion error")
                            exit(1)
                        #for t_ac in plan:
                        #    print (t_ac, conj_model[DOMAIN][t_ac])
                        #exit(1)
                        if len(failure_clauses) > 0:
                            #print ("FAilure clause",failure_clauses)
                            #print ("Failure_path", failure_paths)
                            #return failure_clauses
                            #seq_failure_clauses = copy.deepcopy(failure_clauses)
                            ALL_ERRORS.append(failure_clauses)
        #if len(seq_failure_clauses) > 0:
        #    print("FAilure clause", seq_failure_clauses)
        #    print("Failure_path", seq_failure_clauses)
        #    return seq_failure_clauses
        if len(ALL_ERRORS) > 0: # and STAGERRED_PRUNING:
            return  ALL_ERRORS

        failure_clauses = []
        for ac in act_precs_yet_tobe_statisfied:
            act_precs_yet_tobe_statisfied[ac] -= conj_model[INSTANCE][INIT]
            if len(act_precs_yet_tobe_statisfied[ac]) > 0:
                for prec in act_precs_yet_tobe_statisfied[ac]:
                    failure_clauses = [set([list(set(prec.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR)))[0]])]
                    print ("UNsatisfied precondition", ac,set(prec.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR)))
        if len(failure_clauses) > 0:
            ALL_ERRORS.append(failure_clauses)
            #print ("Relaxed plan")
            #print ("UNsatisfied precondition", failure_clauses)
        #    return failure_clauses

        if len(ALL_ERRORS) > 0: # and STAGERRED_PRUNING:
            return  ALL_ERRORS

        parallel_error_clauses = []

        for del_level, fail_level, del_fact in parallel_error_stack:
            if not self.check_if_same_action(plan[fail_level], plan[del_level]):
                # find common successor
                common_succ = self.find_common_succ(del_level, fail_level, succ_act_map)
                sorted(common_succ, reverse=True)
                # find path to each successor
                for succ_level in common_succ:
                    failure_clauses = []
                    del_paths = self.find_failure_paths_new(del_level, succ_level, succ_act_map)
                    failure_paths = self.find_failure_paths_new(fail_level, succ_level, succ_act_map)
                    failure_paths.add(del_fact)

                    for d_prep in del_paths:
                        for f_prep in failure_paths:
                            curr_var = set() #[d_prep, f_prep])
                            for fct in d_prep.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR):
                                curr_var.add(fct)
                            for fct in f_prep.replace(CONJ_PREFIX,'').split(CONJ_SEPARATOR):
                                curr_var.add(fct)
                            if curr_var not in failure_clauses:
                                failure_clauses.append(curr_var)

                    if len(failure_clauses) > 0:
                        #print ("failure",del_level, fail_level, del_fact, del_paths, failure_paths )
                        #print ("Parallel failure", failure_clauses)
                        ALL_ERRORS.append(failure_clauses)
        return ALL_ERRORS


