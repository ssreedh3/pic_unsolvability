mkdir -p ../LOGS_ORIGINAL/$1/
for i in `ls ../domains/grounded/$1/`
do
    echo $i
    mkdir -p ../LOGS_ORIGINAL/${1}/${i}/
    pushd ../src/picunsolv/
    timeout 30m python main.py ../../domains/grounded/${1}/${i}/domain.pddl ../../domains/grounded/${1}/${i}/problem.pddl > ../../LOGS_ORIGINAL/${1}/${i}/results.logs
    popd
done
