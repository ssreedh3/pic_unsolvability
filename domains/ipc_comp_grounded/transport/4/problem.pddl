(define
	(problem grounded-TRANSPORT-CITY-SEQUENTIAL-15NODES-1000SIZE-3DEGREE-100MINDISTANCE-3TRUCKS-8PACKAGES-2012SEED)
	(:domain grounded-TRANSPORT)
	(:init
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG6_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG5_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG4_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG3_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-15_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-12_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG2_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-14_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-13_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-11_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-10_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-9_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-8_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-7_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-6_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-5_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-4_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_TRUCK-3_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_TRUCK-2_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG1_TRUCK-1_PNUM0 )
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-2_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-10_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-13_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-3_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-11_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-6_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-15_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-12_PNUM0 )
		( AT-VEHICLE_TRUCK-3_CITY-LOC-14 )
		( AT-VEHICLE_TRUCK-2_CITY-LOC-2 )
		( AT-VEHICLE_TRUCK-1_CITY-LOC-13 )
		( CAPACITY_TRUCK-3_CAPACITY-2 )
		( CAPACITY_TRUCK-1_CAPACITY-2 )
		( CAPACITY_TRUCK-2_CAPACITY-4 )
	)
	(:goal
		(and 
		( COUNT-PACKAGE_PACKAGE-BAG7_CITY-LOC-15_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG6_CITY-LOC-9_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG5_CITY-LOC-8_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG4_CITY-LOC-12_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG3_CITY-LOC-6_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG2_CITY-LOC-4_PNUM1 )
		( COUNT-PACKAGE_PACKAGE-BAG1_CITY-LOC-10_PNUM2 )
		)
	)

)
