(define
	(problem grounded-GRID-PROB-4-1)
	(:domain grounded-RESOURCES)
	(:init
		( PERSON-AT_P3_LOC-0-3 )
		( PERSON-AT_P2_LOC-0-2 )
		( PERSON-AT_P1_LOC-0-1 )
		( PERSON-AT_P0_LOC-0-0 )
		( ACTIVE_LOC-3-3 )
		( ACTIVE_LOC-3-2 )
		( ACTIVE_LOC-3-1 )
		( ACTIVE_LOC-3-0 )
		( ACTIVE_LOC-2-0 )
		( ACTIVE_LOC-1-3 )
		( ACTIVE_LOC-1-2 )
		( ACTIVE_LOC-1-1 )
		( ACTIVE_LOC-1-0 )
	)
	(:goal
		(and 
		( PERSON-AT_P3_LOC-3-3 )
		( PERSON-AT_P2_LOC-3-2 )
		( PERSON-AT_P1_LOC-3-1 )
		( PERSON-AT_P0_LOC-3-0 )
		)
	)

)
